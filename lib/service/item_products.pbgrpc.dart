///
//  Generated code. Do not modify.
//  source: item_products.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'item_products.pb.dart' as $0;
export 'item_products.pb.dart';

class ProductoStockClient extends $grpc.Client {
  static final _$getProductsList =
      $grpc.ClientMethod<$0.CategoryIdRequest, $0.ItemListResponse>(
          '/item_products_pb.itemProducts.ProductoStock/GetProductsList',
          ($0.CategoryIdRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ItemListResponse.fromBuffer(value));
  static final _$setProduct =
      $grpc.ClientMethod<$0.Item, $0.ItemStatusResponse>(
          '/item_products_pb.itemProducts.ProductoStock/SetProduct',
          ($0.Item value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ItemStatusResponse.fromBuffer(value));
  static final _$getCategoriesList = $grpc.ClientMethod<
          $0.GetCategoriesListRequest, $0.GetCategoriesListResponse>(
      '/item_products_pb.itemProducts.ProductoStock/GetCategoriesList',
      ($0.GetCategoriesListRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetCategoriesListResponse.fromBuffer(value));

  ProductoStockClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ItemListResponse> getProductsList(
      $0.CategoryIdRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getProductsList, request, options: options);
  }

  $grpc.ResponseFuture<$0.ItemStatusResponse> setProduct($0.Item request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$setProduct, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetCategoriesListResponse> getCategoriesList(
      $0.GetCategoriesListRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getCategoriesList, request, options: options);
  }
}

abstract class ProductoStockServiceBase extends $grpc.Service {
  $core.String get $name => 'item_products_pb.itemProducts.ProductoStock';

  ProductoStockServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CategoryIdRequest, $0.ItemListResponse>(
        'GetProductsList',
        getProductsList_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CategoryIdRequest.fromBuffer(value),
        ($0.ItemListResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Item, $0.ItemStatusResponse>(
        'SetProduct',
        setProduct_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Item.fromBuffer(value),
        ($0.ItemStatusResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetCategoriesListRequest,
            $0.GetCategoriesListResponse>(
        'GetCategoriesList',
        getCategoriesList_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetCategoriesListRequest.fromBuffer(value),
        ($0.GetCategoriesListResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.ItemListResponse> getProductsList_Pre($grpc.ServiceCall call,
      $async.Future<$0.CategoryIdRequest> request) async {
    return getProductsList(call, await request);
  }

  $async.Future<$0.ItemStatusResponse> setProduct_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Item> request) async {
    return setProduct(call, await request);
  }

  $async.Future<$0.GetCategoriesListResponse> getCategoriesList_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetCategoriesListRequest> request) async {
    return getCategoriesList(call, await request);
  }

  $async.Future<$0.ItemListResponse> getProductsList(
      $grpc.ServiceCall call, $0.CategoryIdRequest request);
  $async.Future<$0.ItemStatusResponse> setProduct(
      $grpc.ServiceCall call, $0.Item request);
  $async.Future<$0.GetCategoriesListResponse> getCategoriesList(
      $grpc.ServiceCall call, $0.GetCategoriesListRequest request);
}
