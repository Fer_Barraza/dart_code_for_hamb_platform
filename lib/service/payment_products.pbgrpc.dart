///
//  Generated code. Do not modify.
//  source: payment_products.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'payment_products.pb.dart' as $0;
export 'payment_products.pb.dart';

class PaymentProductsClient extends $grpc.Client {
  static final _$insertProductsPurchased = $grpc.ClientMethod<
          $0.PaymentToProcess, $0.ItemStatusResponse>(
      '/payment_products_pb.paymentProducts.PaymentProducts/InsertProductsPurchased',
      ($0.PaymentToProcess value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ItemStatusResponse.fromBuffer(value));
  static final _$changeOrderState = $grpc.ClientMethod<$0.OrderToChangeRequest,
          $0.ItemStatusResponse>(
      '/payment_products_pb.paymentProducts.PaymentProducts/ChangeOrderState',
      ($0.OrderToChangeRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ItemStatusResponse.fromBuffer(value));
  static final _$getLastOrders =
      $grpc.ClientMethod<$0.GetLastOrdersRequest, $0.GetLastOrdersResponse>(
          '/payment_products_pb.paymentProducts.PaymentProducts/GetLastOrders',
          ($0.GetLastOrdersRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetLastOrdersResponse.fromBuffer(value));
  static final _$getLastOrderById = $grpc.ClientMethod<
          $0.GetLastOrderByIdRequest, $0.GetLastOrderByIdResponse>(
      '/payment_products_pb.paymentProducts.PaymentProducts/GetLastOrderById',
      ($0.GetLastOrderByIdRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetLastOrderByIdResponse.fromBuffer(value));

  PaymentProductsClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ItemStatusResponse> insertProductsPurchased(
      $0.PaymentToProcess request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$insertProductsPurchased, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.ItemStatusResponse> changeOrderState(
      $0.OrderToChangeRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$changeOrderState, request, options: options);
  }

  $grpc.ResponseStream<$0.GetLastOrdersResponse> getLastOrders(
      $0.GetLastOrdersRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$getLastOrders, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseStream<$0.GetLastOrderByIdResponse> getLastOrderById(
      $0.GetLastOrderByIdRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$getLastOrderById, $async.Stream.fromIterable([request]),
        options: options);
  }
}

abstract class PaymentProductsServiceBase extends $grpc.Service {
  $core.String get $name =>
      'payment_products_pb.paymentProducts.PaymentProducts';

  PaymentProductsServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.PaymentToProcess, $0.ItemStatusResponse>(
        'InsertProductsPurchased',
        insertProductsPurchased_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PaymentToProcess.fromBuffer(value),
        ($0.ItemStatusResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.OrderToChangeRequest, $0.ItemStatusResponse>(
            'ChangeOrderState',
            changeOrderState_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.OrderToChangeRequest.fromBuffer(value),
            ($0.ItemStatusResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.GetLastOrdersRequest, $0.GetLastOrdersResponse>(
            'GetLastOrders',
            getLastOrders_Pre,
            false,
            true,
            ($core.List<$core.int> value) =>
                $0.GetLastOrdersRequest.fromBuffer(value),
            ($0.GetLastOrdersResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetLastOrderByIdRequest,
            $0.GetLastOrderByIdResponse>(
        'GetLastOrderById',
        getLastOrderById_Pre,
        false,
        true,
        ($core.List<$core.int> value) =>
            $0.GetLastOrderByIdRequest.fromBuffer(value),
        ($0.GetLastOrderByIdResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.ItemStatusResponse> insertProductsPurchased_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PaymentToProcess> request) async {
    return insertProductsPurchased(call, await request);
  }

  $async.Future<$0.ItemStatusResponse> changeOrderState_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.OrderToChangeRequest> request) async {
    return changeOrderState(call, await request);
  }

  $async.Stream<$0.GetLastOrdersResponse> getLastOrders_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetLastOrdersRequest> request) async* {
    yield* getLastOrders(call, await request);
  }

  $async.Stream<$0.GetLastOrderByIdResponse> getLastOrderById_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetLastOrderByIdRequest> request) async* {
    yield* getLastOrderById(call, await request);
  }

  $async.Future<$0.ItemStatusResponse> insertProductsPurchased(
      $grpc.ServiceCall call, $0.PaymentToProcess request);
  $async.Future<$0.ItemStatusResponse> changeOrderState(
      $grpc.ServiceCall call, $0.OrderToChangeRequest request);
  $async.Stream<$0.GetLastOrdersResponse> getLastOrders(
      $grpc.ServiceCall call, $0.GetLastOrdersRequest request);
  $async.Stream<$0.GetLastOrderByIdResponse> getLastOrderById(
      $grpc.ServiceCall call, $0.GetLastOrderByIdRequest request);
}
