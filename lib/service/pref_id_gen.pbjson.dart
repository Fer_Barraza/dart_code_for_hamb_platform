///
//  Generated code. Do not modify.
//  source: pref_id_gen.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use paymentToProcessDescriptor instead')
const PaymentToProcess$json = const {
  '1': 'PaymentToProcess',
  '2': const [
    const {'1': 'lista_items', '3': 1, '4': 3, '5': 11, '6': '.pref_id_gen_pb.prefIdGen.Item', '10': 'listaItems'},
    const {'1': 'payer', '3': 2, '4': 1, '5': 11, '6': '.pref_id_gen_pb.prefIdGen.Payer', '10': 'payer'},
    const {'1': 'statement_descriptor', '3': 4, '4': 1, '5': 9, '10': 'statementDescriptor'},
    const {'1': 'additional_info', '3': 5, '4': 1, '5': 9, '10': 'additionalInfo'},
    const {'1': 'method_of_delivery', '3': 6, '4': 1, '5': 9, '10': 'methodOfDelivery'},
    const {'1': 'payment_method_options', '3': 7, '4': 1, '5': 9, '10': 'paymentMethodOptions'},
    const {'1': 'total_price', '3': 8, '4': 1, '5': 2, '10': 'totalPrice'},
    const {'1': 'headquarter_id', '3': 9, '4': 1, '5': 5, '10': 'headquarterId'},
    const {'1': 'branch_id', '3': 10, '4': 1, '5': 5, '10': 'branchId'},
  ],
  '9': const [
    const {'1': 3, '2': 4},
  ],
  '10': const ['preference_id'],
};

/// Descriptor for `PaymentToProcess`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List paymentToProcessDescriptor = $convert.base64Decode('ChBQYXltZW50VG9Qcm9jZXNzEj8KC2xpc3RhX2l0ZW1zGAEgAygLMh4ucHJlZl9pZF9nZW5fcGIucHJlZklkR2VuLkl0ZW1SCmxpc3RhSXRlbXMSNQoFcGF5ZXIYAiABKAsyHy5wcmVmX2lkX2dlbl9wYi5wcmVmSWRHZW4uUGF5ZXJSBXBheWVyEjEKFHN0YXRlbWVudF9kZXNjcmlwdG9yGAQgASgJUhNzdGF0ZW1lbnREZXNjcmlwdG9yEicKD2FkZGl0aW9uYWxfaW5mbxgFIAEoCVIOYWRkaXRpb25hbEluZm8SLAoSbWV0aG9kX29mX2RlbGl2ZXJ5GAYgASgJUhBtZXRob2RPZkRlbGl2ZXJ5EjQKFnBheW1lbnRfbWV0aG9kX29wdGlvbnMYByABKAlSFHBheW1lbnRNZXRob2RPcHRpb25zEh8KC3RvdGFsX3ByaWNlGAggASgCUgp0b3RhbFByaWNlEiUKDmhlYWRxdWFydGVyX2lkGAkgASgFUg1oZWFkcXVhcnRlcklkEhsKCWJyYW5jaF9pZBgKIAEoBVIIYnJhbmNoSWRKBAgDEARSDXByZWZlcmVuY2VfaWQ=');
@$core.Deprecated('Use itemDescriptor instead')
const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'quantity', '3': 3, '4': 1, '5': 13, '10': 'quantity'},
    const {'1': 'currency_id', '3': 4, '4': 1, '5': 9, '10': 'currencyId'},
    const {'1': 'unit_price', '3': 5, '4': 1, '5': 2, '10': 'unitPrice'},
    const {'1': 'item_id', '3': 6, '4': 1, '5': 13, '10': 'itemId'},
    const {'1': 'category_id', '3': 7, '4': 1, '5': 13, '10': 'categoryId'},
  ],
};

/// Descriptor for `Item`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDescriptor = $convert.base64Decode('CgRJdGVtEhQKBXRpdGxlGAEgASgJUgV0aXRsZRIgCgtkZXNjcmlwdGlvbhgCIAEoCVILZGVzY3JpcHRpb24SGgoIcXVhbnRpdHkYAyABKA1SCHF1YW50aXR5Eh8KC2N1cnJlbmN5X2lkGAQgASgJUgpjdXJyZW5jeUlkEh0KCnVuaXRfcHJpY2UYBSABKAJSCXVuaXRQcmljZRIXCgdpdGVtX2lkGAYgASgNUgZpdGVtSWQSHwoLY2F0ZWdvcnlfaWQYByABKA1SCmNhdGVnb3J5SWQ=');
@$core.Deprecated('Use payerDescriptor instead')
const Payer$json = const {
  '1': 'Payer',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'surname', '3': 3, '4': 1, '5': 9, '10': 'surname'},
    const {'1': 'phone', '3': 4, '4': 1, '5': 11, '6': '.pref_id_gen_pb.prefIdGen.Phone', '10': 'phone'},
    const {'1': 'address', '3': 5, '4': 1, '5': 11, '6': '.pref_id_gen_pb.prefIdGen.Address', '10': 'address'},
  ],
};

/// Descriptor for `Payer`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List payerDescriptor = $convert.base64Decode('CgVQYXllchIUCgVlbWFpbBgBIAEoCVIFZW1haWwSEgoEbmFtZRgCIAEoCVIEbmFtZRIYCgdzdXJuYW1lGAMgASgJUgdzdXJuYW1lEjUKBXBob25lGAQgASgLMh8ucHJlZl9pZF9nZW5fcGIucHJlZklkR2VuLlBob25lUgVwaG9uZRI7CgdhZGRyZXNzGAUgASgLMiEucHJlZl9pZF9nZW5fcGIucHJlZklkR2VuLkFkZHJlc3NSB2FkZHJlc3M=');
@$core.Deprecated('Use phoneDescriptor instead')
const Phone$json = const {
  '1': 'Phone',
  '2': const [
    const {'1': 'area_code', '3': 1, '4': 1, '5': 9, '10': 'areaCode'},
    const {'1': 'number', '3': 2, '4': 1, '5': 9, '10': 'number'},
  ],
};

/// Descriptor for `Phone`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List phoneDescriptor = $convert.base64Decode('CgVQaG9uZRIbCglhcmVhX2NvZGUYASABKAlSCGFyZWFDb2RlEhYKBm51bWJlchgCIAEoCVIGbnVtYmVy');
@$core.Deprecated('Use addressDescriptor instead')
const Address$json = const {
  '1': 'Address',
  '2': const [
    const {'1': 'zip_code', '3': 1, '4': 1, '5': 9, '10': 'zipCode'},
    const {'1': 'street_name', '3': 2, '4': 1, '5': 9, '10': 'streetName'},
    const {'1': 'street_number', '3': 3, '4': 1, '5': 13, '10': 'streetNumber'},
  ],
};

/// Descriptor for `Address`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addressDescriptor = $convert.base64Decode('CgdBZGRyZXNzEhkKCHppcF9jb2RlGAEgASgJUgd6aXBDb2RlEh8KC3N0cmVldF9uYW1lGAIgASgJUgpzdHJlZXROYW1lEiMKDXN0cmVldF9udW1iZXIYAyABKA1SDHN0cmVldE51bWJlcg==');
@$core.Deprecated('Use itemStatusResponseDescriptor instead')
const ItemStatusResponse$json = const {
  '1': 'ItemStatusResponse',
  '2': const [
    const {'1': 'status_successful', '3': 1, '4': 1, '5': 8, '10': 'statusSuccessful'},
    const {'1': 'error', '3': 2, '4': 1, '5': 9, '10': 'error'},
    const {'1': 'order_code', '3': 3, '4': 1, '5': 3, '10': 'orderCode'},
    const {'1': 'pref_id_gen', '3': 4, '4': 1, '5': 9, '10': 'prefIdGen'},
  ],
};

/// Descriptor for `ItemStatusResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemStatusResponseDescriptor = $convert.base64Decode('ChJJdGVtU3RhdHVzUmVzcG9uc2USKwoRc3RhdHVzX3N1Y2Nlc3NmdWwYASABKAhSEHN0YXR1c1N1Y2Nlc3NmdWwSFAoFZXJyb3IYAiABKAlSBWVycm9yEh0KCm9yZGVyX2NvZGUYAyABKANSCW9yZGVyQ29kZRIeCgtwcmVmX2lkX2dlbhgEIAEoCVIJcHJlZklkR2Vu');
