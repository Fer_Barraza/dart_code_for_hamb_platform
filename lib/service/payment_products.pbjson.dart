///
//  Generated code. Do not modify.
//  source: payment_products.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use paymentToProcessDescriptor instead')
const PaymentToProcess$json = const {
  '1': 'PaymentToProcess',
  '2': const [
    const {'1': 'lista_items', '3': 1, '4': 3, '5': 11, '6': '.payment_products_pb.paymentProducts.Item', '10': 'listaItems'},
    const {'1': 'payer', '3': 2, '4': 1, '5': 11, '6': '.payment_products_pb.paymentProducts.Payer', '10': 'payer'},
    const {'1': 'orderMp_generated', '3': 3, '4': 1, '5': 13, '10': 'orderMpGenerated'},
    const {'1': 'statement_descriptor', '3': 4, '4': 1, '5': 9, '10': 'statementDescriptor'},
    const {'1': 'additional_info', '3': 5, '4': 1, '5': 9, '10': 'additionalInfo'},
    const {'1': 'method_of_delivery', '3': 6, '4': 1, '5': 9, '10': 'methodOfDelivery'},
    const {'1': 'payment_method_options', '3': 7, '4': 1, '5': 9, '10': 'paymentMethodOptions'},
    const {'1': 'total_price', '3': 8, '4': 1, '5': 2, '10': 'totalPrice'},
    const {'1': 'headquarter_id', '3': 9, '4': 1, '5': 13, '10': 'headquarterId'},
    const {'1': 'branch_id', '3': 10, '4': 1, '5': 13, '10': 'branchId'},
  ],
};

/// Descriptor for `PaymentToProcess`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List paymentToProcessDescriptor = $convert.base64Decode('ChBQYXltZW50VG9Qcm9jZXNzEkoKC2xpc3RhX2l0ZW1zGAEgAygLMikucGF5bWVudF9wcm9kdWN0c19wYi5wYXltZW50UHJvZHVjdHMuSXRlbVIKbGlzdGFJdGVtcxJACgVwYXllchgCIAEoCzIqLnBheW1lbnRfcHJvZHVjdHNfcGIucGF5bWVudFByb2R1Y3RzLlBheWVyUgVwYXllchIrChFvcmRlck1wX2dlbmVyYXRlZBgDIAEoDVIQb3JkZXJNcEdlbmVyYXRlZBIxChRzdGF0ZW1lbnRfZGVzY3JpcHRvchgEIAEoCVITc3RhdGVtZW50RGVzY3JpcHRvchInCg9hZGRpdGlvbmFsX2luZm8YBSABKAlSDmFkZGl0aW9uYWxJbmZvEiwKEm1ldGhvZF9vZl9kZWxpdmVyeRgGIAEoCVIQbWV0aG9kT2ZEZWxpdmVyeRI0ChZwYXltZW50X21ldGhvZF9vcHRpb25zGAcgASgJUhRwYXltZW50TWV0aG9kT3B0aW9ucxIfCgt0b3RhbF9wcmljZRgIIAEoAlIKdG90YWxQcmljZRIlCg5oZWFkcXVhcnRlcl9pZBgJIAEoDVINaGVhZHF1YXJ0ZXJJZBIbCglicmFuY2hfaWQYCiABKA1SCGJyYW5jaElk');
@$core.Deprecated('Use itemDescriptor instead')
const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'quantity', '3': 3, '4': 1, '5': 13, '10': 'quantity'},
    const {'1': 'currency_id', '3': 4, '4': 1, '5': 9, '10': 'currencyId'},
    const {'1': 'unit_price', '3': 5, '4': 1, '5': 2, '10': 'unitPrice'},
    const {'1': 'item_id', '3': 6, '4': 1, '5': 13, '10': 'itemId'},
    const {'1': 'category_id', '3': 7, '4': 1, '5': 13, '10': 'categoryId'},
    const {'1': 'headquarter_id', '3': 8, '4': 1, '5': 13, '10': 'headquarterId'},
    const {'1': 'branch_id', '3': 9, '4': 1, '5': 13, '10': 'branchId'},
    const {'1': 'image_name', '3': 10, '4': 1, '5': 9, '10': 'imageName'},
  ],
};

/// Descriptor for `Item`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDescriptor = $convert.base64Decode('CgRJdGVtEhQKBXRpdGxlGAEgASgJUgV0aXRsZRIgCgtkZXNjcmlwdGlvbhgCIAEoCVILZGVzY3JpcHRpb24SGgoIcXVhbnRpdHkYAyABKA1SCHF1YW50aXR5Eh8KC2N1cnJlbmN5X2lkGAQgASgJUgpjdXJyZW5jeUlkEh0KCnVuaXRfcHJpY2UYBSABKAJSCXVuaXRQcmljZRIXCgdpdGVtX2lkGAYgASgNUgZpdGVtSWQSHwoLY2F0ZWdvcnlfaWQYByABKA1SCmNhdGVnb3J5SWQSJQoOaGVhZHF1YXJ0ZXJfaWQYCCABKA1SDWhlYWRxdWFydGVySWQSGwoJYnJhbmNoX2lkGAkgASgNUghicmFuY2hJZBIdCgppbWFnZV9uYW1lGAogASgJUglpbWFnZU5hbWU=');
@$core.Deprecated('Use payerDescriptor instead')
const Payer$json = const {
  '1': 'Payer',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'surname', '3': 3, '4': 1, '5': 9, '10': 'surname'},
    const {'1': 'phone', '3': 4, '4': 1, '5': 11, '6': '.payment_products_pb.paymentProducts.Phone', '10': 'phone'},
    const {'1': 'address', '3': 5, '4': 1, '5': 11, '6': '.payment_products_pb.paymentProducts.Address', '10': 'address'},
  ],
};

/// Descriptor for `Payer`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List payerDescriptor = $convert.base64Decode('CgVQYXllchIUCgVlbWFpbBgBIAEoCVIFZW1haWwSEgoEbmFtZRgCIAEoCVIEbmFtZRIYCgdzdXJuYW1lGAMgASgJUgdzdXJuYW1lEkAKBXBob25lGAQgASgLMioucGF5bWVudF9wcm9kdWN0c19wYi5wYXltZW50UHJvZHVjdHMuUGhvbmVSBXBob25lEkYKB2FkZHJlc3MYBSABKAsyLC5wYXltZW50X3Byb2R1Y3RzX3BiLnBheW1lbnRQcm9kdWN0cy5BZGRyZXNzUgdhZGRyZXNz');
@$core.Deprecated('Use phoneDescriptor instead')
const Phone$json = const {
  '1': 'Phone',
  '2': const [
    const {'1': 'area_code', '3': 1, '4': 1, '5': 9, '10': 'areaCode'},
    const {'1': 'number', '3': 2, '4': 1, '5': 9, '10': 'number'},
  ],
};

/// Descriptor for `Phone`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List phoneDescriptor = $convert.base64Decode('CgVQaG9uZRIbCglhcmVhX2NvZGUYASABKAlSCGFyZWFDb2RlEhYKBm51bWJlchgCIAEoCVIGbnVtYmVy');
@$core.Deprecated('Use addressDescriptor instead')
const Address$json = const {
  '1': 'Address',
  '2': const [
    const {'1': 'zip_code', '3': 1, '4': 1, '5': 9, '10': 'zipCode'},
    const {'1': 'street_name', '3': 2, '4': 1, '5': 9, '10': 'streetName'},
    const {'1': 'street_number', '3': 3, '4': 1, '5': 13, '10': 'streetNumber'},
  ],
};

/// Descriptor for `Address`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addressDescriptor = $convert.base64Decode('CgdBZGRyZXNzEhkKCHppcF9jb2RlGAEgASgJUgd6aXBDb2RlEh8KC3N0cmVldF9uYW1lGAIgASgJUgpzdHJlZXROYW1lEiMKDXN0cmVldF9udW1iZXIYAyABKA1SDHN0cmVldE51bWJlcg==');
@$core.Deprecated('Use itemStatusResponseDescriptor instead')
const ItemStatusResponse$json = const {
  '1': 'ItemStatusResponse',
  '2': const [
    const {'1': 'status_successful', '3': 1, '4': 1, '5': 8, '10': 'statusSuccessful'},
    const {'1': 'error', '3': 2, '4': 1, '5': 9, '10': 'error'},
    const {'1': 'order_code', '3': 3, '4': 1, '5': 4, '10': 'orderCode'},
  ],
};

/// Descriptor for `ItemStatusResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemStatusResponseDescriptor = $convert.base64Decode('ChJJdGVtU3RhdHVzUmVzcG9uc2USKwoRc3RhdHVzX3N1Y2Nlc3NmdWwYASABKAhSEHN0YXR1c1N1Y2Nlc3NmdWwSFAoFZXJyb3IYAiABKAlSBWVycm9yEh0KCm9yZGVyX2NvZGUYAyABKARSCW9yZGVyQ29kZQ==');
@$core.Deprecated('Use orderToChangeResponseDescriptor instead')
const OrderToChangeResponse$json = const {
  '1': 'OrderToChangeResponse',
  '2': const [
    const {'1': 'status_successful', '3': 1, '4': 1, '5': 8, '10': 'statusSuccessful'},
    const {'1': 'error', '3': 2, '4': 1, '5': 9, '10': 'error'},
  ],
};

/// Descriptor for `OrderToChangeResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderToChangeResponseDescriptor = $convert.base64Decode('ChVPcmRlclRvQ2hhbmdlUmVzcG9uc2USKwoRc3RhdHVzX3N1Y2Nlc3NmdWwYASABKAhSEHN0YXR1c1N1Y2Nlc3NmdWwSFAoFZXJyb3IYAiABKAlSBWVycm9y');
@$core.Deprecated('Use orderToChangeRequestDescriptor instead')
const OrderToChangeRequest$json = const {
  '1': 'OrderToChangeRequest',
  '2': const [
    const {'1': 'order_code', '3': 1, '4': 1, '5': 4, '10': 'orderCode'},
    const {'1': 'new_state_order', '3': 2, '4': 1, '5': 4, '10': 'newStateOrder'},
  ],
};

/// Descriptor for `OrderToChangeRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderToChangeRequestDescriptor = $convert.base64Decode('ChRPcmRlclRvQ2hhbmdlUmVxdWVzdBIdCgpvcmRlcl9jb2RlGAEgASgEUglvcmRlckNvZGUSJgoPbmV3X3N0YXRlX29yZGVyGAIgASgEUg1uZXdTdGF0ZU9yZGVy');
@$core.Deprecated('Use getLastOrdersRequestDescriptor instead')
const GetLastOrdersRequest$json = const {
  '1': 'GetLastOrdersRequest',
  '2': const [
    const {'1': 'branch_id', '3': 1, '4': 1, '5': 13, '10': 'branchId'},
    const {'1': 'headquarter_id', '3': 2, '4': 1, '5': 13, '10': 'headquarterId'},
  ],
};

/// Descriptor for `GetLastOrdersRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getLastOrdersRequestDescriptor = $convert.base64Decode('ChRHZXRMYXN0T3JkZXJzUmVxdWVzdBIbCglicmFuY2hfaWQYASABKA1SCGJyYW5jaElkEiUKDmhlYWRxdWFydGVyX2lkGAIgASgNUg1oZWFkcXVhcnRlcklk');
@$core.Deprecated('Use getLastOrdersResponseDescriptor instead')
const GetLastOrdersResponse$json = const {
  '1': 'GetLastOrdersResponse',
  '2': const [
    const {'1': 'lista_orders', '3': 1, '4': 3, '5': 11, '6': '.payment_products_pb.paymentProducts.OrdersToProcess', '10': 'listaOrders'},
  ],
};

/// Descriptor for `GetLastOrdersResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getLastOrdersResponseDescriptor = $convert.base64Decode('ChVHZXRMYXN0T3JkZXJzUmVzcG9uc2USVwoMbGlzdGFfb3JkZXJzGAEgAygLMjQucGF5bWVudF9wcm9kdWN0c19wYi5wYXltZW50UHJvZHVjdHMuT3JkZXJzVG9Qcm9jZXNzUgtsaXN0YU9yZGVycw==');
@$core.Deprecated('Use getLastOrderByIdRequestDescriptor instead')
const GetLastOrderByIdRequest$json = const {
  '1': 'GetLastOrderByIdRequest',
  '2': const [
    const {'1': 'branch_id', '3': 1, '4': 1, '5': 13, '10': 'branchId'},
    const {'1': 'order_code', '3': 2, '4': 1, '5': 4, '10': 'orderCode'},
    const {'1': 'headquarter_id', '3': 3, '4': 1, '5': 13, '10': 'headquarterId'},
  ],
};

/// Descriptor for `GetLastOrderByIdRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getLastOrderByIdRequestDescriptor = $convert.base64Decode('ChdHZXRMYXN0T3JkZXJCeUlkUmVxdWVzdBIbCglicmFuY2hfaWQYASABKA1SCGJyYW5jaElkEh0KCm9yZGVyX2NvZGUYAiABKARSCW9yZGVyQ29kZRIlCg5oZWFkcXVhcnRlcl9pZBgDIAEoDVINaGVhZHF1YXJ0ZXJJZA==');
@$core.Deprecated('Use getLastOrderByIdResponseDescriptor instead')
const GetLastOrderByIdResponse$json = const {
  '1': 'GetLastOrderByIdResponse',
  '2': const [
    const {'1': 'order_by_id', '3': 1, '4': 1, '5': 11, '6': '.payment_products_pb.paymentProducts.OrdersToProcess', '10': 'orderById'},
  ],
};

/// Descriptor for `GetLastOrderByIdResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getLastOrderByIdResponseDescriptor = $convert.base64Decode('ChhHZXRMYXN0T3JkZXJCeUlkUmVzcG9uc2USVAoLb3JkZXJfYnlfaWQYASABKAsyNC5wYXltZW50X3Byb2R1Y3RzX3BiLnBheW1lbnRQcm9kdWN0cy5PcmRlcnNUb1Byb2Nlc3NSCW9yZGVyQnlJZA==');
@$core.Deprecated('Use ordersToProcessDescriptor instead')
const OrdersToProcess$json = const {
  '1': 'OrdersToProcess',
  '2': const [
    const {'1': 'lista_items', '3': 1, '4': 3, '5': 11, '6': '.payment_products_pb.paymentProducts.Item', '10': 'listaItems'},
    const {'1': 'payer', '3': 2, '4': 1, '5': 11, '6': '.payment_products_pb.paymentProducts.Payer', '10': 'payer'},
    const {'1': 'orderStatus_type', '3': 3, '4': 1, '5': 9, '10': 'orderStatusType'},
    const {'1': 'order_status', '3': 4, '4': 1, '5': 13, '10': 'orderStatus'},
    const {'1': 'headquarters_name', '3': 5, '4': 1, '5': 9, '10': 'headquartersName'},
    const {'1': 'method_of_delivery', '3': 6, '4': 1, '5': 9, '10': 'methodOfDelivery'},
    const {'1': 'payment_method_options', '3': 7, '4': 1, '5': 9, '10': 'paymentMethodOptions'},
    const {'1': 'total_price', '3': 8, '4': 1, '5': 2, '10': 'totalPrice'},
    const {'1': 'order_date', '3': 9, '4': 1, '5': 9, '10': 'orderDate'},
    const {'1': 'order_time', '3': 10, '4': 1, '5': 9, '10': 'orderTime'},
    const {'1': 'order_code', '3': 11, '4': 1, '5': 4, '10': 'orderCode'},
  ],
};

/// Descriptor for `OrdersToProcess`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List ordersToProcessDescriptor = $convert.base64Decode('Cg9PcmRlcnNUb1Byb2Nlc3MSSgoLbGlzdGFfaXRlbXMYASADKAsyKS5wYXltZW50X3Byb2R1Y3RzX3BiLnBheW1lbnRQcm9kdWN0cy5JdGVtUgpsaXN0YUl0ZW1zEkAKBXBheWVyGAIgASgLMioucGF5bWVudF9wcm9kdWN0c19wYi5wYXltZW50UHJvZHVjdHMuUGF5ZXJSBXBheWVyEikKEG9yZGVyU3RhdHVzX3R5cGUYAyABKAlSD29yZGVyU3RhdHVzVHlwZRIhCgxvcmRlcl9zdGF0dXMYBCABKA1SC29yZGVyU3RhdHVzEisKEWhlYWRxdWFydGVyc19uYW1lGAUgASgJUhBoZWFkcXVhcnRlcnNOYW1lEiwKEm1ldGhvZF9vZl9kZWxpdmVyeRgGIAEoCVIQbWV0aG9kT2ZEZWxpdmVyeRI0ChZwYXltZW50X21ldGhvZF9vcHRpb25zGAcgASgJUhRwYXltZW50TWV0aG9kT3B0aW9ucxIfCgt0b3RhbF9wcmljZRgIIAEoAlIKdG90YWxQcmljZRIdCgpvcmRlcl9kYXRlGAkgASgJUglvcmRlckRhdGUSHQoKb3JkZXJfdGltZRgKIAEoCVIJb3JkZXJUaW1lEh0KCm9yZGVyX2NvZGUYCyABKARSCW9yZGVyQ29kZQ==');
