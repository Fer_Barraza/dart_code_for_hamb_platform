///
//  Generated code. Do not modify.
//  source: item_products.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class CategoryIdRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CategoryIdRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item_products_pb.itemProducts'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reqCategoryId', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reqBranchId', $pb.PbFieldType.OU3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reqHeadquarterId', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  CategoryIdRequest._() : super();
  factory CategoryIdRequest({
    $core.int? reqCategoryId,
    $core.int? reqBranchId,
    $core.int? reqHeadquarterId,
  }) {
    final _result = create();
    if (reqCategoryId != null) {
      _result.reqCategoryId = reqCategoryId;
    }
    if (reqBranchId != null) {
      _result.reqBranchId = reqBranchId;
    }
    if (reqHeadquarterId != null) {
      _result.reqHeadquarterId = reqHeadquarterId;
    }
    return _result;
  }
  factory CategoryIdRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CategoryIdRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CategoryIdRequest clone() => CategoryIdRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CategoryIdRequest copyWith(void Function(CategoryIdRequest) updates) => super.copyWith((message) => updates(message as CategoryIdRequest)) as CategoryIdRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CategoryIdRequest create() => CategoryIdRequest._();
  CategoryIdRequest createEmptyInstance() => create();
  static $pb.PbList<CategoryIdRequest> createRepeated() => $pb.PbList<CategoryIdRequest>();
  @$core.pragma('dart2js:noInline')
  static CategoryIdRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CategoryIdRequest>(create);
  static CategoryIdRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get reqCategoryId => $_getIZ(0);
  @$pb.TagNumber(1)
  set reqCategoryId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasReqCategoryId() => $_has(0);
  @$pb.TagNumber(1)
  void clearReqCategoryId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get reqBranchId => $_getIZ(1);
  @$pb.TagNumber(2)
  set reqBranchId($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReqBranchId() => $_has(1);
  @$pb.TagNumber(2)
  void clearReqBranchId() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get reqHeadquarterId => $_getIZ(2);
  @$pb.TagNumber(3)
  set reqHeadquarterId($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasReqHeadquarterId() => $_has(2);
  @$pb.TagNumber(3)
  void clearReqHeadquarterId() => clearField(3);
}

class ItemListResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemListResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item_products_pb.itemProducts'), createEmptyInstance: create)
    ..pc<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'listaItems', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  ItemListResponse._() : super();
  factory ItemListResponse({
    $core.Iterable<Item>? listaItems,
  }) {
    final _result = create();
    if (listaItems != null) {
      _result.listaItems.addAll(listaItems);
    }
    return _result;
  }
  factory ItemListResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemListResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemListResponse clone() => ItemListResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemListResponse copyWith(void Function(ItemListResponse) updates) => super.copyWith((message) => updates(message as ItemListResponse)) as ItemListResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemListResponse create() => ItemListResponse._();
  ItemListResponse createEmptyInstance() => create();
  static $pb.PbList<ItemListResponse> createRepeated() => $pb.PbList<ItemListResponse>();
  @$core.pragma('dart2js:noInline')
  static ItemListResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemListResponse>(create);
  static ItemListResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Item> get listaItems => $_getList(0);
}

class ItemStatusResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemStatusResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item_products_pb.itemProducts'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'statusSuccessful')
    ..hasRequiredFields = false
  ;

  ItemStatusResponse._() : super();
  factory ItemStatusResponse({
    $core.bool? statusSuccessful,
  }) {
    final _result = create();
    if (statusSuccessful != null) {
      _result.statusSuccessful = statusSuccessful;
    }
    return _result;
  }
  factory ItemStatusResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemStatusResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemStatusResponse clone() => ItemStatusResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemStatusResponse copyWith(void Function(ItemStatusResponse) updates) => super.copyWith((message) => updates(message as ItemStatusResponse)) as ItemStatusResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemStatusResponse create() => ItemStatusResponse._();
  ItemStatusResponse createEmptyInstance() => create();
  static $pb.PbList<ItemStatusResponse> createRepeated() => $pb.PbList<ItemStatusResponse>();
  @$core.pragma('dart2js:noInline')
  static ItemStatusResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemStatusResponse>(create);
  static ItemStatusResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get statusSuccessful => $_getBF(0);
  @$pb.TagNumber(1)
  set statusSuccessful($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatusSuccessful() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatusSuccessful() => clearField(1);
}

class Item extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Item', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item_products_pb.itemProducts'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currencyId')
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unitPrice', $pb.PbFieldType.OF)
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', $pb.PbFieldType.OU3)
    ..a<$core.int>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryId', $pb.PbFieldType.OU3)
    ..a<$core.int>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'headquarterId', $pb.PbFieldType.OU3)
    ..a<$core.int>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'branchId', $pb.PbFieldType.OU3)
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageName')
    ..aOS(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'category')
    ..hasRequiredFields = false
  ;

  Item._() : super();
  factory Item({
    $core.String? title,
    $core.String? description,
    $core.String? currencyId,
    $core.double? unitPrice,
    $core.int? itemId,
    $core.int? categoryId,
    $core.int? headquarterId,
    $core.int? branchId,
    $core.String? imageName,
    $core.String? category,
  }) {
    final _result = create();
    if (title != null) {
      _result.title = title;
    }
    if (description != null) {
      _result.description = description;
    }
    if (currencyId != null) {
      _result.currencyId = currencyId;
    }
    if (unitPrice != null) {
      _result.unitPrice = unitPrice;
    }
    if (itemId != null) {
      _result.itemId = itemId;
    }
    if (categoryId != null) {
      _result.categoryId = categoryId;
    }
    if (headquarterId != null) {
      _result.headquarterId = headquarterId;
    }
    if (branchId != null) {
      _result.branchId = branchId;
    }
    if (imageName != null) {
      _result.imageName = imageName;
    }
    if (category != null) {
      _result.category = category;
    }
    return _result;
  }
  factory Item.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Item.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Item clone() => Item()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Item copyWith(void Function(Item) updates) => super.copyWith((message) => updates(message as Item)) as Item; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Item create() => Item._();
  Item createEmptyInstance() => create();
  static $pb.PbList<Item> createRepeated() => $pb.PbList<Item>();
  @$core.pragma('dart2js:noInline')
  static Item getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Item>(create);
  static Item? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get title => $_getSZ(0);
  @$pb.TagNumber(1)
  set title($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTitle() => $_has(0);
  @$pb.TagNumber(1)
  void clearTitle() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get description => $_getSZ(1);
  @$pb.TagNumber(2)
  set description($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);

  @$pb.TagNumber(4)
  $core.String get currencyId => $_getSZ(2);
  @$pb.TagNumber(4)
  set currencyId($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(4)
  $core.bool hasCurrencyId() => $_has(2);
  @$pb.TagNumber(4)
  void clearCurrencyId() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get unitPrice => $_getN(3);
  @$pb.TagNumber(5)
  set unitPrice($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(5)
  $core.bool hasUnitPrice() => $_has(3);
  @$pb.TagNumber(5)
  void clearUnitPrice() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get itemId => $_getIZ(4);
  @$pb.TagNumber(6)
  set itemId($core.int v) { $_setUnsignedInt32(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasItemId() => $_has(4);
  @$pb.TagNumber(6)
  void clearItemId() => clearField(6);

  @$pb.TagNumber(7)
  $core.int get categoryId => $_getIZ(5);
  @$pb.TagNumber(7)
  set categoryId($core.int v) { $_setUnsignedInt32(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasCategoryId() => $_has(5);
  @$pb.TagNumber(7)
  void clearCategoryId() => clearField(7);

  @$pb.TagNumber(8)
  $core.int get headquarterId => $_getIZ(6);
  @$pb.TagNumber(8)
  set headquarterId($core.int v) { $_setUnsignedInt32(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasHeadquarterId() => $_has(6);
  @$pb.TagNumber(8)
  void clearHeadquarterId() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get branchId => $_getIZ(7);
  @$pb.TagNumber(9)
  set branchId($core.int v) { $_setUnsignedInt32(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasBranchId() => $_has(7);
  @$pb.TagNumber(9)
  void clearBranchId() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get imageName => $_getSZ(8);
  @$pb.TagNumber(10)
  set imageName($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(10)
  $core.bool hasImageName() => $_has(8);
  @$pb.TagNumber(10)
  void clearImageName() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get category => $_getSZ(9);
  @$pb.TagNumber(11)
  set category($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(11)
  $core.bool hasCategory() => $_has(9);
  @$pb.TagNumber(11)
  void clearCategory() => clearField(11);
}

class GetCategoriesListRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetCategoriesListRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item_products_pb.itemProducts'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reqBranchId', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reqHeadquarterId', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  GetCategoriesListRequest._() : super();
  factory GetCategoriesListRequest({
    $core.int? reqBranchId,
    $core.int? reqHeadquarterId,
  }) {
    final _result = create();
    if (reqBranchId != null) {
      _result.reqBranchId = reqBranchId;
    }
    if (reqHeadquarterId != null) {
      _result.reqHeadquarterId = reqHeadquarterId;
    }
    return _result;
  }
  factory GetCategoriesListRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetCategoriesListRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetCategoriesListRequest clone() => GetCategoriesListRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetCategoriesListRequest copyWith(void Function(GetCategoriesListRequest) updates) => super.copyWith((message) => updates(message as GetCategoriesListRequest)) as GetCategoriesListRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetCategoriesListRequest create() => GetCategoriesListRequest._();
  GetCategoriesListRequest createEmptyInstance() => create();
  static $pb.PbList<GetCategoriesListRequest> createRepeated() => $pb.PbList<GetCategoriesListRequest>();
  @$core.pragma('dart2js:noInline')
  static GetCategoriesListRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetCategoriesListRequest>(create);
  static GetCategoriesListRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get reqBranchId => $_getIZ(0);
  @$pb.TagNumber(1)
  set reqBranchId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasReqBranchId() => $_has(0);
  @$pb.TagNumber(1)
  void clearReqBranchId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get reqHeadquarterId => $_getIZ(1);
  @$pb.TagNumber(2)
  set reqHeadquarterId($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReqHeadquarterId() => $_has(1);
  @$pb.TagNumber(2)
  void clearReqHeadquarterId() => clearField(2);
}

class Category extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Category', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item_products_pb.itemProducts'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'resCategoriesId', $pb.PbFieldType.OU3)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'resCategoriesNombreId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'resCategoriesUrlimageId')
    ..hasRequiredFields = false
  ;

  Category._() : super();
  factory Category({
    $core.int? resCategoriesId,
    $core.String? resCategoriesNombreId,
    $core.String? resCategoriesUrlimageId,
  }) {
    final _result = create();
    if (resCategoriesId != null) {
      _result.resCategoriesId = resCategoriesId;
    }
    if (resCategoriesNombreId != null) {
      _result.resCategoriesNombreId = resCategoriesNombreId;
    }
    if (resCategoriesUrlimageId != null) {
      _result.resCategoriesUrlimageId = resCategoriesUrlimageId;
    }
    return _result;
  }
  factory Category.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Category.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Category clone() => Category()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Category copyWith(void Function(Category) updates) => super.copyWith((message) => updates(message as Category)) as Category; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Category create() => Category._();
  Category createEmptyInstance() => create();
  static $pb.PbList<Category> createRepeated() => $pb.PbList<Category>();
  @$core.pragma('dart2js:noInline')
  static Category getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Category>(create);
  static Category? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get resCategoriesId => $_getIZ(0);
  @$pb.TagNumber(1)
  set resCategoriesId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasResCategoriesId() => $_has(0);
  @$pb.TagNumber(1)
  void clearResCategoriesId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get resCategoriesNombreId => $_getSZ(1);
  @$pb.TagNumber(2)
  set resCategoriesNombreId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasResCategoriesNombreId() => $_has(1);
  @$pb.TagNumber(2)
  void clearResCategoriesNombreId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get resCategoriesUrlimageId => $_getSZ(2);
  @$pb.TagNumber(3)
  set resCategoriesUrlimageId($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasResCategoriesUrlimageId() => $_has(2);
  @$pb.TagNumber(3)
  void clearResCategoriesUrlimageId() => clearField(3);
}

class GetCategoriesListResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetCategoriesListResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item_products_pb.itemProducts'), createEmptyInstance: create)
    ..pc<Category>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'listCategories', $pb.PbFieldType.PM, subBuilder: Category.create)
    ..hasRequiredFields = false
  ;

  GetCategoriesListResponse._() : super();
  factory GetCategoriesListResponse({
    $core.Iterable<Category>? listCategories,
  }) {
    final _result = create();
    if (listCategories != null) {
      _result.listCategories.addAll(listCategories);
    }
    return _result;
  }
  factory GetCategoriesListResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetCategoriesListResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetCategoriesListResponse clone() => GetCategoriesListResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetCategoriesListResponse copyWith(void Function(GetCategoriesListResponse) updates) => super.copyWith((message) => updates(message as GetCategoriesListResponse)) as GetCategoriesListResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetCategoriesListResponse create() => GetCategoriesListResponse._();
  GetCategoriesListResponse createEmptyInstance() => create();
  static $pb.PbList<GetCategoriesListResponse> createRepeated() => $pb.PbList<GetCategoriesListResponse>();
  @$core.pragma('dart2js:noInline')
  static GetCategoriesListResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetCategoriesListResponse>(create);
  static GetCategoriesListResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Category> get listCategories => $_getList(0);
}

