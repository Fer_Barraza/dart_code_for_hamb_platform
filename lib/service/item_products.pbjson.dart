///
//  Generated code. Do not modify.
//  source: item_products.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use categoryIdRequestDescriptor instead')
const CategoryIdRequest$json = const {
  '1': 'CategoryIdRequest',
  '2': const [
    const {'1': 'req_category_id', '3': 1, '4': 1, '5': 13, '10': 'reqCategoryId'},
    const {'1': 'req_branch_id', '3': 2, '4': 1, '5': 13, '10': 'reqBranchId'},
    const {'1': 'req_headquarter_id', '3': 3, '4': 1, '5': 13, '10': 'reqHeadquarterId'},
  ],
};

/// Descriptor for `CategoryIdRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List categoryIdRequestDescriptor = $convert.base64Decode('ChFDYXRlZ29yeUlkUmVxdWVzdBImCg9yZXFfY2F0ZWdvcnlfaWQYASABKA1SDXJlcUNhdGVnb3J5SWQSIgoNcmVxX2JyYW5jaF9pZBgCIAEoDVILcmVxQnJhbmNoSWQSLAoScmVxX2hlYWRxdWFydGVyX2lkGAMgASgNUhByZXFIZWFkcXVhcnRlcklk');
@$core.Deprecated('Use itemListResponseDescriptor instead')
const ItemListResponse$json = const {
  '1': 'ItemListResponse',
  '2': const [
    const {'1': 'lista_items', '3': 1, '4': 3, '5': 11, '6': '.item_products_pb.itemProducts.Item', '10': 'listaItems'},
  ],
};

/// Descriptor for `ItemListResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemListResponseDescriptor = $convert.base64Decode('ChBJdGVtTGlzdFJlc3BvbnNlEkQKC2xpc3RhX2l0ZW1zGAEgAygLMiMuaXRlbV9wcm9kdWN0c19wYi5pdGVtUHJvZHVjdHMuSXRlbVIKbGlzdGFJdGVtcw==');
@$core.Deprecated('Use itemStatusResponseDescriptor instead')
const ItemStatusResponse$json = const {
  '1': 'ItemStatusResponse',
  '2': const [
    const {'1': 'status_successful', '3': 1, '4': 1, '5': 8, '10': 'statusSuccessful'},
  ],
};

/// Descriptor for `ItemStatusResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemStatusResponseDescriptor = $convert.base64Decode('ChJJdGVtU3RhdHVzUmVzcG9uc2USKwoRc3RhdHVzX3N1Y2Nlc3NmdWwYASABKAhSEHN0YXR1c1N1Y2Nlc3NmdWw=');
@$core.Deprecated('Use itemDescriptor instead')
const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'currency_id', '3': 4, '4': 1, '5': 9, '10': 'currencyId'},
    const {'1': 'unit_price', '3': 5, '4': 1, '5': 2, '10': 'unitPrice'},
    const {'1': 'item_id', '3': 6, '4': 1, '5': 13, '10': 'itemId'},
    const {'1': 'category_id', '3': 7, '4': 1, '5': 13, '10': 'categoryId'},
    const {'1': 'headquarter_id', '3': 8, '4': 1, '5': 13, '10': 'headquarterId'},
    const {'1': 'branch_id', '3': 9, '4': 1, '5': 13, '10': 'branchId'},
    const {'1': 'image_name', '3': 10, '4': 1, '5': 9, '10': 'imageName'},
    const {'1': 'category', '3': 11, '4': 1, '5': 9, '10': 'category'},
  ],
};

/// Descriptor for `Item`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDescriptor = $convert.base64Decode('CgRJdGVtEhQKBXRpdGxlGAEgASgJUgV0aXRsZRIgCgtkZXNjcmlwdGlvbhgCIAEoCVILZGVzY3JpcHRpb24SHwoLY3VycmVuY3lfaWQYBCABKAlSCmN1cnJlbmN5SWQSHQoKdW5pdF9wcmljZRgFIAEoAlIJdW5pdFByaWNlEhcKB2l0ZW1faWQYBiABKA1SBml0ZW1JZBIfCgtjYXRlZ29yeV9pZBgHIAEoDVIKY2F0ZWdvcnlJZBIlCg5oZWFkcXVhcnRlcl9pZBgIIAEoDVINaGVhZHF1YXJ0ZXJJZBIbCglicmFuY2hfaWQYCSABKA1SCGJyYW5jaElkEh0KCmltYWdlX25hbWUYCiABKAlSCWltYWdlTmFtZRIaCghjYXRlZ29yeRgLIAEoCVIIY2F0ZWdvcnk=');
@$core.Deprecated('Use getCategoriesListRequestDescriptor instead')
const GetCategoriesListRequest$json = const {
  '1': 'GetCategoriesListRequest',
  '2': const [
    const {'1': 'req_branch_id', '3': 1, '4': 1, '5': 13, '10': 'reqBranchId'},
    const {'1': 'req_headquarter_id', '3': 2, '4': 1, '5': 13, '10': 'reqHeadquarterId'},
  ],
};

/// Descriptor for `GetCategoriesListRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getCategoriesListRequestDescriptor = $convert.base64Decode('ChhHZXRDYXRlZ29yaWVzTGlzdFJlcXVlc3QSIgoNcmVxX2JyYW5jaF9pZBgBIAEoDVILcmVxQnJhbmNoSWQSLAoScmVxX2hlYWRxdWFydGVyX2lkGAIgASgNUhByZXFIZWFkcXVhcnRlcklk');
@$core.Deprecated('Use categoryDescriptor instead')
const Category$json = const {
  '1': 'Category',
  '2': const [
    const {'1': 'res_categories_id', '3': 1, '4': 1, '5': 13, '10': 'resCategoriesId'},
    const {'1': 'res_categories_nombre_id', '3': 2, '4': 1, '5': 9, '10': 'resCategoriesNombreId'},
    const {'1': 'res_categories_urlimage_id', '3': 3, '4': 1, '5': 9, '10': 'resCategoriesUrlimageId'},
  ],
};

/// Descriptor for `Category`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List categoryDescriptor = $convert.base64Decode('CghDYXRlZ29yeRIqChFyZXNfY2F0ZWdvcmllc19pZBgBIAEoDVIPcmVzQ2F0ZWdvcmllc0lkEjcKGHJlc19jYXRlZ29yaWVzX25vbWJyZV9pZBgCIAEoCVIVcmVzQ2F0ZWdvcmllc05vbWJyZUlkEjsKGnJlc19jYXRlZ29yaWVzX3VybGltYWdlX2lkGAMgASgJUhdyZXNDYXRlZ29yaWVzVXJsaW1hZ2VJZA==');
@$core.Deprecated('Use getCategoriesListResponseDescriptor instead')
const GetCategoriesListResponse$json = const {
  '1': 'GetCategoriesListResponse',
  '2': const [
    const {'1': 'list_categories', '3': 1, '4': 3, '5': 11, '6': '.item_products_pb.itemProducts.Category', '10': 'listCategories'},
  ],
};

/// Descriptor for `GetCategoriesListResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getCategoriesListResponseDescriptor = $convert.base64Decode('ChlHZXRDYXRlZ29yaWVzTGlzdFJlc3BvbnNlElAKD2xpc3RfY2F0ZWdvcmllcxgBIAMoCzInLml0ZW1fcHJvZHVjdHNfcGIuaXRlbVByb2R1Y3RzLkNhdGVnb3J5Ug5saXN0Q2F0ZWdvcmllcw==');
