///
//  Generated code. Do not modify.
//  source: pref_id_gen.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class PaymentToProcess extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PaymentToProcess', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pref_id_gen_pb.prefIdGen'), createEmptyInstance: create)
    ..pc<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'listaItems', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..aOM<Payer>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'payer', subBuilder: Payer.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'statementDescriptor')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'additionalInfo')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'methodOfDelivery')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'paymentMethodOptions')
    ..a<$core.double>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'totalPrice', $pb.PbFieldType.OF)
    ..a<$core.int>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'headquarterId', $pb.PbFieldType.O3)
    ..a<$core.int>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'branchId', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  PaymentToProcess._() : super();
  factory PaymentToProcess({
    $core.Iterable<Item>? listaItems,
    Payer? payer,
    $core.String? statementDescriptor,
    $core.String? additionalInfo,
    $core.String? methodOfDelivery,
    $core.String? paymentMethodOptions,
    $core.double? totalPrice,
    $core.int? headquarterId,
    $core.int? branchId,
  }) {
    final _result = create();
    if (listaItems != null) {
      _result.listaItems.addAll(listaItems);
    }
    if (payer != null) {
      _result.payer = payer;
    }
    if (statementDescriptor != null) {
      _result.statementDescriptor = statementDescriptor;
    }
    if (additionalInfo != null) {
      _result.additionalInfo = additionalInfo;
    }
    if (methodOfDelivery != null) {
      _result.methodOfDelivery = methodOfDelivery;
    }
    if (paymentMethodOptions != null) {
      _result.paymentMethodOptions = paymentMethodOptions;
    }
    if (totalPrice != null) {
      _result.totalPrice = totalPrice;
    }
    if (headquarterId != null) {
      _result.headquarterId = headquarterId;
    }
    if (branchId != null) {
      _result.branchId = branchId;
    }
    return _result;
  }
  factory PaymentToProcess.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PaymentToProcess.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PaymentToProcess clone() => PaymentToProcess()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PaymentToProcess copyWith(void Function(PaymentToProcess) updates) => super.copyWith((message) => updates(message as PaymentToProcess)) as PaymentToProcess; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PaymentToProcess create() => PaymentToProcess._();
  PaymentToProcess createEmptyInstance() => create();
  static $pb.PbList<PaymentToProcess> createRepeated() => $pb.PbList<PaymentToProcess>();
  @$core.pragma('dart2js:noInline')
  static PaymentToProcess getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PaymentToProcess>(create);
  static PaymentToProcess? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Item> get listaItems => $_getList(0);

  @$pb.TagNumber(2)
  Payer get payer => $_getN(1);
  @$pb.TagNumber(2)
  set payer(Payer v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasPayer() => $_has(1);
  @$pb.TagNumber(2)
  void clearPayer() => clearField(2);
  @$pb.TagNumber(2)
  Payer ensurePayer() => $_ensure(1);

  @$pb.TagNumber(4)
  $core.String get statementDescriptor => $_getSZ(2);
  @$pb.TagNumber(4)
  set statementDescriptor($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(4)
  $core.bool hasStatementDescriptor() => $_has(2);
  @$pb.TagNumber(4)
  void clearStatementDescriptor() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get additionalInfo => $_getSZ(3);
  @$pb.TagNumber(5)
  set additionalInfo($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(5)
  $core.bool hasAdditionalInfo() => $_has(3);
  @$pb.TagNumber(5)
  void clearAdditionalInfo() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get methodOfDelivery => $_getSZ(4);
  @$pb.TagNumber(6)
  set methodOfDelivery($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(6)
  $core.bool hasMethodOfDelivery() => $_has(4);
  @$pb.TagNumber(6)
  void clearMethodOfDelivery() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get paymentMethodOptions => $_getSZ(5);
  @$pb.TagNumber(7)
  set paymentMethodOptions($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(7)
  $core.bool hasPaymentMethodOptions() => $_has(5);
  @$pb.TagNumber(7)
  void clearPaymentMethodOptions() => clearField(7);

  @$pb.TagNumber(8)
  $core.double get totalPrice => $_getN(6);
  @$pb.TagNumber(8)
  set totalPrice($core.double v) { $_setFloat(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasTotalPrice() => $_has(6);
  @$pb.TagNumber(8)
  void clearTotalPrice() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get headquarterId => $_getIZ(7);
  @$pb.TagNumber(9)
  set headquarterId($core.int v) { $_setSignedInt32(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasHeadquarterId() => $_has(7);
  @$pb.TagNumber(9)
  void clearHeadquarterId() => clearField(9);

  @$pb.TagNumber(10)
  $core.int get branchId => $_getIZ(8);
  @$pb.TagNumber(10)
  set branchId($core.int v) { $_setSignedInt32(8, v); }
  @$pb.TagNumber(10)
  $core.bool hasBranchId() => $_has(8);
  @$pb.TagNumber(10)
  void clearBranchId() => clearField(10);
}

class Item extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Item', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pref_id_gen_pb.prefIdGen'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity', $pb.PbFieldType.OU3)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currencyId')
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unitPrice', $pb.PbFieldType.OF)
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', $pb.PbFieldType.OU3)
    ..a<$core.int>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categoryId', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  Item._() : super();
  factory Item({
    $core.String? title,
    $core.String? description,
    $core.int? quantity,
    $core.String? currencyId,
    $core.double? unitPrice,
    $core.int? itemId,
    $core.int? categoryId,
  }) {
    final _result = create();
    if (title != null) {
      _result.title = title;
    }
    if (description != null) {
      _result.description = description;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (currencyId != null) {
      _result.currencyId = currencyId;
    }
    if (unitPrice != null) {
      _result.unitPrice = unitPrice;
    }
    if (itemId != null) {
      _result.itemId = itemId;
    }
    if (categoryId != null) {
      _result.categoryId = categoryId;
    }
    return _result;
  }
  factory Item.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Item.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Item clone() => Item()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Item copyWith(void Function(Item) updates) => super.copyWith((message) => updates(message as Item)) as Item; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Item create() => Item._();
  Item createEmptyInstance() => create();
  static $pb.PbList<Item> createRepeated() => $pb.PbList<Item>();
  @$core.pragma('dart2js:noInline')
  static Item getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Item>(create);
  static Item? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get title => $_getSZ(0);
  @$pb.TagNumber(1)
  set title($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTitle() => $_has(0);
  @$pb.TagNumber(1)
  void clearTitle() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get description => $_getSZ(1);
  @$pb.TagNumber(2)
  set description($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get quantity => $_getIZ(2);
  @$pb.TagNumber(3)
  set quantity($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasQuantity() => $_has(2);
  @$pb.TagNumber(3)
  void clearQuantity() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get currencyId => $_getSZ(3);
  @$pb.TagNumber(4)
  set currencyId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCurrencyId() => $_has(3);
  @$pb.TagNumber(4)
  void clearCurrencyId() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get unitPrice => $_getN(4);
  @$pb.TagNumber(5)
  set unitPrice($core.double v) { $_setFloat(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasUnitPrice() => $_has(4);
  @$pb.TagNumber(5)
  void clearUnitPrice() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get itemId => $_getIZ(5);
  @$pb.TagNumber(6)
  set itemId($core.int v) { $_setUnsignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasItemId() => $_has(5);
  @$pb.TagNumber(6)
  void clearItemId() => clearField(6);

  @$pb.TagNumber(7)
  $core.int get categoryId => $_getIZ(6);
  @$pb.TagNumber(7)
  set categoryId($core.int v) { $_setUnsignedInt32(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasCategoryId() => $_has(6);
  @$pb.TagNumber(7)
  void clearCategoryId() => clearField(7);
}

class Payer extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Payer', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pref_id_gen_pb.prefIdGen'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'surname')
    ..aOM<Phone>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone', subBuilder: Phone.create)
    ..aOM<Address>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address', subBuilder: Address.create)
    ..hasRequiredFields = false
  ;

  Payer._() : super();
  factory Payer({
    $core.String? email,
    $core.String? name,
    $core.String? surname,
    Phone? phone,
    Address? address,
  }) {
    final _result = create();
    if (email != null) {
      _result.email = email;
    }
    if (name != null) {
      _result.name = name;
    }
    if (surname != null) {
      _result.surname = surname;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    if (address != null) {
      _result.address = address;
    }
    return _result;
  }
  factory Payer.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Payer.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Payer clone() => Payer()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Payer copyWith(void Function(Payer) updates) => super.copyWith((message) => updates(message as Payer)) as Payer; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Payer create() => Payer._();
  Payer createEmptyInstance() => create();
  static $pb.PbList<Payer> createRepeated() => $pb.PbList<Payer>();
  @$core.pragma('dart2js:noInline')
  static Payer getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Payer>(create);
  static Payer? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get surname => $_getSZ(2);
  @$pb.TagNumber(3)
  set surname($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSurname() => $_has(2);
  @$pb.TagNumber(3)
  void clearSurname() => clearField(3);

  @$pb.TagNumber(4)
  Phone get phone => $_getN(3);
  @$pb.TagNumber(4)
  set phone(Phone v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasPhone() => $_has(3);
  @$pb.TagNumber(4)
  void clearPhone() => clearField(4);
  @$pb.TagNumber(4)
  Phone ensurePhone() => $_ensure(3);

  @$pb.TagNumber(5)
  Address get address => $_getN(4);
  @$pb.TagNumber(5)
  set address(Address v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasAddress() => $_has(4);
  @$pb.TagNumber(5)
  void clearAddress() => clearField(5);
  @$pb.TagNumber(5)
  Address ensureAddress() => $_ensure(4);
}

class Phone extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Phone', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pref_id_gen_pb.prefIdGen'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'areaCode')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'number')
    ..hasRequiredFields = false
  ;

  Phone._() : super();
  factory Phone({
    $core.String? areaCode,
    $core.String? number,
  }) {
    final _result = create();
    if (areaCode != null) {
      _result.areaCode = areaCode;
    }
    if (number != null) {
      _result.number = number;
    }
    return _result;
  }
  factory Phone.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Phone.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Phone clone() => Phone()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Phone copyWith(void Function(Phone) updates) => super.copyWith((message) => updates(message as Phone)) as Phone; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Phone create() => Phone._();
  Phone createEmptyInstance() => create();
  static $pb.PbList<Phone> createRepeated() => $pb.PbList<Phone>();
  @$core.pragma('dart2js:noInline')
  static Phone getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Phone>(create);
  static Phone? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get areaCode => $_getSZ(0);
  @$pb.TagNumber(1)
  set areaCode($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAreaCode() => $_has(0);
  @$pb.TagNumber(1)
  void clearAreaCode() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get number => $_getSZ(1);
  @$pb.TagNumber(2)
  set number($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNumber() => $_has(1);
  @$pb.TagNumber(2)
  void clearNumber() => clearField(2);
}

class Address extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Address', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pref_id_gen_pb.prefIdGen'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zipCode')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'streetName')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'streetNumber', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  Address._() : super();
  factory Address({
    $core.String? zipCode,
    $core.String? streetName,
    $core.int? streetNumber,
  }) {
    final _result = create();
    if (zipCode != null) {
      _result.zipCode = zipCode;
    }
    if (streetName != null) {
      _result.streetName = streetName;
    }
    if (streetNumber != null) {
      _result.streetNumber = streetNumber;
    }
    return _result;
  }
  factory Address.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Address.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Address clone() => Address()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Address copyWith(void Function(Address) updates) => super.copyWith((message) => updates(message as Address)) as Address; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Address create() => Address._();
  Address createEmptyInstance() => create();
  static $pb.PbList<Address> createRepeated() => $pb.PbList<Address>();
  @$core.pragma('dart2js:noInline')
  static Address getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Address>(create);
  static Address? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get zipCode => $_getSZ(0);
  @$pb.TagNumber(1)
  set zipCode($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasZipCode() => $_has(0);
  @$pb.TagNumber(1)
  void clearZipCode() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get streetName => $_getSZ(1);
  @$pb.TagNumber(2)
  set streetName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStreetName() => $_has(1);
  @$pb.TagNumber(2)
  void clearStreetName() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get streetNumber => $_getIZ(2);
  @$pb.TagNumber(3)
  set streetNumber($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStreetNumber() => $_has(2);
  @$pb.TagNumber(3)
  void clearStreetNumber() => clearField(3);
}

class ItemStatusResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemStatusResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pref_id_gen_pb.prefIdGen'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'statusSuccessful')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'error')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'orderCode')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'prefIdGen')
    ..hasRequiredFields = false
  ;

  ItemStatusResponse._() : super();
  factory ItemStatusResponse({
    $core.bool? statusSuccessful,
    $core.String? error,
    $fixnum.Int64? orderCode,
    $core.String? prefIdGen,
  }) {
    final _result = create();
    if (statusSuccessful != null) {
      _result.statusSuccessful = statusSuccessful;
    }
    if (error != null) {
      _result.error = error;
    }
    if (orderCode != null) {
      _result.orderCode = orderCode;
    }
    if (prefIdGen != null) {
      _result.prefIdGen = prefIdGen;
    }
    return _result;
  }
  factory ItemStatusResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemStatusResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemStatusResponse clone() => ItemStatusResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemStatusResponse copyWith(void Function(ItemStatusResponse) updates) => super.copyWith((message) => updates(message as ItemStatusResponse)) as ItemStatusResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemStatusResponse create() => ItemStatusResponse._();
  ItemStatusResponse createEmptyInstance() => create();
  static $pb.PbList<ItemStatusResponse> createRepeated() => $pb.PbList<ItemStatusResponse>();
  @$core.pragma('dart2js:noInline')
  static ItemStatusResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemStatusResponse>(create);
  static ItemStatusResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get statusSuccessful => $_getBF(0);
  @$pb.TagNumber(1)
  set statusSuccessful($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatusSuccessful() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatusSuccessful() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get error => $_getSZ(1);
  @$pb.TagNumber(2)
  set error($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasError() => $_has(1);
  @$pb.TagNumber(2)
  void clearError() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get orderCode => $_getI64(2);
  @$pb.TagNumber(3)
  set orderCode($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasOrderCode() => $_has(2);
  @$pb.TagNumber(3)
  void clearOrderCode() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get prefIdGen => $_getSZ(3);
  @$pb.TagNumber(4)
  set prefIdGen($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPrefIdGen() => $_has(3);
  @$pb.TagNumber(4)
  void clearPrefIdGen() => clearField(4);
}

