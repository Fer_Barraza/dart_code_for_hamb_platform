///
//  Generated code. Do not modify.
//  source: pref_id_gen.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'pref_id_gen.pb.dart' as $0;
export 'pref_id_gen.pb.dart';

class PaymentProductsClient extends $grpc.Client {
  static final _$prefIdGenAndInsertProductsPurchased = $grpc.ClientMethod<
          $0.PaymentToProcess, $0.ItemStatusResponse>(
      '/pref_id_gen_pb.prefIdGen.PaymentProducts/PrefIdGenAndInsertProductsPurchased',
      ($0.PaymentToProcess value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ItemStatusResponse.fromBuffer(value));

  PaymentProductsClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ItemStatusResponse>
      prefIdGenAndInsertProductsPurchased($0.PaymentToProcess request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$prefIdGenAndInsertProductsPurchased, request,
        options: options);
  }
}

abstract class PaymentProductsServiceBase extends $grpc.Service {
  $core.String get $name => 'pref_id_gen_pb.prefIdGen.PaymentProducts';

  PaymentProductsServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.PaymentToProcess, $0.ItemStatusResponse>(
        'PrefIdGenAndInsertProductsPurchased',
        prefIdGenAndInsertProductsPurchased_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PaymentToProcess.fromBuffer(value),
        ($0.ItemStatusResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.ItemStatusResponse> prefIdGenAndInsertProductsPurchased_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PaymentToProcess> request) async {
    return prefIdGenAndInsertProductsPurchased(call, await request);
  }

  $async.Future<$0.ItemStatusResponse> prefIdGenAndInsertProductsPurchased(
      $grpc.ServiceCall call, $0.PaymentToProcess request);
}
