import 'dart:io';

import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/add_item_reaction/addcartchangelike_cubit.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/generate_payment/generate_payment_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/get_prod_db/get_prod_db_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/routes/routes.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/ad_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  final initFuture = MobileAds.instance.initialize();
  final adState = AdState(initFuture);
  runApp(Provider.value(
    value: adState,
    builder: (context, child) => MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent));
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (BuildContext context) => GetProdDbBloc()),
        BlocProvider(create: (BuildContext context) => CartItemsBloc()),
        BlocProvider(create: (BuildContext context) => GeneratePaymentBloc()),
        BlocProvider(
            create: (BuildContext context) => AddcartchangelikeCubit()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Hamburguesería',
        initialRoute: '/splash',
        routes: getApplicationRoutes(),
      ),
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
