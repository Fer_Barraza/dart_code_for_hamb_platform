

import 'package:codigo_hamburgueseria_flutter/src/pages/cart/cart_screen.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/purchase/purchase_screen.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/result_payment/result_page.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/shipping/shipping_screen.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/menu_botones/botones_page.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/chat/chat_page.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/detalle_page.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/pago/payment_page.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/splash/splash_screen.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/summary/sumary_screen.dart';
import 'package:flutter/material.dart';




Map<String, WidgetBuilder> getApplicationRoutes(){
  return <String, WidgetBuilder> {
    '/splash' : (BuildContext context ) => SplashScreen(),
    'menu' : (BuildContext context ) => BotonesPage(),
    'detalle_producto' : (BuildContext context ) => DetallePage(),
    'carrito' : (BuildContext context ) => CartScreen(),
    'envio_detalle' : (BuildContext context ) => ShippingScreen(),
    'resumen' : (BuildContext context ) => SumaryScreen(),
    'purchase' : (BuildContext context ) => PurchaseScreen(),
    'chat' : (BuildContext context ) => ChatPage(),
    'pago' : (BuildContext context ) => PaymentPage(),
    'result_payment' : (BuildContext context ) => ResultPage(),
    
    
  };
    
    }
    
