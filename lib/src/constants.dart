import 'package:flutter/material.dart';
import 'package:codigo_hamburgueseria_flutter/src/size_config.dart';
// import 'package:codigo_hamburgueseria_flutter/src/constants.dart';

//const kPrimaryColor = Color(0xFFFF7643);
const kPrimaryColor = Color(0xFFFF7643);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

const publicKey = "TEST-cd788559-1ebd-438a-9e68-5d47d58304a5";
List<String> shippingMethod = [
  'Delivery',
  'Retiro en sucursal',
];
List<String> paymentMethod = [
  'Efectivo',
  'Tarjeta',
];

const url = "https://wa.me/5491133552550";
const branchId = 1;
const headquarterId = 1;

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamelNullError = "Please Enter your name";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kAddressNullError = "Please Enter your address";

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: kTextColor),
  );
}

const Color generalColorTextDark = Colors.black;
const Color generalColorText = Colors.black87;
const Color generalColorSubText = Colors.black54;

const Color snackbarBackColor = Colors.greenAccent;

const Color appBarBackColor = Colors.transparent;
const Color appBarIconColor = Colors.black87;
const Color appBarTextColor = Colors.black87;

const Color defaultButtonTextColor = Color(0xFFF5F6F9);
const Color defaultButtonColor = Colors.orange;
const Color whatsAppButtonColor = Colors.green;
const Color defaultAlertButtonColor = Colors.red;
const Color defaultOtherColor = Colors.redAccent;

const Color boxDecorationBackgroundColor = Colors.grey;
const double boxDecorationBackgroundColorOpacity = 0.1;

const Color cartCardBackgroundColor = Colors.transparent;
const Color cartCardColorText = Colors.black87;

const Color checkoutCardBackgroundColor = Colors.white70;
const Color checkoutReceiptColor = Color(0xFFF5F6F9);
const Color checkoutBackgroundReceiptColor = Color(0xFFDADADA);
const double checkoutReceiptOpacity = 0.15;

// const Color backgroundGeneralColor = Color(0xFFFFE6E6);
const Color backgroundGeneralColor = Colors.white;
const Color backgroundMenuColor = Colors.white70;
const List<Color> gradientBox = [
  //Color.fromRGBO(236, 98, 188, 1.0),
  Colors.orangeAccent,
  //Color.fromRGBO(241, 142, 172, 1.0)
  Colors.deepOrangeAccent,
];
