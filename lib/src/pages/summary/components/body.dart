import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../size_config.dart';
import 'cart_card.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartItemsBloc, CartInfoPrefId>(
      builder: (context, state) {
        return SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 22),
                child: Container(
                  decoration: BoxDecoration(
                      color: boxDecorationBackgroundColor.withOpacity(boxDecorationBackgroundColorOpacity),
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(children: [
                    ListTile(
                      title: Text(
                          'Comprador: ${state.payer!.name} ${state.payer!.surname}'),
                    ),
                    ListTile(
                      title: Text('Entrega: ${state.methodOfDelivery}'),
                    ),
                    ListTile(
                      title: Text("Tipo de pago: ${state.paymentMethodOptions}"),
                    ),
                    ListTile(
                      title: Text('Celular: ${state.payer!.phone!.number}'),
                    ),
                    if (state.methodOfDelivery == "Delivery")
                      ListTile(
                          title: Text(
                        'Dirección: ${state.payer!.address!.streetName} ${state.payer!.address!.streetNumber}',
                      )),
                  ]),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.items.length,
                  itemBuilder: (context, index) => Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    // child: ListTile(
                    //   key: Key(new DateTime.now().millisecondsSinceEpoch.toString()),
                    // direction: DismissDirection.horizontal,
                    // onDismissed: (direction) {
                    //   setState(() {
                    //     context.read<CartItemsBloc>().add(OnItemDeleteChangeTotal(state.items[index].unitPrice));
                    //     print(state.totalPrice);
                    //     //print(state.payer.name);

                    //     state.items.removeAt(index);
                    //     //context.select((CartItemsBloc element) => element.state.totalPrice);

                    //   });
                    // },
                    // background: Container(
                    //   padding: EdgeInsets.symmetric(horizontal: 20),
                    //   decoration: BoxDecoration(
                    //     color: Color(0xFFFFE6E6),
                    //     borderRadius: BorderRadius.circular(15),
                    //   ),
                    //   child: Row(
                    //     children: [
                    //       Spacer(),
                    //       Container(),
                    //       FaIcon(FontAwesomeIcons.trash),
                    //       //SvgPicture.asset("assets/icons/Trash.svg"),
                    //     ],
                    //   ),
                    // ),
                    child: CartCard(index: index),
                    // ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
