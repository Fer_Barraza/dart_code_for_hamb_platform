import 'dart:async';

import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/generate_payment/generate_payment_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_payment.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/preferencias_usuario.dart';
//import 'package:codigo_hamburgueseria_flutter/src/services_singleton/preferencias_usuario.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_svg/flutter_svg.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/default_button.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mercado_pago_mobile_checkout/mercado_pago_mobile_checkout.dart';
//import 'package:flutter/services.dart';

//import '../../../constants.dart';
import '../../../size_config.dart';


class CheckoutCard extends StatefulWidget {
  const CheckoutCard({required this.totalPrice});
  final double totalPrice;

  @override
  _CheckoutCardState createState() => _CheckoutCardState();
}

class _CheckoutCardState extends State<CheckoutCard> {
  final prefs = new PreferenciasUsuario();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String? platformVersion = "";
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await MercadoPagoMobileCheckout.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    print("platformVersion");
    print(platformVersion);
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      //_platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: getProportionateScreenWidth(15),
        horizontal: getProportionateScreenWidth(30),
      ),
      // height: 174,
      decoration: BoxDecoration(
        color: checkoutCardBackgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          )
        ],
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  height: getProportionateScreenWidth(40),
                  width: getProportionateScreenWidth(40),
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(10),
                  ),
//                  child: Container(),
                  child: Center(child: FaIcon(FontAwesomeIcons.receipt)),
                  //child: SvgPicture.asset("assets/icons/receipt.svg"),
                ),
                Spacer(),
                //Text("Detalles de envío antes de continuar",),
                const SizedBox(width: 10),
                // Icon(
                //   Icons.info,
                //   size: 12,
                //   //color: kTextColor,
                // )
              ],
            ),
            SizedBox(height: getProportionateScreenHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text.rich(
                  TextSpan(
                    text: "Total:\n",
                    children: [
                      TextSpan(
                        text: "\$ ${widget.totalPrice.toString()}",
                        style: TextStyle(fontSize: 16, color: generalColorText),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: getProportionateScreenWidth(190),
                  child: BlocBuilder<GeneratePaymentBloc, GeneratePaymentState>(
                      builder: (context, state) {
                    var stateCart = context.read<CartItemsBloc>().state;
                    if (state.preferenceId == "" && stateCart.paymentMethodOptions == "Tarjeta") {
                      return DefaultButton(text: "Comprar", press: (){});
                    } else {
                      return DefaultButton(
                        text: "Comprar",
                        press: () async {
                          await GenPayment.generatePaymentCall(stateCart, context, state.preferenceId);
                        },
                      );
                    }
                  }),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
