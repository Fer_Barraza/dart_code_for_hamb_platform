// import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';



import 'package:codigo_hamburgueseria_flutter/src/components/app_bar.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
// import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
// import 'package:codigo_hamburgueseria_flutter/src/services_grpc/client/grpc_client.dart';
// import 'package:codigo_hamburgueseria_flutter/src/services_grpc/payment_products/payment_products_grpc.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:shop_app/models/Cart.dart';
// import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

import 'components/body.dart';
// import 'components/check_out_card.dart';

class PurchaseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: ()async{
          //print("intento de cancel");
          //await getOrderByOrderId(null).listen((event) { }).cancel();
          return true;
        },
          child: Scaffold(
        backgroundColor: backgroundGeneralColor,
        appBar: AppBars(context: context,texto: "Orden de compra",),
        body: Body(),
        //bottomNavigationBar: CheckoutCard(),
      ),
    );
  }
}
