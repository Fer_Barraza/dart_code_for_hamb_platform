import 'package:codigo_hamburgueseria_flutter/service/payment_products.pb.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/ad_banner.dart';
// import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/payment_products/payment_products_grpc.dart';
import 'package:codigo_hamburgueseria_flutter/src/size_config.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/sqlite_provider.dart';
import 'package:codigo_hamburgueseria_flutter/src/models/sql_db_model.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

// import '../../../size_config.dart';
import 'cart_card.dart';

// const url = "https://wa.me/5491133552550";

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.only(left: 18.0, right: 18.0),
          child: Column(
            children: [
              _lastPurchase(context),
              Divider(),
              // AdBanner(),
              _previousPurchases(context),
              // AdBanner(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _lastPurchase(BuildContext context) {
    return Container(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // SizedBox(height: 5.0),
            // Container(
            //     padding: EdgeInsets.only(left: 20.0, top: 18.0, bottom: 20),
            //     child: Text('Último pedido:',
            //         style: Theme.of(context).textTheme.headline5,
            //     ),
            // ),
            FutureBuilder(
              future: DBProvider.db.orderSelectLastOnly(),
              //initialData: InitialData,
              builder: (BuildContext context,
                  AsyncSnapshot<OrderSqliteModel?> snapshot) {
                if (!snapshot.hasData || snapshot.hasError) {
                  return Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      Center(
                          child: Container(
                        child: FaIcon(
                          FontAwesomeIcons.exclamationCircle,
                          size: 70,
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Para poder ver el listado de compras primero debe ordenar algo.',
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ));
                } else if (snapshot.data != null) {
                  final lastOrderFromList = snapshot.data;
                  return _streamDataBuilder(lastOrderFromList, context, true);
                } else {
                  return Container();
                }
              },
            )
          ]),
    );
  }
}



Widget _previousPurchases(BuildContext context) {
  return Container(
    child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 5.0),
          Container(
              padding: EdgeInsets.only(left: 20.0),
              child: Text('Historial de pedidos:',
                  style: Theme.of(context).textTheme.headline5)),
          FutureBuilder(
            future: DBProvider.db.newOrderSelect(),
            //initialData: InitialData,
            builder: (BuildContext context,
                AsyncSnapshot<List<OrderSqliteModel>> snapshot) {
              if (!snapshot.hasData || snapshot.hasError) {
                return Container();
                // return Center(
                //     child: Column(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: [
                //     SizedBox(
                //       height: 30,
                //     ),
                //     Center(
                //         child: Container(
                //       child: FaIcon(
                //         FontAwesomeIcons.exclamationCircle,
                //         size: 70,
                //       ),
                //     )),
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Text(
                //         'Para poder ver el listado de compras primero debe ordenar algo.',
                //         style: Theme.of(context).textTheme.caption,
                //       ),
                //     ),
                //     SizedBox(
                //       height: 20,
                //     ),
                //   ],
                // ));
              }
              List<OrderSqliteModel> prevOrdersList;
              prevOrdersList = snapshot.data!;

              if (prevOrdersList.length == 0 ||
                  prevOrdersList == [] /* || prevOrdersList == null */) {
                return Padding(
                  padding: const EdgeInsets.all(28.0),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                          child: Container(
                        child: FaIcon(
                          FontAwesomeIcons.exclamationCircle,
                          size: 70,
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'No hay información.',
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  )),
                );
              } else {
                //print(prevOrdersList[0].orderDate);
                return ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: prevOrdersList.length,
                  itemBuilder: (context, index) => Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: ExpansionTile(
                        maintainState: true,
                        title: Text(
                          prevOrdersList[index].orderDate!,
                          style: Theme.of(context).textTheme.caption,
                        ),
                        children: [
                          _streamDataBuilder(prevOrdersList[index], context, false),
                        ]),
                  ),
                );
              }
            },
          )
        ]),
  );
}

Widget _streamDataBuilder(
    OrderSqliteModel? orderPurchase, BuildContext context, bool isLastOrder) {
  if (orderPurchase == null || orderPurchase.orderId == null) {
    return Padding(
      padding: const EdgeInsets.all(28.0),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
              child: Container(
            child: FaIcon(
              FontAwesomeIcons.exclamationCircle,
              size: 70,
            ),
          )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'No hay información.',
              style: Theme.of(context).textTheme.caption,
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      )),
    );
  } else {
    //print(orderPurchase.orderId);
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Container(
        padding: const EdgeInsets.only(bottom: 8.0),
        decoration: BoxDecoration(
            color: boxDecorationBackgroundColor.withOpacity(0.2),
            borderRadius: BorderRadius.circular(10)),
        child: StreamBuilder(
          stream: getOrderByOrderId(orderPurchase.orderCode!),
          //initialData: initialData ,
          builder: (BuildContext context,
              AsyncSnapshot<GetLastOrderByIdResponse> snapshot) {
            if (snapshot.hasData) {
              final orderData = snapshot.data!;
              return OrderStatus(orderData: orderData, isLastOrder: isLastOrder,);
            } else if (snapshot.hasError) {
              print("tira error");
              return Center(
                child: AlertDialog(
                  title: Text("Error"),
                  content: Text(snapshot.error.toString()),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text("Atras"))
                  ],
                ),
              );
            }
            return Center(
                child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                //Expanded(
                Center(
                    child: Container(
                  child: CircularProgressIndicator(),
                )),
                //),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Cargando datos...',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ));
          },
        ),
      ),
    );
  }
}

class OrderStatus extends StatelessWidget {
  const OrderStatus({
    required this.orderData, required this.isLastOrder
  });

  final GetLastOrderByIdResponse orderData;
  final bool isLastOrder;

  @override
  Widget build(BuildContext context) {
    print("orderData.orderById.payer.address.streetNumber");
    print(orderData.orderById.payer.address.streetName);
    print(orderData.orderById.payer.address.streetNumber);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      // Container(
      //     padding: EdgeInsets.only(left: 20.0),
      //     child: Text('Último pedido',
      //         style: Theme.of(context).textTheme.subtitle1)
      // ),
      // ListTile(
      //   title: Text(
      //       'Comprador: ${orderData.orderById.payer.name} ${orderData.orderById.payer.surname}'),
      // ),
      Padding(
        padding: const EdgeInsets.only(left: 18.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          //mainAxisAlignment: MainAxisAlignment.end,
          children: [
            isLastOrder ? Container(
                padding: EdgeInsets.only(top: 18.0, bottom: 4.0),
                child: Text('Último pedido:',
                    style: Theme.of(context).textTheme.headline5))
                    : Container(),
            Divider(
              color: Colors.transparent,
            ),
            Text(
                'Comprador: ${orderData.orderById.payer.name} ${orderData.orderById.payer.surname}'),
            Divider(
              color: Colors.transparent,
            ),
            Text("Tipo de pago: ${orderData.orderById.paymentMethodOptions}"),
            Divider(
              color: Colors.transparent,
            ),
            Text('Entrega: ${orderData.orderById.methodOfDelivery}'),
            Divider(
              color: Colors.transparent,
            ),
            Text('Celular: ${orderData.orderById.payer.phone.number}'),
            Divider(
              color: Colors.transparent,
            ),
            if (orderData.orderById.methodOfDelivery == "Delivery")
              Text(
                'Dirección: ${orderData.orderById.payer.address.streetName} ${orderData.orderById.payer.address.streetNumber}',
              ),
            Divider(
              color: Colors.transparent,
            ),
            Text('Total: ${orderData.orderById.totalPrice.toString()}'),
            Divider(
              color: Colors.transparent,
            ),
            // Padding(
            //   padding: const EdgeInsets.all(18.0),
            //   child: Text('Este es su pedido final.'),
            // ),
          ],
        ),
      ),
      Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: orderData.orderById.listaItems.length,
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: CartCard(index: index, orderPurchase: orderData.orderById),
            // ),
          ),
        ),
      ),
      ListTile(
        title: Text(
            'Estado del pedido: ${_deliveryCases(orderData.orderById.orderStatus.toInt())}'),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: OutlinedButton(
            onPressed: () {},
            child: Center(child: Text("Cancelar pedido")),
            style: ElevatedButton.styleFrom(
              primary: defaultAlertButtonColor, // background
              onPrimary: defaultButtonTextColor, // foreground
            )),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: OutlinedButton(
            onPressed: () => _launchURL(),
            child: Center(child: Text("Whatsapp")),
            style: ElevatedButton.styleFrom(
              primary: whatsAppButtonColor, // background
              onPrimary: defaultButtonTextColor, // foreground
            )),
      ),
      // Padding(
      //   padding: const EdgeInsets.all(8.0),
      //   child: ElevatedButton(
      //       onPressed: () {
      //         // Navigator.pushNamed(context, 'chat');
      //       _launchURL();},
      //       child: Center(child: Text("Chat")),
      //     ),
      // ),
    ]);
  }
}

_launchURL() async {
  // const url = 'https://flutter.io';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'No se puede abrir url $url';
  }
}

String _deliveryCases(int statusDelivery) {
  switch (statusDelivery) {
    case 1:
      return "";
    // break;
    case 2:
      return "En camino";
    // break;
    case 3:
      return "";
    // break;
    case 4:
      return "Entregado";
    // break;
    case 5:
      return "";
    // break;
    case 6:
      return "";
    // break;
    default:
      return "";
  }
}
