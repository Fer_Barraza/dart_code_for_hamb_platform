

import 'package:codigo_hamburgueseria_flutter/service/payment_products.pb.dart';
import 'package:flutter/material.dart';
// import 'package:shop_app/models/Cart.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CartCard extends StatelessWidget {
  const CartCard({
    required this.index,
    required this.orderPurchase,
  });

  final int index;
  final OrdersToProcess orderPurchase;

  @override
  Widget build(BuildContext context) {
    //print(orderPurchase.listaItems[index].itemId);
    return Row(
      children: [
        SizedBox(
          width: 88,
          child: AspectRatio(
            aspectRatio: 0.88,
            child: Container(
                padding: EdgeInsets.all(getProportionateScreenWidth(10)),
                decoration: BoxDecoration(
                  color: Color(0xFFF5F6F9).withOpacity(0.4),
                  borderRadius: BorderRadius.circular(15),
                ),
                //child: Image.asset(state.items[index].imageNameOnlyForReference),
                //child: Container(),
                //child: Container(),
                child: FadeInImage(
                  image: NetworkImage('https://grpc-fabrikapps9.servehttp.com:30443/images/${orderPurchase.listaItems[index].categoryId}/${orderPurchase.listaItems[index].imageName}'), 
                  // image:
                  //     NetworkImage('assets/images/no-available/no-image.jpg'),
                  placeholder:
                      AssetImage('assets/images/no-available/loading.gif'),
                  fit: BoxFit.cover,
                  height: 160.0,
                )),
          ),
        ),
        SizedBox(width: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              orderPurchase.listaItems[index].title,
              style: TextStyle(color: generalColorText, fontSize: 16),
              maxLines: 2,
            ),
            SizedBox(height: 10),
            Text.rich(
              TextSpan(
                //text: "sdgsdgsd",
                text: "\$${orderPurchase.listaItems[index].unitPrice}",
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: kPrimaryColor),
                children: [
                  TextSpan(
                      text: " x${orderPurchase.listaItems[index].quantity}",
                      style: Theme.of(context).textTheme.bodyText1),
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}
