// import 'package:codigo_hamburgueseria_flutter/service/payment_products.pb.dart';
// import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
// import 'package:codigo_hamburgueseria_flutter/src/services_grpc/payment_products/payment_products_grpc.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:codigo_hamburgueseria_flutter/src/services_singleton/sqlite_provider.dart';
// import 'package:codigo_hamburgueseria_flutter/src/models/sql_db_model.dart';



// import '../../../size_config.dart';
// import 'cart_card.dart';

// class Body extends StatefulWidget {
//   @override
//   _BodyState createState() => _BodyState();
// }

// class _BodyState extends State<Body> {
//   @override
//   Widget build(BuildContext context) {
//     GetLastOrderByIdRequest req = GetLastOrderByIdRequest.create();
//     req.branchId = 1;
//     req.orderCode = 107;
//     return FutureBuilder(
//       future: DBProvider.db.newOrderSelect(),
//       //initialData: InitialData,
//       builder: (BuildContext context,
//           AsyncSnapshot<List<OrderSqliteModel>> snapshot) {
//         if (!snapshot.hasData) {
//           return Center(
//               child: Column(
//             children: [
//               Expanded(
//                 child: Center(
//                     child: Container(
//                   child: CircularProgressIndicator(),
//                 )),
//               ),
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Text(
//                   'Cargando datos...',
//                   style: Theme.of(context).textTheme.bodyText1,
//                 ),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//             ],
//           ));
//         } else if (snapshot.hasError) {
//           return Center(
//               child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Center(
//                   child: Container(
//                 child: FaIcon(
//                   FontAwesomeIcons.exclamationCircle,
//                   size: 70,
//                 ),
//               )),
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Text(
//                   'Hubo un problema al intentar conectarse. Intente nuevamente.',
//                   style: Theme.of(context).textTheme.caption,
//                 ),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//             ],
//           ));
//         }
//         final localOrdersList = snapshot.data;
//         if (localOrdersList.length == 0) {
//           return Center(
//               child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Center(
//                   child: Container(
//                 child: FaIcon(
//                   FontAwesomeIcons.exclamationCircle,
//                   size: 70,
//                 ),
//               )),
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Text(
//                   'No hay información.',
//                   style: Theme.of(context).textTheme.caption,
//                 ),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               StreamBuilder(
//                 stream: getOrderByOrderId(req),
//                 //initialData: initialData ,
//                 builder: (BuildContext context, AsyncSnapshot snapshot) {
//                   if(snapshot.hasData){
//                     print("hay data");
//                   }
//                   return Container(
//                     child: Container(),
//                   );
//                 },
//               ),
//             ],
//           ));
//         } else {
//           //GetLastOrderById
//           // GetLastOrderByIdRequest req = GetLastOrderByIdRequest.create();
//           // req.branchId = 1;
//           // req.orderCode = 107;
//           //getOrderByOrderIdF(req);
//           //getOrderByOrderId(req);

//           return ListView.builder(
//             itemCount: localOrdersList.length,
//             itemBuilder: (context, i) => ListTile(
//               //leading: Icon
//               title: Text(localOrdersList[i].orderId.toString()),
//             ),
//           );
//         }
//       },
//     );

//     // return BlocBuilder<CartItemsBloc, CartInfoPrefId>(
//     //   builder: (context, state) {
//     //     return SingleChildScrollView(
//     //       physics: ScrollPhysics(),
//     //       child: Column(
//     //         crossAxisAlignment: CrossAxisAlignment.start,
//     //         children: [
//     //           ListTile(
//     //             title: Text('Comprador: {state.payer.name} {state.payer.surname}'),
//     //             subtitle: Text('Entrega: {state.methodOfDelivery}'),
//     //             trailing: Text("Tipo de pago: {state.paymentMethodOptions}"),
//     //           ),
//     //           Padding(
//     //             padding: const EdgeInsets.all(18.0),
//     //             child: Column(
//     //               crossAxisAlignment: CrossAxisAlignment.start,
//     //               //mainAxisAlignment: MainAxisAlignment.end,
//     //               children: [
//     //                 if(state.methodOfDelivery == "Delivery") Text('Dirección: {state.payer.address.streetName} {state.payer.address.streetNumber}',),
//     //                 Divider(color: Colors.transparent,),
//     //                 Text('Celular: {state.payer.phone.number}'),
//     //               ],
//     //             ),
//     //           ),
//     //           Divider(),

//     //           Padding(
//     //             padding: const EdgeInsets.all(18.0),
//     //             child: Text('Este es su pedido final.'),
//     //           ),
//     //           Padding(
//     //             padding: EdgeInsets.symmetric(
//     //                 horizontal: getProportionateScreenWidth(20)),
//     //             child: ListView.builder(
//     //               physics: NeverScrollableScrollPhysics(),
//     //               shrinkWrap: true,
//     //               itemCount: state.items.length,
//     //               itemBuilder: (context, index) => Padding(
//     //                 padding: EdgeInsets.symmetric(vertical: 10),
//     //                 child: Container(),
//     //                 //child: CartCard(index: index),
//     //               ),
//     //             ),
//     //           ),

//     //         ],
//     //       ),
//     //     );
//     //   },
//     // );
//   }
// }
