import 'package:codigo_hamburgueseria_flutter/src/bloc/get_prod_db/get_prod_db_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_detalle_prod.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/heart_like.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_card_swipper/flutter_card_swiper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CardSwiper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return
        // GestureDetector(
        //   onDoubleTap: () {
        //     // BlocCallsDetProd.addItemToCart(context);
        //   },
        //   child:
        Container(
      padding: EdgeInsets.only(top: 10),
      child: BlocBuilder<GetProdDbBloc, GetProdDbState>(
        builder: (context, state) {
          if (state.productList?.tipos?.length != 1) {
            return Stack(children: [
              Swiper(
                itemWidth: _screenSize.width * 0.7,
                itemHeight: _screenSize.width * 1.0,
                itemBuilder: (BuildContext context, int index) {
                  String tempNameUrlImage =
                      state.productList!.tipos![index].getPosterImg();
                  return ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: FadeInImage(
                        image: NetworkImage(tempNameUrlImage),
                        placeholder: AssetImage(
                            'assets/images/no-available/loading.gif'),
                        fit: BoxFit.cover,
                        height: 160.0,
                      ));
                },
                itemCount: state.productList!.tipos!.length,
                layout: SwiperLayout.STACK,
                onIndexChanged: (value) {
                  context
                      .read<GetProdDbBloc>()
                      .add(OnChangeCurrentValueItem(value));
                },
              ),
              IgnorePointer(
                child: Center(
                  child: Container(
                    width: _screenSize.width * 0.7,
                    height: _screenSize.width * 1.0,
                    child: HeartLikeReaction(),
                  ),
                ),
              ),
            ]);
          } else if (state.productList?.tipos?.length == 1) {
            String tempNameUrlImage =
                state.productList!.tipos!.first.getPosterImg();
            return Container(
              child: Stack(children: [
                Container(
                  width: _screenSize.width * 0.7,
                  height: _screenSize.width * 1.0,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: FadeInImage(
                        image: NetworkImage(tempNameUrlImage),
                        placeholder: AssetImage(
                            'assets/images/no-available/loading.gif'),
                        fit: BoxFit.cover,
                        height: 160.0,
                      )),
                ),
                Container(
                    width: _screenSize.width * 0.7,
                    height: _screenSize.width * 1.0,
                    child: HeartLikeReaction()),
              ]),
            );
          }
          return CircularProgressIndicator();
        },
      ),
    );
  }
}
