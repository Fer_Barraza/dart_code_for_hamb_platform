

import 'package:animate_do/animate_do.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BotonNaranja extends StatelessWidget {
  final String texto;
  final double alto;
  final double ancho;
  final Color color;

  BotonNaranja(
      {required this.texto,
      this.alto = 50,
      this.ancho = 150,
      this.color = defaultButtonColor}
  );

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          alignment: Alignment.center,
          width: this.ancho,
          height: this.alto,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
          ),
          child: Row(
            children: [
              Expanded(child: Container()),

              Text(
                '$texto',
                style: TextStyle(color: defaultButtonTextColor),
              ),
              //Expanded(child: Container()),
              FaIcon(
                FontAwesomeIcons.shoppingCart,
                color: defaultButtonTextColor, /* size: 40 */
              ),
              Expanded(child: Container()),
            ],
          ),
        ),
        BlocBuilder<CartItemsBloc, CartInfoPrefId>(
          builder: (context, state) {
            //final algo = AnimationController(vsync: null);
            return Positioned(
              top: 10.0,
              //height: 20.0,
              left: 95.0,
              child: BounceInDown(
                from: 10,
                //delay: Duration( milliseconds: 800 ),
                child: Bounce(
                  from: 10,
                  //controller: AnimationController(vsync: BounceInDown(from: 10,)).forward(from:0.0);
                  child: Container(
                        child: Text(
                          state.contCart.toString(),
                          style: TextStyle(color: defaultButtonTextColor, fontSize: 11),
                        ),
                   
                    alignment: Alignment.center,
                    width: 16,
                    height: 16,
                    decoration: BoxDecoration(
                      color: defaultOtherColor,
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
