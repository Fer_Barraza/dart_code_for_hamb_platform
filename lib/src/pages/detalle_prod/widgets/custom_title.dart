

import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_detalle_prod.dart';
import 'package:flutter/material.dart';



class CustomTitle extends StatelessWidget {
  
  final String texto;

  CustomTitle({
    required this.texto
  });

  @override
  Widget build(BuildContext context) {
    return Center(
          child: Container(
            margin: EdgeInsets.only(top: 30),
            // width: double.infinity,
            child: 
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: <Widget>[

                Text( this.texto, style: TextStyle( fontSize: 30, fontWeight: FontWeight.w700 ), ),
                // GestureDetector(
                //   child: Icon( Icons.menu, size: 30 ),
                //   onTap: (){
                //     Navigator.of(context).pop();
                //     BlocCallsDetProd.onGetBackPage(context);
                //   },
                // )

              // ],
            ),
          // ),
        );
  }
}