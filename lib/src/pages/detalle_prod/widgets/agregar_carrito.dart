import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_detalle_prod.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/add_item_reaction/addcartchangelike_cubit.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/widgets/boton_naranja.dart';
import 'package:flutter/material.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/widgets/custom_widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AgregarCarritoBoton extends StatelessWidget {
  
  final double monto;

  AgregarCarritoBoton({ required this.monto });
  
  
  @override
  Widget build(BuildContext context) {
    var cubitNoPause = context.read<AddcartchangelikeCubit>();
    return Padding(
      padding: EdgeInsets.all( 10 ),
      child: Container(
        width: double.infinity,
        height: 100,
        decoration: BoxDecoration(
          color: boxDecorationBackgroundColor.withOpacity(boxDecorationBackgroundColorOpacity),
          borderRadius: BorderRadius.circular(100)
        ),

        child: Row(
          children: <Widget>[
            SizedBox( width: 20 ),
            Text('\$$monto', style: TextStyle( fontSize: 28, fontWeight: FontWeight.bold )),
            Spacer(),
              ClipRRect(
                borderRadius: BorderRadius.circular(13),
                child: Container(
                  color: defaultButtonColor,
                  child: Material(
                    borderRadius: BorderRadius.circular(13),
                    color: Colors.transparent,
                    child: InkWell( 
                      hoverColor: Colors.blue,
                      splashColor: Colors.red,
                      highlightColor: Colors.deepOrangeAccent,
                      focusColor: Colors.blue,
                      borderRadius: BorderRadius.circular(13),
                      onTap: () {
                        BlocCallsDetProd.addItemToCart(context);
                        cubitNoPause.addItemNotMorePaused();
                     },
                      child: Ink(
                        color: Colors.transparent,
                        child: BotonNaranja(texto: 'Llevar  '))),
                  ),
                ),
              ),
            SizedBox( width: 20 )
          ],
        ),
      ),
    );
  }
}