

import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';

class ProductoDescripcion extends StatelessWidget {
  
  final String/*!*/ titulo;
  final String descripcion;

  const ProductoDescripcion({
    required this.titulo,
    required this.descripcion
  });

  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric( horizontal: 30),
      alignment: Alignment.bottomLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox( height: 20 ),
          Text( this.titulo, style: TextStyle( color: generalColorText, fontSize: 30, fontWeight: FontWeight.w700,), ),
          SizedBox( height: 20 ),
          Text( this.descripcion, style: TextStyle( color: generalColorSubText, height: 1.6 ), ),

        ],
      ),
    );
  }
}