import 'package:codigo_hamburgueseria_flutter/src/bloc/add_item_reaction/addcartchangelike_cubit.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/get_prod_db/get_prod_db_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_detalle_prod.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/app_bar.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';

import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/widgets/agregar_carrito.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/widgets/card_swiper_widget.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/widgets/custom_title.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/widgets/prod_desc.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DetallePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        productDetailOnPop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: backgroundGeneralColor,
        appBar: AppBars(context: context,texto: "Productos",functionOnPress: productDetailOnPop),
        body: BlocBuilder<GetProdDbBloc, GetProdDbState>(
          builder: (context, state) {
            if (state.connectionState == "isLoading") {
              return Center(
                  child: Column(
                children: [
                  Expanded(child: Center(child: Container(child: CircularProgressIndicator(),)),),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Cargando datos...',style: Theme.of(context).textTheme.bodyText1,),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ));
            } 
            else if (state.connectionState == "error") {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                      Center(
                          child: Container(
                        child: FaIcon(FontAwesomeIcons.exclamationCircle,size: 70,),
                      )),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Hubo un problema al intentar conectarse. Intente nuevamente.',
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
              ));          
            } else if (state.connectionState == "successful") {
              return Column(
                children: <Widget>[
                  CustomTitle(
                      texto: state
                          .productList!.tipos![state.valueCurrentItem].category!),
                  SizedBox(height: 20),
                  Expanded(
                      child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: <Widget>[
                        CardSwiper(),
                        ProductoDescripcion(
                          titulo: state.productList!
                              .tipos![state.valueCurrentItem].nombreItem!,
                          descripcion: state.productList!
                              .tipos![state.valueCurrentItem].descripcion!,
                        ),
                      ],
                    ),
                  )),
                  AgregarCarritoBoton(
                      monto: state
                          .productList!.tipos![state.valueCurrentItem].price!),
                ],
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  void productDetailOnPop(BuildContext context) {
    BlocCallsDetProd.onGetBackPage(context);
    context.read<AddcartchangelikeCubit>().restartPaused();
  }
}
