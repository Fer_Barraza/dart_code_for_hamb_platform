
// import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';



import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:mercado_pago_mobile_checkout/mercado_pago_mobile_checkout.dart';

import 'package:codigo_hamburgueseria_flutter/src/bloc/generate_payment/generate_payment_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Get your app public from the credentials pages:
// https://www.mercadopago.com/mla/account/credentials
const publicKey = "TEST-cd788559-1ebd-438a-9e68-5d47d58304a5";

// The preferenceId should be fetch from your backend server. Do not
// expose your Access Token. Also you can create a preference using
// curl:
// ```bash
// curl -X POST \
//     'https://api.mercadopago.com/checkout/preferences?access_token=ACCESS_TOKEN' \
//     -H 'Content-Type: application/json' \
    // -d '{
    //       "items": [
    //           {
    //           "title": "Dummy Item",
    //           "description": "Multicolor Item",
    //           "quantity": 1,
    //           "currency_id": "ARS",
    //           "unit_price": 10.0
    //           }
    //       ],
    //       "payer": {
    //           "email": "payer@email.com"
    //       }
    // }'
// ```
//const preferenceId = context.read<GeneratePaymentBloc>().state;
//const preferenceId = "236976484-3a0b4fe3-2942-455b-91c8-04e124617665";
//const preferenceId = "236976484-e44921b1-d178-4e98-82c0-c406f19f568b";

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  String? _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String? platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await MercadoPagoMobileCheckout.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    print('hola6');
    var preferenceId = context.select((GeneratePaymentBloc element) => element.state.preferenceId);

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text('Running on: $_platformVersion\n'),
              ElevatedButton(
                onPressed: () async {
                  //context.read<GeneratePaymentBloc>().add(OnCallPrefGen(context.read<CartItemsBloc>().state));
                  //var preferenceId = context.select((GeneratePaymentBloc element) => element.state.preferenceId);

                  PaymentResult? result =
                      await MercadoPagoMobileCheckout.startCheckout(
                    publicKey,
                    preferenceId,
                  );
                  print(result.toString());
                },
                child: Text("Pagar"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
  //String _platformVersion = 'Unknown';
  // Tarjeta de prueba visa 4509 9535 6623 3704 - 123 - 11/25 
  // Tarjeta de prueba mastercard 5031 7557 3453 0604 - 123 - 11/25 
  // Tarjeta de prueba american 3711 803032 57522 - 1234 - 11/25
  // Tarjeta de prueba visa 4509 9535 6623 3704 mastercard 5031 7557 3453 0604 american 3711 803032 57522 
  // APRO - CONT - OTHE - CALL - FUND - SECU - EXPI - FORM - 

/* 

  I/flutter (18065): PaymentResult(result: done, id: 1233819211, status: approved, statusDetail: accredited, paymentMethodId: visa, paymentTypeId: credit_card, issuerId: null, installments: null, captured: null, liveMode: null, operationType: regular_payment, transactionAmount: 860, errorMessage: null)

  _$_PaymentResult (PaymentResult(result: done, id: 1233819211, status: approved, statusDetail: accredited, paymentMethodId: visa, paymentTypeId: credit_card, issuerId: null, installments: null, captured: null, liveMode: null, operationType: regular_payment, transactionAmount: 860, errorMessage: null))
  runtimeType:Type (_$_PaymentResult)
  hashCode:1454789781
  copyWith:__$PaymentResultCopyWithImpl (Instance of '__$PaymentResultCopyWithImpl<_PaymentResult>')
  transactionAmount:"860"
  statusDetail:"accredited"
  status:"approved"
  result:"done"
  paymentTypeId:"credit_card"
  paymentMethodId:"visa"
  operationType:"regular_payment"
  liveMode:null
  issuerId:null
  installments:null
  id:1233819211
  errorMessage:null
  captured:

    {
          "items": [
              {
              "title": "Dummy Item",
              "description": "Multicolor Item",
              "quantity": 1,
              "currency_id": "ARS",
              "unit_price": 10.5
              }
          ],
          "payer": {
              "email": "payer@email.com",
              "name": "Fernando",
              "surname": "Maradona",
              "phone": {
                "area_code": "12",
                "number": "21312312"
            },
              "address": {
                  "zip_code": "1853",
                  "street_name": "Rivadavia",
                  "street_number": 35326
                
            }
          },
          "payment_methods": {
            "excluded_payment_types":[
                {"id":"ticket"},
                {"id":"atm"}
            ],
            "installments": 1
        },
        "statement_descriptor": "dsgagadsgdagagd",
        "additional_info": "dsgagadsgdagagd"
    }
    
     */