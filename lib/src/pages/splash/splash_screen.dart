//import 'package:codigo_hamburgueseria_flutter/src/services_singleton/env-on-init.dart';



import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/splash/components/body.dart';
import 'package:codigo_hamburgueseria_flutter/src/size_config.dart';

class SplashScreen extends StatelessWidget {
  //static String routeName = "/splash";
  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    //productosServices.cargaLocal2();
    return Scaffold(
      backgroundColor: backgroundGeneralColor,
      body: Body(),
    );
  }
}
