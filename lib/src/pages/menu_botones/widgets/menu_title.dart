import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';

class MenuTitle extends StatelessWidget {
  const MenuTitle({
    Key? key,
    required this.titulo,
    this.subtitulo,
  }) : super(key: key);

  final String titulo;
  final String? subtitulo;

  @override
  Widget build(BuildContext context) {
    // final String titulo;
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(titulo,
                style: TextStyle(
                    color: generalColorText,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 10.0),
            (subtitulo == null)
                ? Container()
                : Text(subtitulo!,
                    style: TextStyle(color: generalColorSubText, fontSize: 18.0)),
          ],
        ),
      ),
    );
  }
}