import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class BackgroudMenu extends StatelessWidget {
  const BackgroudMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final gradiente = Container(
      width: double.infinity,
      height: double.infinity,
      color: backgroundMenuColor,
    );

    final cajaGradiente = Transform.rotate(
        angle: -pi / 5.0,
        child: Container(
          height: 360.0,
          width: 360.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80.0),
              gradient: LinearGradient(colors: gradientBox)),
        ));

    return Stack(
      children: <Widget>[
        gradiente,
        Positioned(top: -100.0, child: cajaGradiente)
      ],
    );
  }
}