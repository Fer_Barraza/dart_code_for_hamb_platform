import 'package:codigo_hamburgueseria_flutter/service/item_products.pb.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/menu_botones/widgets/categories_horizontal.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/item_products/get_products_desc_grpc.dart';
import 'package:flutter/material.dart';
// import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_menu.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
// import 'dart:ui';
// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

// class MenuItems extends StatelessWidget {
//   const MenuItems({
//     Key? key,
//     required this.context,
//   }) : super(key: key);

//   final BuildContext context;

//   @override
//   Widget build(BuildContext context) {
//     return Table(
//       children: [
//         TableRow(children: [
//           SingleItemOpt(
//               context: context,
//               color: Colors.blue,
//               iconoImg: 'hamburguesa_intro.png',
//               texto: 'Hamburguesas',
//               productsIdentifier: 0),
//           SingleItemOpt(
//               context: context,
//               color: Colors.purpleAccent,
//               iconoImg: 'bacon_cheddar_fries.png',
//               texto: 'Papas',
//               productsIdentifier: 1),
//         ]),
//         TableRow(children: [
//           SingleItemOpt(
//               context: context,
//               color: Colors.white60,
//               iconoImg: 'craft_beers.png',
//               texto: 'Cervezas',
//               productsIdentifier: 2),
//           SingleItemOpt(
//               context: context,
//               color: Colors.orange,
//               iconoImg: 'gaseosas.png',
//               texto: 'Gaseosas',
//               productsIdentifier: 3),
//         ]),
//         TableRow(children: [
//           SingleItemOpt(
//               context: context,
//               color: Colors.blueAccent,
//               iconoImg: 'helado.png',
//               texto: 'Helado',
//               productsIdentifier: 4),
//           SingleItemOpt(
//               context: context,
//               color: Colors.green,
//               iconoImg: 'hotdog.png',
//               texto: 'Panchos',
//               productsIdentifier: 5),
//         ]),
//         TableRow(children: [
//           SingleItemOpt(
//               context: context,
//               color: Colors.red,
//               iconoImg: 'golosinas_otros.png',
//               texto: 'Otros',
//               productsIdentifier: 6),
//           SingleItemOpt(
//               context: context,
//               color: Colors.teal,
//               iconoImg: 'descuento.png',
//               texto: 'Promos',
//               productsIdentifier: 7),
//         ])
//       ],
//     );
//   }
// }

// class SingleItemOpt extends StatelessWidget {
//   const SingleItemOpt({
//     Key? key,
//     required this.context,
//     required this.color,
//     required this.iconoImg,
//     required this.texto,
//     required this.productsIdentifier,
//   }) : super(key: key);

//   final BuildContext context;
//   final Color color;
//   final String iconoImg;
//   final String texto;
//   final int productsIdentifier;

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         //context.read<CurrentProdDescBloc>().add(OnDesactivarProducto());
//         // context.read<GetProdDbBloc>().add(OnCategorySelect(productsIdentifier+1));
//         BlocCallsMenu.onCatSelectGetProd(context, productsIdentifier);
//         Navigator.pushNamed(context, 'detalle_producto', arguments: {
//           'productArgument': texto,
//           'productsIdentifier': productsIdentifier
//         });
//       },
//       child: Container(
//         margin: EdgeInsets.all(15.0),
//         decoration: BoxDecoration(
//             //borderRadius: BorderRadius.circular(30.0),
//             //color: Colors.red,
//             ),
//         child: ClipRRect(
//           clipBehavior: Clip.antiAlias,
//           borderRadius: BorderRadius.circular(30.0),
//           child: BackdropFilter(
//             filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
//             child: Container(
//               height: 180.0,
//               //margin: EdgeInsets.all(15.0),
//               decoration: BoxDecoration(
//                   color: Color.fromRGBO(116, 117, 152, 0.2),
//                   borderRadius: BorderRadius.circular(20.0)),
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.spaceAround,
//                 children: <Widget>[
//                   SizedBox(height: 5.0),
//                   CircleAvatar(
//                     backgroundColor: color,
//                     radius: 35.0,
//                     child: Image.asset('assets/images/$iconoImg'),
//                   ),
//                   Text(texto, style: TextStyle(color: color)),
//                   SizedBox(height: 5.0)
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

/////////////////////
///
///

class NoonLoopingDemo extends StatelessWidget {
  const NoonLoopingDemo({
    required this.context,
  });

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    // List<int> text = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];
    // int maxRowNr = text.length ~/ 4;
    // int maxColNr = 4;

    // List<List<int>> numbers = [];
    // int cont = 0;
    // for (int i = 0; i <= maxRowNr; i++) {
    //   List<int> columnNumbers = [];

    //   for (int j = 0; j < maxColNr; j++) {
    //     if (text.length == cont){
    //       break;
    //     }
    //     columnNumbers.add(text[cont]);
    //     cont++;
    //   }

    //   numbers.add(columnNumbers);
    //   if (text.length == cont){
    //     break;
    //   }
    //   //columnNumbers = [];
    // }

    // print(numbers);

    return FutureBuilder(
      future: ItemProdCaller.getCategorieInfoForMenu(branchId, headquarterId),
      // initialData: InitialData,
      builder: (BuildContext context,
          AsyncSnapshot<GetCategoriesListResponse> snapshot) {
        if (snapshot.hasData) {
          var categoriesInfo = snapshot.data!.listCategories;
          int maxRowNr = categoriesInfo.length ~/ 4;
          int maxColNr = 4;
          List<List<Category>> categorieMatrix = [];
          int cont = 0;
          for (int i = 0; i <= maxRowNr; i++) {
            List<Category> colCategorieMatrix = [];
            for (int j = 0; j < maxColNr; j++) {
              if (categoriesInfo.length == cont) {
                break;
              }
              colCategorieMatrix.add(categoriesInfo[cont]);
              cont++;
            }
            categorieMatrix.add(colCategorieMatrix);
            if (categoriesInfo.length == cont) {
              break;
            }
          }
          // print(categorieMatrix);
          // List<List<Category>> categorieMatrix = [];
          //
          //

          return matrixCategoryBuilder(categoriesInfo, categorieMatrix);
        } else if (snapshot.hasError) {
          return Center(
              heightFactor: 6,
              child: Container(
                child: Text("Error"),
              ));
        } else {
          return Center(heightFactor: 6, child: CircularProgressIndicator());
        }
      },
    );
  }
}

Widget matrixCategoryBuilder(
    List<Category> listCategoryRow, List<List<Category>> categorieMatrix) {
  var matrixCategory = ListView.builder(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.only(left: 7.0, right: 7.0),
      shrinkWrap: true,

      // pageSnapping: false,
      // controller: _pageController,
      // children:
      //   _tarjetas(context),
      itemCount: categorieMatrix.length,
      itemBuilder: (context, i) {
        return CategoryHorizontal(
          categories: categorieMatrix[i],
        );
      });

  return Container(child: matrixCategory);
}
