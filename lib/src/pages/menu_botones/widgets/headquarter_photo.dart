import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';

class HeadquarterPhoto extends StatelessWidget {
  final bool fullScreen;

  HeadquarterPhoto({this.fullScreen = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(
          horizontal: (this.fullScreen) ? 5 : 30,
          vertical: (this.fullScreen) ? 5 : 0,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(38.0),
          // borderRadius: (!this.fullScreen)
          //     ? BorderRadius.circular(50)
          //     : BorderRadius.only(
          //         bottomLeft: Radius.circular(50),
          //         bottomRight: Radius.circular(50),
          //         topLeft: Radius.circular(40),
          //         topRight: Radius.circular(40)),
          child: Image(
            image: AssetImage("assets/images/d_twwr0p9ve-1024x683.jpg"),
          ),
        )
        // Container(
        //   width: double.infinity,
        //   // height: (this.fullScreen) ? 410 : 430,
        //   height: 230,
        //   decoration: BoxDecoration(
        //     color: Color(0xffFFCF53),
        //     borderRadius:
        //         (!this.fullScreen)
        //           ? BorderRadius.circular(50)
        //           : BorderRadius.only( bottomLeft: Radius.circular(50),
        //                                bottomRight: Radius.circular(50),
        //                                topLeft: Radius.circular(40),
        //                                topRight: Radius.circular(40))

        //   ),
        //   // child: Image(image: AssetImage("assets/images/d_twwr0p9ve-1024x683.jpg"))
        // ),
        );
  }
}
