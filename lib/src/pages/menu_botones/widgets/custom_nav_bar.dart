// import 'package:codigo_hamburgueseria_flutter/src/components/ad_intersticial.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/ad_state.dart';
import 'package:flutter/material.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

class CustomBottomNavBar extends StatefulWidget {
  const CustomBottomNavBar({
    Key? key,
    required this.context,
  }) : super(key: key);

  final BuildContext context;

  @override
  _CustomBottomNavBarState createState() => _CustomBottomNavBarState();
}

class _CustomBottomNavBarState extends State<CustomBottomNavBar> {
  InterstitialAd? _intersticialAd;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      setState(() {
        _intersticialAd = InterstitialAd(
          adUnitId: adState.intersticialUnitId,
          request: AdRequest(),
          listener: adState.adListener,
        )..load();
      });
    });
  }

  @override
  void dispose() {
    _intersticialAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      //borderRadius: BorderRadius.circular(30.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          //color: Colors.red,
          //border: Border.all(color: Colors.red)
        ),
        child: Theme(
          data: Theme.of(context).copyWith(

              //brightness: Brightness.light,
              //accentColorBrightness: Brightness.dark,
              canvasColor: backgroundGeneralColor,
              primaryColor: kPrimaryColor,
              textTheme: Theme.of(context).textTheme.copyWith(
                  caption:
                      TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)))),
          child: BottomNavigationBar(
            currentIndex: 0,
            onTap: (index) {
              switch (index) {
                case 0:
                  //Navigator.pushNamed(context , 'menu');
                  break;
                case 1:
                  Navigator.pushNamed(context, 'carrito');
                  break;
                case 2:
                  //_save();
                  Navigator.pushNamed(context, 'purchase');
                  // _intersticialAd.show()
                  if (_intersticialAd != null) {
                    () async {
                      // bool loadBool = await _intersticialAd!.isLoaded();
                      _intersticialAd!.show();
                      // if(loadBool == true){
                      //   print("1");
                      //   _intersticialAd!.show();
                      // }else{
                      //   print("2");
                      //   _intersticialAd!.load();
                      //   _intersticialAd!.show();
                      // }
                    }();                 
                  }
                  break;
              }
            },
            items: [
              BottomNavigationBarItem(
//                            icon: Icon( Icons.fastfood, size: 30.0 ),
                icon: FaIcon(FontAwesomeIcons.alignJustify),
                label: "Menú",
              ),
              BottomNavigationBarItem(
//                            icon: Icon( Icons.add_shopping_cart, size: 30.0 ),align-justify
                icon: FaIcon(FontAwesomeIcons.shoppingCart),
                label: "Carrito",
              ),
              BottomNavigationBarItem(
                icon: FaIcon(FontAwesomeIcons.angleDoubleRight),
                label: "Tu pedido",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
