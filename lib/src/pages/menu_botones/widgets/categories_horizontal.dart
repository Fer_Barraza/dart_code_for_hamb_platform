import 'package:codigo_hamburgueseria_flutter/service/item_products.pb.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_menu.dart';
import 'package:flutter/material.dart';

class CategoryHorizontal extends StatelessWidget {
  final List<Category> categories;

  // final Function siguientePagina;

  CategoryHorizontal({
    required this.categories,
    /* @required this.siguientePagina */
  });

  // final _pageController = new PageController(
  //   initialPage: 0,
  //   viewportFraction: 0.4,
  // );

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    // _pageController.addListener(() {
    //   if(_pageController.position.pixels >= _pageController.position.maxScrollExtent - 200){
    //     siguientePagina();
    //   }
    // });

    return Container(
      // margin: EdgeInsets.only(
      //   left: 15.0,right: 15.0,
      // ),
      height: _screenSize.height * 0.3,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.only(left: 7.0,right: 7.0),
        // pageSnapping: false,
        // controller: _pageController,
        // children:
        //   _tarjetas(context),
        itemCount: categories.length,
        itemBuilder: (context, i) {
          return _tarjeta(context, categories[i]);
        },
      ),
    );
  }

  Widget _tarjeta(BuildContext context, Category category) {
    // category.resCategoriesId = '${category.id}-poster';

    final categoryCard = Container(
      // margin: EdgeInsets.only(
      //   left: 15.0,
      // ),
      child: Column(
        children: <Widget>[
          Container(
            // tag: category.resCategoriesId,
            // height: 200,
            // width: 130,
            child: Padding(
              padding: const EdgeInsets.all(7.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                  placeholder:
                      AssetImage('assets/images/no-available/loading.gif'),
                  image: NetworkImage(category.resCategoriesUrlimageId),
                  fit: BoxFit.cover,
                  width: 130,
                  height: 200.0,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Text(
            category.resCategoriesNombreId,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );
    return GestureDetector(
        child: categoryCard,
        onTap: () {
          BlocCallsMenu.onCatSelectGetProd(
              context, categories.indexOf(category));
          Navigator.pushNamed(context, 'detalle_producto', arguments: {
            'productArgument': "texto",
            'productsIdentifier': categories.indexOf(category)
          });
        });
  }

  // List<Widget> _tarjetas(BuildContext context) {
  //   return categories.map((category){
  //     return Container(
  //       margin: EdgeInsets.only(right: 15.0,),
  //       child: Column(
  //         children: <Widget>[
  //           ClipRRect(
  //             borderRadius: BorderRadius.circular(20.0),
  //             child: FadeInImage(
  //               placeholder: AssetImage('assets/img/no-image.jpg'),
  //               image: NetworkImage(category.getPosterImg()),
  //               fit: BoxFit.cover,
  //               height: 160.0,
  //             ),
  //           ),
  //           SizedBox(height: 5.0,),
  //           Text(
  //             category.title,
  //             overflow: TextOverflow.ellipsis,
  //             style: Theme.of(context).textTheme.caption,
  //           )
  //         ],
  //       ),
  //     );
  //   }).toList();
  // }
}
