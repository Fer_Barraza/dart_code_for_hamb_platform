import 'package:codigo_hamburgueseria_flutter/src/components/successful_animation.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/cart/components/floating_menu_button.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/menu_botones/widgets/background_menu.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/menu_botones/widgets/custom_nav_bar.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/menu_botones/widgets/headquarter_photo.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/menu_botones/widgets/menu_items_categories.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/menu_botones/widgets/menu_title.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_cache_builder.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:google_fonts/google_fonts.dart';

// import 'package:shared_preferences/shared_preferences.dart';

class BotonesPage extends StatelessWidget {
  //int _currentIndex = 0;
  @override
  // void deactivate() {
  //   context.read<CurrentProdDescBloc>().add(OnDesactivarProducto());

  //   print('asdfadsfdsafa');
  //   super.deactivate();
  // }
  // void initState() {
  //   super.initState();
  //   print('llor2');
  //   //_currentIndex = 0;
  // }

  Widget build(BuildContext context) {
    // print(productosServices.productos[1].name);
    // print(productosServices.productos[1].tipos[0].price);
    //context.read<GetProdDbBloc>().add(OnGetBackToMenu());
    //context.select((GetProdDbBloc element) => element.state.copyWith(productoActivo: false,valueCurrentItem: 0));

    //final p = context.select((CurrentProdDescBloc c) => c.state.productoActivo == false);
    //print(p);
    //print('asdfafadfdsa');
    // final asset = AssetFlare(bundle: rootBundle, name: 'assets/Filip.flr');
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      //child: WillPopScope(
      //onWillPop: () async => false,
      child: Scaffold(
          body: Stack(
            children: <Widget>[
              ClipRect(
                //borderRadius: BorderRadius.only(bottomLeft: Radius.circular(100.0)),
                child: Container(child: BackgroudMenu()),
              ),
              SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    MenuTitle(titulo: 'Bienvenido a Lotomo!'),
                    // AnimatedTitle(),

                    // SizedBox(height:100),
                    HeadquarterPhoto(),
                    MenuTitle(
                        titulo: 'Compra lo que quieras',
                        subtitulo:
                            'Puedes pagar con efectivo o con tarjeta, retirar el pedido o esperar que llegue a tu casa.'),
                    // HeadquarterPhoto(),
                    // MenuItems(context: context),
                    //
                    // LikeReaction(),
                    NoonLoopingDemo(context: context),
                    // LikeReaction(),
                    // Container(
                    //   child: FlareCacheBuilder(
                    //     [asset],
                    //     builder: (context, bool isWarm) {
                    //       return FlareActor.asset(
                    //         asset,
                    //         alignment: Alignment.center,
                    //         fit: BoxFit.contain,
                    //         animation: 'idle',
                    //         // antialias: _useAA,
                    //       );
                    //     },
                    //   ),
                    // ),
                    // NoonLoopingDemo(context: context),
                  ],
                ),
              )
            ],
          ),
          // bottomNavigationBar: CustomBottomNavBar(context: context),
          floatingActionButton: FloatingMenuButton(),    
      ),
      // ),
    );
  }
}

class AnimatedTitle extends StatelessWidget {
  const AnimatedTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      // width: 250,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // const SizedBox(width: 20.0, height: 100.0),
          // const Text(
          //   'Be',
          //   style: TextStyle(fontSize: 43.0),
          // ),
          // const SizedBox(width: 20.0, height: 100.0),
          DefaultTextStyle(
            style:  GoogleFonts.roboto(
              textStyle: const TextStyle(
                color: generalColorText,
                fontSize: 20.0,
                // fontFamily: 'Horizon',
              ),
            ),
            child: AnimatedTextKit(
              repeatForever: true,
              animatedTexts: [
                RotateAnimatedText('SERVICIO DE DELIVERY'),
                RotateAnimatedText('RETIRO EN SUCURSAL'),
                RotateAnimatedText('PAGA CON TARJETA O EFECTIVO'),
              ],
              // onTap: () {
              //   print("Tap Event");
              // },
            ),
          ),
          // const SizedBox(width: 80.0, height: 100.0),
        ],
      ),
    );
  }
}

// _save() async {

//   // List<int> myListOfIntegers = [1,2,3,4];
//   // List<String> myListOfStrings=  myListOfIntegers.map((i)=>i.toString()).toList();

//   // SharedPreferences prefs = await SharedPreferences.getInstance();
//   // List<String> myList = (prefs.getStringList('mylist') ?? List<String>()) ;
//   // List<int> myOriginaList = myList.map((i)=> int.parse(i)).toList();
//   // print('Your list  $myOriginaList');
//   // await prefs.setStringList('mylist', myListOfStrings);
// }
