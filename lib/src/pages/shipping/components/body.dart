import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_shipping.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/ad_banner.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../constants.dart';

// import '../../../size_config.dart';
class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late TextEditingController _textControllerName;
  late TextEditingController _textControllerSurname;
  late TextEditingController _textControllerCellphone;
  late TextEditingController _textControllerEmail;
  late TextEditingController _textControllerAddress;
  late TextEditingController _textControllerStreetNumber;
  // TextEditingController? _textControllerStreetNumber;

  @override
  void initState() {
    _initTextControllers();
    // BlocCallsShipping.onShippingFormSubInit(context);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    BlocCallsShipping.onShippingFormSubInit(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _deactivateTextControllers();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: BlocBuilder<CartItemsBloc, CartInfoPrefId>(
        builder: (context, state) {
          return Container(
            child: ListView(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('El costo del delivery es de 50 pesos'),
                ),
                Divider(),
                _crearDropdownMethodShipping(),
                Divider(),
                _crearDropdownPaymentMethod(),
                Divider(),
                _crearInputName(),
                Divider(),
                _crearInputSurname(),
                Divider(),
                _crearPhoneNumber(),
                // Divider(),
                // AdBanner(),
                Divider(),
                _crearEmail(),
                Divider(),
                if (state.methodOfDelivery == "Delivery") ...[
                  _crearAddress(),
                  Divider(
                    color: Colors.transparent,
                  ),
                  _crearNumberAddress(),
                  Divider(),
                ],
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _crearInputName() {
    return TextField(
      controller: _textControllerName,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Nombre del comprador',
          labelText: 'Nombre',
          helperText: 'Obligatorio',
          icon: FaIcon(FontAwesomeIcons.user)),
      onChanged: (valor) {
        BlocCallsShipping.onShippingFormSubReplaceValue(
            context, valor, "nombreComprador");
        // context
        // .read<CartItemsBloc>()
        // .add(OnShippingFormSubmit(valor, "nombreComprador"));
        prefs.nombreComprador = valor;
      },
    );
  }

  Widget _crearInputSurname() {
    return TextField(
      controller: _textControllerSurname,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Apellido del comprador',
          labelText: 'Apellido',
          helperText: 'Obligatorio',
          icon: FaIcon(
            FontAwesomeIcons.user,
            color: Colors.transparent,
          )),
      onChanged: (valor) {
        BlocCallsShipping.onShippingFormSubReplaceValue(
            context, valor, "apellidoComprador");
        prefs.apellidoComprador = valor;
      },
    );
  }

  Widget _crearPhoneNumber() {
    return TextField(
      controller: _textControllerCellphone,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Celular del comprador',
          labelText: 'Celular',
          helperText: 'Obligatorio',
          icon: Icon(Icons.phone_android)),
      onChanged: (valor) {
        BlocCallsShipping.onShippingFormSubReplaceValue(
            context, valor, "celComprador");
        prefs.celComprador = valor;
      },
    );
  }

  Widget _crearEmail() {
    return TextField(
      controller: _textControllerEmail,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Email',
          labelText: 'Email',
          icon: FaIcon(
            FontAwesomeIcons.envelope,
          )),
      onChanged: (valor) {
        BlocCallsShipping.onShippingFormSubReplaceValue(
            context, valor, "mailComprador");
        prefs.mailComprador = valor;
      },
    );
  }

  Widget _crearAddress() {
    return TextField(
      controller: _textControllerAddress,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Dirección de envío',
          labelText: 'Dirección',
          icon: FaIcon(
            FontAwesomeIcons.mapMarkerAlt,
          )),
      onChanged: (valor) {
        BlocCallsShipping.onShippingFormSubReplaceValue(
            context, valor, "direccionComprador");
        prefs.direccionComprador = valor;
      },
    );
  }

  Widget _crearNumberAddress() {
    return TextField(
      controller: _textControllerStreetNumber,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
        hintText: 'Número de dirección',
        labelText: 'Número',
        helperText: 'Obligatorios',
        icon: FaIcon(
          FontAwesomeIcons.user,
          color: Colors.transparent,
        ),
      ),
      onChanged: (valor) {
        BlocCallsShipping.onShippingFormSubReplaceValue(
            context, valor, "streetNumber");
        prefs.streetNumberString = valor;
      },
    );
  }

  List<DropdownMenuItem<String>> getOpcionesDropdown(
      List<String> _listOptions) {
    List<DropdownMenuItem<String>> lista = new List.empty(growable: true);

    _listOptions.forEach((poder) {
      lista.add(DropdownMenuItem(
        child: Text(poder),
        value: poder,
      ));
    });

    return lista;
  }

  Widget _crearDropdownMethodShipping() {
    return Row(
      children: <Widget>[
        BlocBuilder<CartItemsBloc, CartInfoPrefId>(
          builder: (context, state) {
            if (state.methodOfDelivery == "Delivery") {
              return FaIcon(
                FontAwesomeIcons.shippingFast,
              );
            } else if (state.methodOfDelivery == "Retiro en sucursal") {
              return FaIcon(
                FontAwesomeIcons.solidHandPaper,
              );
            }
            return Container();
          },
        ),
        SizedBox(width: 30.0),
        Expanded(
          child: BlocBuilder<CartItemsBloc, CartInfoPrefId>(
            builder: (context, state) {
              return DropdownButton(
                disabledHint: Text('data'),
                hint: Text('Seleccione una opción de entrega'),
                value: state.methodOfDelivery,
                items: getOpcionesDropdown(shippingMethod),
                onChanged: (dynamic opt) {
                  if (opt != state.methodOfDelivery) {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    //_opcionSeleccionada = opt;
                    if (opt == 'Delivery') {
                      print('deliveryyy');
                      BlocCallsShipping.changeShippingMethod(context, opt);
                    } else if (opt == 'Retiro en sucursal') {
                      BlocCallsShipping.changeShippingMethod(context, opt);
                    }
                  }
                },
              );
            },
          ),
        )
      ],
    );
  }

  Widget _crearDropdownPaymentMethod() {
    return Row(
      children: <Widget>[
        BlocBuilder<CartItemsBloc, CartInfoPrefId>(
          builder: (context, state) {
            if (state.paymentMethodOptions == "Efectivo") {
              return FaIcon(
                FontAwesomeIcons.handHoldingUsd,
              );
            } else if (state.paymentMethodOptions == "Tarjeta") {
              return FaIcon(
                FontAwesomeIcons.creditCard,
              );
            }
            return Container();
          },
        ),
        SizedBox(width: 30.0),
        Expanded(
          child: BlocBuilder<CartItemsBloc, CartInfoPrefId>(
            builder: (context, state) {
              return DropdownButton(
                disabledHint: Text('data'),
                hint: Text('Seleccione una opción de pago'),
                value: state.paymentMethodOptions,
                items: getOpcionesDropdown(paymentMethod),
                onChanged: (dynamic opt) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  if (opt == 'Efectivo') {
                    BlocCallsShipping.changeMethodPayment(context, opt);
                  } else if (opt == 'Tarjeta') {
                    BlocCallsShipping.changeMethodPayment(context, opt);
                  }
                },
              );
            },
          ),
        )
      ],
    );
  }

  void _initTextControllers() {
    _textControllerName =
        new TextEditingController(text: prefs.nombreComprador);
    _textControllerSurname =
        new TextEditingController(text: prefs.apellidoComprador);
    _textControllerCellphone =
        new TextEditingController(text: prefs.celComprador);
    _textControllerEmail = new TextEditingController(text: prefs.mailComprador);
    _textControllerAddress =
        new TextEditingController(text: prefs.direccionComprador);
    _textControllerStreetNumber =
        new TextEditingController(text: prefs.streetNumberString);
  }

  void _deactivateTextControllers() {
    _textControllerName.dispose();
    _textControllerSurname.dispose();
    _textControllerCellphone.dispose();
    _textControllerEmail.dispose();
    _textControllerAddress.dispose();
    _textControllerStreetNumber.dispose();
  }
}
