

import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_shipping.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/default_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// import '../../../constants.dart';
import '../../../size_config.dart';

class CheckoutCard extends StatelessWidget {
  const CheckoutCard({
    required this.totalPrice
  });
  final double totalPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: getProportionateScreenWidth(15),
        horizontal: getProportionateScreenWidth(30),
      ),
      decoration: BoxDecoration(
        color: checkoutCardBackgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: checkoutBackgroundReceiptColor.withOpacity(checkoutReceiptOpacity),
          )
        ],
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  height: getProportionateScreenWidth(40),
                  width: getProportionateScreenWidth(40),
                  decoration: BoxDecoration(
                    color: checkoutReceiptColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                    child: Center(child: FaIcon(FontAwesomeIcons.receipt)),
                ),
                Spacer(),
                const SizedBox(width: 10),
              ],
            ),
            SizedBox(height: getProportionateScreenHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text.rich(
                  TextSpan(
                    text: "Total:\n",
                    children: [
                      TextSpan(
                        text: "\$ ${totalPrice.toString()}",
                        style: TextStyle(fontSize: 16, color: generalColorText),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: getProportionateScreenWidth(190),
                  child: DefaultButton(
                    text: "Siguiente",
                    press: () {
                      //var stateCart = context.select((CartItemsBloc element) => element.state);
                      BlocCallsShipping.getSummaryPage(context);
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
