import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_mp_pago_page.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/app_bar.dart';
// import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:codigo_hamburgueseria_flutter/src/bloc/generate_payment/generate_payment_bloc.dart';
import 'components/body.dart';

class ResultPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var algoe = context.read<CartItemsBloc>().state;
    //var algoo = context.select((CartItemsBloc element) => element.state);
    //print(algoe);
    //context.read<GeneratePaymentBloc>().add(OnCartPurchase(algoo));

    return WillPopScope(
        onWillPop: () async {
          // context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
          BlocCallsMpPayment.onGetBackPage(context);
          return true;
        },
        child: Scaffold(
          appBar: AppBars(
            context: context,
            texto: "",
          ),
          body: Container(
            child: BlocBuilder<GeneratePaymentBloc, GeneratePaymentState>(
              builder: (context, state) {
                if (state.connectionState == "isLoading") {
                  return Center(
                      child: Column(
                    children: [
                      Expanded(
                        child: Center(
                            child: Container(
                          child: CircularProgressIndicator(),
                        )),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Cargando datos...',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ));
                } else if (state.connectionState == "error") {
                  return Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                          child: Container(
                        child: FaIcon(
                          FontAwesomeIcons.exclamationCircle,
                          size: 70,
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Hubo un problema al intentar conectarse. Intente nuevamente.',
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ));
                } else if (state.connectionState == "successful") {
                  return Container(
                    child: Body(),
                  );
                }
                return Container();
              },
            ),
          ),
        ));
  }
}
