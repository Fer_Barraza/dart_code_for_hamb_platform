import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_result_page.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/successful_animation.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/default_button.dart';
import 'package:codigo_hamburgueseria_flutter/src/size_config.dart';
import 'package:flutter/services.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  final asset = AssetFlare(bundle: rootBundle, name: 'assets/images/Success Check (3).flr');

    return Center(
      child: Column(
        children: [
          Expanded(child: SizedBox(height: SizeConfig.screenHeight * 0.04)),
          SuccessfulReaction(asset:asset,animationName:'Untitled'),
          Expanded(child: SizedBox(height: SizeConfig.screenHeight * 0.08)),
          Text(
            "Compra realizada",
            style: TextStyle(
              fontSize: getProportionateScreenWidth(30),
              fontWeight: FontWeight.bold,
              color: generalColorText,
            ),
          ),
          Spacer(),
          Expanded(
                    child: SizedBox(
              width: SizeConfig.screenWidth * 0.6,
              child: DefaultButton(
                text: "Ir a menú",
                press: () {
                  // context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
                  BlocCallsResultPayment.onGetBackPage(context);
                  Navigator.pushReplacementNamed(context, 'menu');
                },
              ),
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
