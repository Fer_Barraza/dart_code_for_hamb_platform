import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/ad_state.dart';
import 'package:flutter/material.dart';

import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

class FloatingMenuButton extends StatefulWidget {
  const FloatingMenuButton({
    Key? key,
  }) : super(key: key);

  @override
  _FloatingMenuButtonState createState() => _FloatingMenuButtonState();
}

class _FloatingMenuButtonState extends State<FloatingMenuButton> {
  InterstitialAd? _intersticialAd;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      setState(() {
        _intersticialAd = InterstitialAd(
          adUnitId: adState.intersticialUnitId,
          request: AdRequest(),
          listener: adState.adListener,
        )..load();
      });
    });
  }

  @override
  void dispose() {
    _intersticialAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ValueNotifier<bool> isDialOpen = ValueNotifier(false);
    
    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
          return false;
        } else {
          return true;
        }
      },
      child: SpeedDial(
        /// both default to 16
        marginEnd: 18,
        marginBottom: 20,
        // animatedIcon: AnimatedIcons.menu_close,
        // animatedIconTheme: IconThemeData(size: 22.0),
        /// This is ignored if animatedIcon is non null
        icon: FontAwesomeIcons.alignJustify,
        activeIcon: FontAwesomeIcons.minus,
        // iconTheme: IconThemeData(color: Colors.grey[50], size: 30),
        /// The label of the main button.
        // label: Text("Open Speed Dial"),
        /// The active label of the main button, Defaults to label if not specified.
        // activeLabel: Text("Close Speed Dial"),
        /// Transition Builder between label and activeLabel, defaults to FadeTransition.
        // labelTransitionBuilder: (widget, animation) => ScaleTransition(scale: animation,child: widget),
        /// The below button size defaults to 56 itself, its the FAB size + It also affects relative padding and other elements
        buttonSize: 56.0,
        visible: true,
        openCloseDial: isDialOpen,

        /// If true user is forced to close dial manually
        /// by tapping main button and overlay is not rendered.
        closeManually: false,

        /// If true overlay will render no matter what.
        renderOverlay: false,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        tooltip: 'Speed Dial',
        heroTag: 'speed-dial-hero-tag',
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 8.0,
        shape: CircleBorder(),
        // orientation: SpeedDialOrientation.Up,
        // childMarginBottom: 2,
        // childMarginTop: 2,
        children: [
          SpeedDialChild(
            // labelBackgroundColor: Colors.transparent,
            child: Center(child: FaIcon(FontAwesomeIcons.alignJustify)),
            backgroundColor: Colors.transparent,
            // label: 'Menu',
            // labelStyle: TextStyle( fontSize: 18, fontWeight: FontWeight.w700, color: defaultButtonTextColor),
            onTap: () => print('FIRST CHILD'),
          ),
          SpeedDialChild(
            // labelStyle: TextStyle( fontSize: 18, fontWeight: FontWeight.w700, color: defaultButtonTextColor),
            // labelBackgroundColor: Colors.transparent,
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.shoppingCart,
            )),
            backgroundColor: Colors.transparent,
            // label: 'Carrito',
            onTap: () => Navigator.pushNamed(context, 'carrito'),
          ),
          SpeedDialChild(
            // labelStyle: TextStyle( fontSize: 18, fontWeight: FontWeight.w700, color: defaultButtonTextColor),
            // labelBackgroundColor: Colors.transparent,
            child: Center(child: FaIcon(FontAwesomeIcons.angleDoubleRight)),
            backgroundColor: Colors.transparent,
            // label: 'Compras',
            onTap: () {
              Navigator.pushNamed(context, 'purchase');
              if (_intersticialAd != null) {
                _intersticialAd!.show();
              }
            },
          ),
        ],
      ),
    );
  }
}
