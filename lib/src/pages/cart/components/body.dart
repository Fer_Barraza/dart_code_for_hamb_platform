

import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_cart.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//import 'package:flutter_svg/svg.dart';
//import 'package:shop_app/models/Cart.dart';

import '../../../components/ad_banner.dart';
import '../../../size_config.dart';
import 'cart_card.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartItemsBloc, CartInfoPrefId>(
      builder: (context, state) {
        return SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Desplace los items horizontalmente \n      para quitarlos del pedido.',),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.items.length,
                  itemBuilder: (context, index) => Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Dismissible(
                      key: Key(new DateTime.now().millisecondsSinceEpoch.toString()),
                      direction: DismissDirection.horizontal,
                      onDismissed: (direction) {
                        setState(() {
                          BlocCallsCart.onItemDelete(context, index, state);
                        });
                      },
                      background: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          color: backgroundGeneralColor,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Row(
                          children: [
                            Spacer(),
                            Container(),
                            FaIcon(FontAwesomeIcons.trash),
                          ],
                        ),
                      ),
                      child: CartCard(index: index),
                    ),
                  ),
                ),
              ),
              // Expanded(child: Container()),
              AdBanner(),

            ],
          ),
        );
      },
    );
  }
}
