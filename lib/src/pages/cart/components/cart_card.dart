import 'package:flutter/material.dart';
// import 'package:shop_app/models/Cart.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CartCard extends StatelessWidget {
  const CartCard({
    // Key key,
    required this.index,
  })
  //  : super(key: key)
  ;

  final int index;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartItemsBloc, CartInfoPrefId>(
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
          color: boxDecorationBackgroundColor.withOpacity(boxDecorationBackgroundColorOpacity),
          borderRadius: BorderRadius.circular(15)
        ),
          child: Row(
            children: [
              SizedBox(
                width: 88,
                child: AspectRatio(
                  aspectRatio: 0.88,
                  child: Container(
                    padding: EdgeInsets.all(getProportionateScreenWidth(10)),
                    decoration: BoxDecoration(
                      color: cartCardBackgroundColor,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    //child: Image.asset(state.items[index].imageNameOnlyForReference),
                    child: FadeInImage(
                      image: NetworkImage(
                          'https://grpc-fabrikapps9.servehttp.com:30443/images/${state.items[index].categoryId}/${state.items[index].imageNameOnlyForReference}'),
                      // image: NetworkImage('https://grpc-fabrikapps9.servehttp.com:29543/images/${state.items[index].categoryId}/${state.items[index].imageNameOnlyForReference}'),
                      placeholder:
                          AssetImage('assets/images/no-available/loading.gif'),
                      fit: BoxFit.cover,
                      height: 160.0,
                    ),
                  ),
                ),
              ),
              SizedBox(width: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    state.items[index].title!,
                    style: TextStyle(color: cartCardColorText, fontSize: 16),
                    maxLines: 2,
                  ),
                  SizedBox(height: 10),
                  Text.rich(
                    TextSpan(
                      //text: "sdgsdgsd",
                      text: "\$${state.items[index].unitPrice}",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                      children: [
                        TextSpan(
                            text: " x${state.items[index].quantity}",
                            style: Theme.of(context).textTheme.bodyText1),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
