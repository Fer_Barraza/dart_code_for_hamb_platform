// import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';



import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/preferencias_usuario.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_svg/flutter_svg.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/default_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../size_config.dart';

class CheckoutCard extends StatelessWidget {
  const CheckoutCard({
     required this.totalPrice
  });
  final double totalPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: getProportionateScreenWidth(15),
        horizontal: getProportionateScreenWidth(30),
      ),
      // height: 174,
      decoration: BoxDecoration(
        color: checkoutCardBackgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: checkoutBackgroundReceiptColor.withOpacity(0.15),
          )
        ],
      ),
      child: SafeArea(
        bottom: true,left: false,right: false,top: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  height: getProportionateScreenWidth(40),
                  width: getProportionateScreenWidth(40),
                  decoration: BoxDecoration(
                    color: checkoutReceiptColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                    child: Center(child: FaIcon(FontAwesomeIcons.receipt)),
                  //child: SvgPicture.asset("assets/icons/receipt.svg"),
                ),
                Spacer(),
                //Text("Detalles de envío antes de continuar",),
                const SizedBox(width: 10),
              ],
            ),
            SizedBox(height: getProportionateScreenHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text.rich(
                  TextSpan(
                    text: "Total:\n",
                    children: [
                      TextSpan(
                        text: "\$ ${totalPrice.toString()}",
                        style: TextStyle(fontSize: 16, color: generalColorText),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: getProportionateScreenWidth(190),
                  child: DefaultButton(
                    text: "Siguiente",
                    press: () async {
                      final prefs = new PreferenciasUsuario();
                      await prefs.initPrefs();
                      Navigator.pushReplacementNamed(context, 'envio_detalle');
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
