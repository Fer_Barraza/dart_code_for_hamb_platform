import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/app_bar.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
// import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:shop_app/models/Cart.dart';
// import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

import 'components/body.dart';
import 'components/check_out_card.dart';

class CartScreen extends StatelessWidget {
  static String routeName = "/cart";
  @override
  Widget build(BuildContext context) {
    final totalPriceString = context.select((CartItemsBloc element) => element.state.totalPrice);
    // print('totalPriceString');
    // print(totalPriceString);
    return Scaffold(
      backgroundColor: backgroundGeneralColor,
      appBar: AppBars(context: context,texto: "Tu compra",),
      body: Body(),
      bottomNavigationBar: CheckoutCard(totalPrice: totalPriceString),
    );
  }
}
