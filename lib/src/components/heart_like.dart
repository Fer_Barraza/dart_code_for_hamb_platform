// import 'package:flare_flutter/flare.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_cache_builder.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/add_item_reaction/addcartchangelike_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';




class HeartLikeReaction extends StatelessWidget {
  final asset = AssetFlare(
      bundle: rootBundle, name: 'assets/images/Like (heart effect).flr');


  @override
  Widget build(BuildContext context) {
// var cubitNoPause = context.read<AddcartchangelikeCubit>();

    return Container(
      height: 200,
      width: 200,
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
              child: FlareCacheBuilder(
            [asset],
            builder: (BuildContext context, bool isWarm) {
              return !isWarm
                  ? Container()
                  : BlocBuilder<AddcartchangelikeCubit, AddcartchangelikeState>(
                      builder: (context, state) {
                        if (state is NotMorePaused) {
                          return FlareActor.asset(
                            asset,
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            animation: "Untitled",
                            // antialias: true,
                            controller: _controls,
                            // isPaused: state.isPaused,
                            // shouldClip: true,
                          );
                        } else {
                          return Container();
                        }
                      },
                    );
            },
          )),
        ],
      )),
    );
  }
}

final FlareControls _controls = FlareControls();

class PlayLike {
  static void playSuccessAnimation() {
    _controls.play("Untitled");
  }
}
