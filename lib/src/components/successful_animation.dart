import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_cache_builder.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

class SuccessfulReaction extends StatelessWidget {
  final AssetFlare asset;
  final String animationName;

  const SuccessfulReaction(
      {Key? key, required this.asset, required this.animationName})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: 300,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
                child: FlareCacheBuilder(
              [asset],
              builder: (BuildContext context, bool isWarm) {
                return !isWarm
                    ? Container()
                    : FlareActor.asset(
                        asset,
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        animation: animationName,
                        // antialias: true,
                      );
              },
            )),
          ],
        ),
      ),
    );
  }
}
