// No se usa

import 'package:codigo_hamburgueseria_flutter/src/services_singleton/ad_state.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

class AdInterstitial extends StatefulWidget {
  @override
  _AdInterstitialState createState() => _AdInterstitialState();
}

class _AdInterstitialState extends State<AdInterstitial> {
  InterstitialAd? _intersticialAd;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      setState(() {
        _intersticialAd = InterstitialAd(
          adUnitId: adState.intersticialUnitId,
          request: AdRequest(),
          listener: adState.adListener,
        )..load();
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    if (_intersticialAd == null)
      return Container(child: Center(child: CircularProgressIndicator()));
    else
      return Container(child: FutureBuilder(
        future: _intersticialAd!.show(),
        // initialData: InitialData,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.hasData){
            _intersticialAd!.show();
            return Container();
          }else{
            return Container();
          }
        },
      ),);
  }

  @override
  void dispose() {
    _intersticialAd?.dispose();
    super.dispose();
  }
}
