import 'package:codigo_hamburgueseria_flutter/src/bloc/bloc_calls/bloc_calls_detalle_prod.dart';
import 'package:codigo_hamburgueseria_flutter/src/pages/detalle_prod/detalle_page.dart';
import 'package:flutter/material.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';

class AppBars extends AppBar {
  final BuildContext context;
  final String texto;
  Function? functionOnPress;
  AppBars({required this.context, required this.texto, functionOnPress})
      : super(
          iconTheme: IconThemeData(
            color: appBarIconColor, //change your color here
          ),
          backgroundColor: appBarBackColor,
          title: Container(
            margin: const EdgeInsets.only(left: 12),
            child: Text(
              texto,
              style: TextStyle( fontSize: 25, fontWeight: FontWeight.w700, color: appBarTextColor), 
            ),
          ),
          elevation: 0.0,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                Navigator.of(context).pop();
                if(functionOnPress != null)
                  functionOnPress(context);
              },
            ),
          ],
        );
}
