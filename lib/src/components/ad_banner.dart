import 'package:codigo_hamburgueseria_flutter/src/services_singleton/ad_state.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// Carga de aplicación	ca-app-pub-3940256099942544/3419835294
// Banner	ca-app-pub-3940256099942544/6300978111
// Intersticial	ca-app-pub-3940256099942544/1033173712
// Intersticial de vídeo	ca-app-pub-3940256099942544/8691691433
// Bonificado	ca-app-pub-3940256099942544/5224354917
// Intersticial bonificado	ca-app-pub-3940256099942544/5354046379
// Nativo avanzado	ca-app-pub-3940256099942544/2247696110
// Nativo avanzado de vídeo	ca-app-pub-3940256099942544/1044960115
class AdBanner extends StatefulWidget {
  @override
  _AdBannerState createState() => _AdBannerState();
}

class _AdBannerState extends State<AdBanner> {
  BannerAd? _bannerAd;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      setState(() {
        _bannerAd = BannerAd(
            size: AdSize.banner,
            adUnitId: adState.bannerUnitId,
            listener: adState.adListener,
            request: AdRequest())
          ..load();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_bannerAd == null)
      return SizedBox(height: 50);
    else
      return Container(
        height: 50,
        child: AdWidget(
          ad: _bannerAd!,
        ),
      );
  }
  @override
  void dispose() {
    _bannerAd?.dispose();
    super.dispose();
  }
}
