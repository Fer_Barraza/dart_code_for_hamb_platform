

part of 'cart_items_bloc.dart';

@immutable
abstract class CartItemsEvent {}

class OnItemPurchase extends CartItemsEvent {
  //final CartInfoPrefId cartinfoprefid;
  final Tipo currentItemPos;
  //int reqPos;

  OnItemPurchase(this.currentItemPos,);
}

class OnItemDeleteChangeTotal extends CartItemsEvent {
  final double? priceOfDelete;

  OnItemDeleteChangeTotal(this.priceOfDelete);
}
class OnItemDeleteDecrementQuantity extends CartItemsEvent {
  final int indexV;
  final List<Item> currentListItem;
  OnItemDeleteDecrementQuantity(this.indexV, this.currentListItem);
}

class OnShippingFormSubmit extends CartItemsEvent {
  final String value;
  final String contexto;

  OnShippingFormSubmit(this.value, this.contexto);
}

class OnShippingMethodChange extends CartItemsEvent {
  final String? shippingMethod;

  OnShippingMethodChange(this.shippingMethod,);
}
class OnMethodPaymentChange extends CartItemsEvent {
  final String? paymentMethod;

  OnMethodPaymentChange(this.paymentMethod,);
}


