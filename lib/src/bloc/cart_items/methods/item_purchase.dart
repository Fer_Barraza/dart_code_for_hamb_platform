// import 'package:bloc/bloc.dart';



import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
// import 'package:codigo_hamburgueseria_flutter/src/models/modelo_product_grpc_database.dart';

class ItemPurchase {
  static CartInfoPrefId onItemPurchase(
      OnItemPurchase event, CartInfoPrefId state) {
    List<Item> listNew = [];
    List<Item> listCurrent = state.items;
    double? totalPrice = state.totalPrice;
    bool isItemAdded = false;
    final temp = new Item(
        currencyId: "ARS",
        title: event.currentItemPos.nombreItem,
        description: event.currentItemPos.descripcion,
        quantity: 1,
        unitPrice: event.currentItemPos.price,
        imageNameOnlyForReference: event.currentItemPos.imageName,
        itemId: event.currentItemPos.id,
        categoryId: event.currentItemPos.categoryId);

    if (listCurrent.isEmpty) {
      listNew.add(temp);
      totalPrice = temp.unitPrice;
    } else {
      listCurrent.forEach((element) {
        if (element.title == temp.title) {
          var tempC = new Item(
            currencyId: element.currencyId,
            description: element.description,
            title: element.title,
            quantity: element.quantity! + 1,
            unitPrice: element.unitPrice,
            imageNameOnlyForReference: element.imageNameOnlyForReference,
            itemId: element.itemId,
            categoryId: element.categoryId,
          );
          listNew.add(tempC);
          isItemAdded = true;
          totalPrice = totalPrice! + element.unitPrice!;
        } else {
          var tempC = new Item(
            currencyId: element.currencyId,
            description: element.description,
            title: element.title,
            quantity: element.quantity,
            unitPrice: element.unitPrice,
            imageNameOnlyForReference: element.imageNameOnlyForReference,
            itemId: element.itemId,
            categoryId: element.categoryId,
          );
          listNew.add(tempC);
        }
      });
      if (isItemAdded == false) {
        listNew.add(temp);
        totalPrice = totalPrice! + temp.unitPrice!;
      }
    }
    var tempContCart = state.contCart;
    tempContCart++;
    var paymentMethods = PaymentMethods(excludedPaymentTypes: [
      ExcludedPaymentType(id: "ticket"),
      ExcludedPaymentType(id: "atm")
    ]);
    return state.copyWith(
        items: listNew,
        totalPrice: totalPrice,
        contCart: tempContCart,
        paymentMethods: paymentMethods);
  }
}
