

import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';

class UpdateForm {
  static CartInfoPrefId formUpdate(
      OnShippingFormSubmit event, CartInfoPrefId state) {
    if (state.payer == null) {
      return state.copyWith(
          payer: _formShippingCasesInitializerInNull(event));
    } else {
      return state.copyWith(
          payer: _formShippingCasesReplacement(event, state.payer),
          costoDelivery: state.costoDelivery);
    }
  }
}

//methods for cleaning the main code
Payer? _formShippingCasesReplacement(
    OnShippingFormSubmit event, Payer? payerTemp) {
  Payer? auxPayer;
  switch (event.contexto) {
    case "nombreComprador":
      {
        auxPayer = payerTemp!.copyWith(name: event.value);
      }
      break;
    case "apellidoComprador":
      {
        auxPayer = payerTemp!.copyWith(surname: event.value);
      }
      break;
    case "celComprador":
      {
        var tempPhoneObj = Phone(number: event.value);
        auxPayer = payerTemp!.copyWith(phone: tempPhoneObj);
      }
      break;
    case "mailComprador":
      {
        auxPayer = payerTemp!.copyWith(email: event.value);
      }
      break;
    case "direccionComprador":
      {
        if (payerTemp!.address == null) {
          var tempAddressObj = Address();
          auxPayer = payerTemp.copyWith(
              address: tempAddressObj.copyWith(streetName: event.value));
        } else {
          var tempAddressObj = payerTemp.address!;
          auxPayer = payerTemp.copyWith(
              address: tempAddressObj.copyWith(streetName: event.value));
        }
      }
      break;
    case "streetNumber":
      {
        if (payerTemp!.address == null) {
          var tempAddressObj = Address();
          if (event.value == "") {
            tempAddressObj.copyWith(streetNumber: null);

            auxPayer = payerTemp.copyWith(address: tempAddressObj);
          } else {
            try {
              int temp = int.parse(event.value);
              var auxPayerAddressChanged =
                  tempAddressObj.copyWith(streetNumber: temp);
              auxPayer = payerTemp.copyWith(address: auxPayerAddressChanged);
            } on Exception catch (e) {
              print("Debe ingresar un valor numerico ");
              print(e);
            }
          }
        } else {
          var tempAddressObj = payerTemp.address;
          if (event.value == "") {
            var auxPayerAddressChanged =
                tempAddressObj!.copyWith(streetNumber: null);

            auxPayer = payerTemp.copyWith(address: auxPayerAddressChanged);
          } else {
            try {
              int temp = int.parse(event.value);
              var auxPayerAddressChanged =
                  tempAddressObj!.copyWith(streetNumber: temp);
              auxPayer = payerTemp.copyWith(address: auxPayerAddressChanged);
            } on Exception catch (e) {
              print("Debe ingresar un valor numerico ");
              print(e);
            }
          }
        }
      }
      break;
  }
  return auxPayer;
}

Payer _formShippingCasesInitializerInNull(
    OnShippingFormSubmit event) {
  var payerTemp = Payer();
  switch (event.contexto) {
    case "nombreComprador":
      {
        payerTemp = payerTemp.copyWith(name: event.value);
      }
      break;
    case "apellidoComprador":
      {
        payerTemp.copyWith(surname: event.value);
      }
      break;
    case "celComprador":
      {
        var tempPhoneObj = Phone();
        tempPhoneObj.copyWith(number: event.value);

        payerTemp.copyWith(phone: tempPhoneObj);
      }
      break;
    case "mailComprador":
      {
        payerTemp.copyWith(email: event.value);
      }
      break;
    case "direccionComprador":
      {
        if (payerTemp.address == null) {
          var tempAddressObj = Address();
          tempAddressObj.copyWith(streetName: event.value);

          payerTemp.copyWith(address: tempAddressObj);
        } else {
          var tempAddressObj = payerTemp.address!;
          tempAddressObj.copyWith(streetName: event.value);

          payerTemp.copyWith(address: tempAddressObj);
        }
      }
      break;
    case "streetNumber":
      {
        if (payerTemp.address == null) {
          var tempAddressObj = Address();
          if (event.value == "") {
            tempAddressObj.copyWith(streetNumber: null);

            payerTemp.copyWith(address: tempAddressObj);
          } else {
            try {
              int temp = int.parse(event.value);
              tempAddressObj.copyWith(streetNumber: temp);
              payerTemp.copyWith(address: tempAddressObj);
            } on Exception catch (e) {
              print("Debe ingresar un valor numerico ");
              print(e);
            }
          }
        } else {
          var tempAddressObj = payerTemp.address;
          if (event.value == "") {
            tempAddressObj!.copyWith(streetNumber: null);

            payerTemp.copyWith(address: tempAddressObj);
          } else {
            try {
              int temp = int.parse(event.value);
              tempAddressObj!.copyWith(streetNumber: temp);
              payerTemp.copyWith(address: tempAddressObj);
            } on Exception catch (e) {
              print("Debe ingresar un valor numerico ");
              print(e);
            }
          }
        }
      }
      break;
  }
  return payerTemp;
}
