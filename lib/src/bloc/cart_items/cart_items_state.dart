

part of 'cart_items_bloc.dart';

// CartInfoPrefId cartInfoPrefIdFromJson(String str) => CartInfoPrefId.fromJson(json.decode(str));

// String cartInfoPrefIdToJson(CartInfoPrefId data) => json.encode(data.toJson());
@immutable
class CartInfoPrefId {
    CartInfoPrefId({
        this.items = const [],
        this.payer,
        this.paymentMethods /* = const PaymentMethods(installments: 1, excludedPaymentTypes: []) */,
        this.statementDescriptor = "Fabrikapps - Pedido de delivery",
        this.additionalInfo = "No hay información adicional",
        this.totalPrice = 0,
        this.contCart = 0,
        this.costoDelivery = 50,
        this.methodOfDelivery = "Retiro en sucursal",
        this.paymentMethodOptions = "Efectivo",
    });

    final List<Item> items;
    final Payer? payer;
    final PaymentMethods? paymentMethods;
    //final PaymentMethods paymentMethods = PaymentMethods(excludedPaymentTypes: [ExcludedPaymentType(id: "ticket"),ExcludedPaymentType(id: "atm")]);
    final String statementDescriptor;
    final String additionalInfo;
    final double totalPrice;
    final int contCart;
    final double costoDelivery;
    final String methodOfDelivery;
    final String paymentMethodOptions;

    CartInfoPrefId copyWith({
        List<Item>? items,
        Payer? payer,
        PaymentMethods? paymentMethods,
        String? statementDescriptor,
        String? additionalInfo,
        double? totalPrice,
        int? contCart,
        double? costoDelivery,
        String? methodOfDelivery,
        String? paymentMethodOptions,
    }) => 
        CartInfoPrefId(
            items: items ?? this.items,
            payer: payer ?? this.payer,
            paymentMethods: paymentMethods ?? this.paymentMethods,
            statementDescriptor: statementDescriptor ?? this.statementDescriptor,
            additionalInfo: additionalInfo ?? this.additionalInfo,
            totalPrice: totalPrice ?? this.totalPrice,
            contCart: contCart ?? this.contCart,
            costoDelivery: costoDelivery ?? this.costoDelivery,
            methodOfDelivery: methodOfDelivery ?? this.methodOfDelivery,
            paymentMethodOptions: paymentMethodOptions ?? this.paymentMethodOptions,
        );

        //CartInfoPrefId addI({List<Item> items,}) => CartInfoPrefId(items: this.items ?? this.items,);

    // factory CartInfoPrefId.fromJson(Map<String, dynamic> json) => CartInfoPrefId(
    //     items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
    //     payer: Payer.fromJson(json["payer"]),
    //     paymentMethods: PaymentMethods.fromJson(json["payment_methods"]),
    //     statementDescriptor: json["statement_descriptor"],
    //     additionalInfo: json["additional_info"],
    // );

    // Map<String, dynamic> toJson() => {
    //     "items": List<dynamic>.from(items.map((x) => x.toJson())),
    //     "payer": payer.toJson(),
    //     "payment_methods": paymentMethods.toJson(),
    //     "statement_descriptor": statementDescriptor,
    //     "additional_info": additionalInfo,
    // };
}

class Item {
    Item({
        this.title,
        this.description,
        this.quantity,
        this.currencyId,
        this.unitPrice,
        this.imageNameOnlyForReference,
        this.itemId,
        this.categoryId,
    });

    final String? title;
    final String? description;
    final int? quantity;
    final String? currencyId;
    final double? unitPrice;
    final String? imageNameOnlyForReference;
    final int? itemId;
    final int? categoryId;

    Item copyWith({
        String? title,
        String? description,
        int? quantity,
        String? currencyId,
        double? unitPrice,
        String? imageNameOnlyForReference,
        int? itemId,
        int? categoryId,
    }) => 
        Item(
            title: title ?? this.title,
            description: description ?? this.description,
            quantity: quantity ?? this.quantity,
            currencyId: currencyId ?? this.currencyId,
            unitPrice: unitPrice ?? this.unitPrice,
            imageNameOnlyForReference: imageNameOnlyForReference ?? this.imageNameOnlyForReference,
            itemId: itemId ?? this.itemId,
            categoryId: categoryId ?? this.categoryId,
        );

    // factory Item.fromJson(Map<String, dynamic> json) => Item(
    //     title: json["title"],
    //     description: json["description"],
    //     quantity: json["quantity"],
    //     currencyId: json["currency_id"],
    //     unitPrice: json["unit_price"].toDouble(),
    // );

    // Map<String, dynamic> toJson() => {
    //     "title": title,
    //     "description": description,
    //     "quantity": quantity,
    //     "currency_id": currencyId,
    //     "unit_price": unitPrice,
    // };
}

class Payer {
    Payer({
        this.email = "",
        this.name = "",
        this.surname = "",
        this.phone,
        this.address,
    });

     final String email;
     final String name; 
     final String surname;
     final Phone? phone;
     final Address? address;

    Payer copyWith({
        String? email,
        String? name,
        String? surname,
        Phone? phone,
        Address? address,
    }) => 
        Payer(
            email: email ?? this.email,
            name: name ?? this.name,
            surname: surname ?? this.surname,
            phone: phone ?? this.phone,
            address: address ?? this.address,
        );

    // factory Payer.fromJson(Map<String, dynamic> json) => Payer(
    //     email: json["email"],
    //     name: json["name"],
    //     surname: json["surname"],
    //     phone: Phone.fromJson(json["phone"]),
    //     address: Address.fromJson(json["address"]),
    // );

    // Map<String, dynamic> toJson() => {
    //     "email": email,
    //     "name": name,
    //     "surname": surname,
    //     "phone": phone.toJson(),
    //     "address": address.toJson(),
    // };
}

class Address {
    Address({
        this.zipCode = "N/A",
        this.streetName,
        this.streetNumber,
    });

    final String zipCode;
    final String? streetName;
    final int? streetNumber;

    Address copyWith({
        String? zipCode,
        String? streetName,
        int? streetNumber,
    }) => 
        Address(
            zipCode: zipCode ?? this.zipCode,
            streetName: streetName ?? this.streetName,
            streetNumber: streetNumber ?? this.streetNumber,
        );

    // factory Address.fromJson(Map<String, dynamic> json) => Address(
    //     zipCode: json["zip_code"],
    //     streetName: json["street_name"],
    //     streetNumber: json["street_number"],
    // );

    // Map<String, dynamic> toJson() => {
    //     "zip_code": zipCode,
    //     "street_name": streetName,
    //     "street_number": streetNumber,
    // };
}

class Phone {
    Phone({
        this.areaCode  = "+54 9 ",
        this.number = "",
    });

    final String areaCode;
    final String number;

    Phone copyWith({
        String? areaCode,
        String? number,
    }) => 
        Phone(
            areaCode: areaCode ?? this.areaCode,
            number: number ?? this.number,
        );

    // factory Phone.fromJson(Map<String, dynamic> json) => Phone(
    //     areaCode: json["area_code"],
    //     number: json["number"],
    // );

    // Map<String, dynamic> toJson() => {
    //     "area_code": areaCode,
    //     "number": number,
    // };
}

class PaymentMethods {
    PaymentMethods({
        this.excludedPaymentTypes ,
        this.installments = 1,
    });

    final List<ExcludedPaymentType>? excludedPaymentTypes ;
    final int installments;

    PaymentMethods copyWith({
        List<ExcludedPaymentType>? excludedPaymentTypes,
        int? installments,
    }) => 
        PaymentMethods(
            excludedPaymentTypes: excludedPaymentTypes ?? this.excludedPaymentTypes,
            installments: installments ?? this.installments,
        );

    // factory PaymentMethods.fromJson(Map<String, dynamic> json) => PaymentMethods(
    //     excludedPaymentTypes: List<ExcludedPaymentType>.from(json["excluded_payment_types"].map((x) => ExcludedPaymentType.fromJson(x))),
    //     installments: json["installments"],
    // );

    // Map<String, dynamic> toJson() => {
    //     "excluded_payment_types": List<dynamic>.from(excludedPaymentTypes.map((x) => x.toJson())),
    //     "installments": installments,
    // };
}

class ExcludedPaymentType {
    ExcludedPaymentType({
        this.id,
    });

    final String? id;

    ExcludedPaymentType copyWith({
        String? id,
    }) => 
        ExcludedPaymentType(
            id: id ?? this.id,
        );

    // factory ExcludedPaymentType.fromJson(Map<String, dynamic> json) => ExcludedPaymentType(
    //     id: json["id"],
    // );

    // Map<String, dynamic> toJson() => {
    //     "id": id,
    // };
}
