

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/methods/update_payer_form.dart';

import 'package:meta/meta.dart';
import 'package:codigo_hamburgueseria_flutter/src/models/modelo_product_grpc_database.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/methods/item_purchase.dart';

part 'cart_items_event.dart';
part 'cart_items_state.dart';

class CartItemsBloc extends Bloc<CartItemsEvent, CartInfoPrefId> {
  CartItemsBloc() : super(CartInfoPrefId());
  @override
  Stream<CartInfoPrefId> mapEventToState(
    CartItemsEvent event,
  ) async* {
    if (event is OnItemPurchase) {
      yield ItemPurchase.onItemPurchase(event, state);
    } else if (event is OnItemDeleteChangeTotal) {
      var tempContCart = state.contCart;
      tempContCart--;
      double tempPrice = state.totalPrice - event.priceOfDelete!;
      yield state.copyWith(totalPrice: tempPrice, contCart: tempContCart);
    } else if (event is OnItemDeleteDecrementQuantity) {
      List<Item> currentListItems = List<Item>.from(event.currentListItem);
      int auxQuant = event.currentListItem[event.indexV].quantity! - 1;

      var algo = currentListItems[event.indexV].copyWith(quantity: auxQuant);
      if (auxQuant == 0) {
        currentListItems.removeAt(event.indexV);
        yield state.copyWith(items: currentListItems);
      } else {
        currentListItems.replaceRange(event.indexV, event.indexV + 1, [algo]);
        yield state.copyWith(items: currentListItems);
      }
    } else if (event is OnShippingFormSubmit) {
      yield UpdateForm.formUpdate(event, state);
    } else if (event is OnShippingMethodChange) {
      double costoEnvio = state.costoDelivery;
      if (event.shippingMethod == "Delivery") {
        double tempTotal = state.totalPrice + costoEnvio;
        yield state.copyWith(
            methodOfDelivery: event.shippingMethod, totalPrice: tempTotal);
      } else if (event.shippingMethod == "Retiro en sucursal") {
        double tempTotal = state.totalPrice - costoEnvio;
        yield state.copyWith(
            methodOfDelivery: event.shippingMethod, totalPrice: tempTotal);
      }
    } else if (event is OnMethodPaymentChange) {
      yield state.copyWith(paymentMethodOptions: event.paymentMethod);
    }
  }
}
