

part of 'get_prod_db_bloc.dart';

@immutable
abstract class GetProdDbEvent {}

class OnCategorySelect extends GetProdDbEvent {
  final int reqId;
  OnCategorySelect(this.reqId);
}

class OnGetBackToMenu extends GetProdDbEvent {}

class OnChangeCurrentValueItem extends GetProdDbEvent {
  final int valueCurrentItem;
  OnChangeCurrentValueItem(this.valueCurrentItem);
}
