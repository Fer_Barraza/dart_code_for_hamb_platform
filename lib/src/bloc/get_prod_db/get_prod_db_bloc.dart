

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:codigo_hamburgueseria_flutter/service/item_products.pb.dart';
import 'package:codigo_hamburgueseria_flutter/src/models/modelo_product_grpc_database.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/item_products/get_products_desc_grpc.dart';
import 'package:meta/meta.dart';

part 'get_prod_db_event.dart';
part 'get_prod_db_state.dart';

class GetProdDbBloc extends Bloc<GetProdDbEvent, GetProdDbState> {
  GetProdDbBloc() : super(GetProdDbState());


  // @override
  // void onError(Object error, StackTrace stackTrace) {

  //   print('$error, $stackTrace');
  //   state.successful = false;
  //   super.onError(error, stackTrace);
  // }


  @override
  Stream<GetProdDbState> mapEventToState(
    GetProdDbEvent event, 
  ) async* {
    //bool isLoadingData = false;
    
    if (event is OnCategorySelect) {
       ItemListResponse? responseQueryGrpc;
      try {
        //isLoadingData = true;
        responseQueryGrpc = await ItemProdCaller.cargaLocalG(event.reqId);
      } catch (e) {
        // print('basta loco');
        // print(state.connectionState);


        //print(e.error);
        yield state.copyWith(
            productList: null,
            valueCurrentItem: 0,
            connectionState: "error",
        );

      } 
        if (responseQueryGrpc != null) {
          //isLoadingData = false;
          // print('basta loco2');
          // print(state.connectionState);  
          List<Tipo> productListItems = [];
          for (var i = 0; i < responseQueryGrpc.listaItems.length; i++) {
            Tipo productListItem = new Tipo();
          
            productListItem.category = responseQueryGrpc.listaItems[i].category;
            productListItem.id = responseQueryGrpc.listaItems[i].itemId;
            productListItem.categoryId =
                responseQueryGrpc.listaItems[i].categoryId;
            productListItem.nombreItem =
                responseQueryGrpc.listaItems[i].title;
            productListItem.imageName = responseQueryGrpc.listaItems[i].imageName;
            productListItem.descripcion =
                responseQueryGrpc.listaItems[i].description;
            productListItem.price = responseQueryGrpc.listaItems[i].unitPrice;
          
            productListItems.add(productListItem);
          }
          ProductDbModel productListItemsObject =
              new ProductDbModel(tipos: productListItems);
          
          yield state.copyWith(
              productList: productListItemsObject,
              valueCurrentItem: 0,
              connectionState: "successful",
          );
        }
    } else if (event is OnGetBackToMenu) {
      yield state.copyWith(valueCurrentItem: 0,connectionState: "isLoading");
    } else if (event is OnChangeCurrentValueItem) {
      yield state.copyWith(valueCurrentItem: event.valueCurrentItem);
    }
  }
}
