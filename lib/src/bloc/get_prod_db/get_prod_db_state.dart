

part of 'get_prod_db_bloc.dart';

//@immutable
class GetProdDbState {  
  final ProductDbModel? productList  ;
  final int? reqId;
  final int valueCurrentItem;
  String connectionState;
  GetProdDbState({
    this.productList,
    this.reqId,
    this.valueCurrentItem = 0,
    this.connectionState = "isLoading",
  });
  GetProdDbState copyWith({
    ProductDbModel? productList,
    int? reqId,
    int? valueCurrentItem,
    String? connectionState,


  }) => GetProdDbState(
    productList: productList ?? this.productList,
    reqId: reqId ?? this.reqId,
    valueCurrentItem: valueCurrentItem ?? this.valueCurrentItem,
    connectionState: connectionState ?? this.connectionState,
  );


}
