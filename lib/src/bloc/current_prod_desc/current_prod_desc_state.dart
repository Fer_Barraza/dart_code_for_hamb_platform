

part of 'current_prod_desc_bloc.dart';

@immutable
class CurrentProdDescState {

  final Tipo? producto;
  final bool productoActivo;

  CurrentProdDescState({
    this.producto,
    this.productoActivo = false,
  });

  CurrentProdDescState copyWith({
    Tipo? producto,
    bool? productoActivo,
  }) => CurrentProdDescState(
    producto: producto ?? this.producto,
    productoActivo: productoActivo ?? this.productoActivo,
    );
}