

part of 'current_prod_desc_bloc.dart';

@immutable
abstract class CurrentProdDescEvent {}

class OnSeleccionarProducto extends CurrentProdDescEvent {
  final Tipo producto;
  OnSeleccionarProducto(this.producto);
}
class OnDesactivarProducto extends CurrentProdDescEvent {

}