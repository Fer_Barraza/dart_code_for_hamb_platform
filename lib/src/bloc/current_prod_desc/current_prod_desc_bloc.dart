

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:codigo_hamburgueseria_flutter/src/models/modelo_product_local.dart';

part 'current_prod_desc_event.dart';
part 'current_prod_desc_state.dart';

class CurrentProdDescBloc extends Bloc<CurrentProdDescEvent, CurrentProdDescState> {
  CurrentProdDescBloc() : super(CurrentProdDescState());

  @override
  Stream<CurrentProdDescState> mapEventToState(
    CurrentProdDescEvent event,
  ) async* {
    if (event is OnSeleccionarProducto){
      yield state.copyWith(producto: event.producto, productoActivo: true);
    } else if (event is OnDesactivarProducto){
      yield state.copyWith(productoActivo: false);
    }

  }
}
