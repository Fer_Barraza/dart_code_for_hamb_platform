import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
// import 'package:codigo_hamburgueseria_flutter/src/bloc/add_item_reaction/addcartchangelike_cubit.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/get_prod_db/get_prod_db_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/components/heart_like.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';

class BlocCallsDetProd {
  static void addItemToCart(BuildContext context) {
    final tempItemCurrent = context
        .read<GetProdDbBloc>()
        .state
        .productList!
        .tipos![context.read<GetProdDbBloc>().state.valueCurrentItem];
    context.read<CartItemsBloc>().add(OnItemPurchase(tempItemCurrent));
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: const Text('Se ha agregado a su carrito.'),
        behavior: SnackBarBehavior.floating,
        backgroundColor: snackbarBackColor,
        duration: Duration(milliseconds: 1600),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(new Radius.circular(20))
),
        // elevation: 200,
        // elevation: 1000,
        margin: EdgeInsets.fromLTRB(0, 0, 0, 125),
      ),
    );

    PlayLike.playSuccessAnimation();
    
  }

  static void onGetBackPage(BuildContext context) {
    context.read<GetProdDbBloc>().add(OnGetBackToMenu());
  }
}
