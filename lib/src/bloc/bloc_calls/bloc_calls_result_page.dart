

import 'package:codigo_hamburgueseria_flutter/src/bloc/generate_payment/generate_payment_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocCallsResultPayment {
    static void onGetBackPage(BuildContext context) {
      context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
    }
}