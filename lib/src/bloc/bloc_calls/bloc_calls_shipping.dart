

import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/generate_payment/generate_payment_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/preferencias_usuario.dart';

final prefs = new PreferenciasUsuario();

class BlocCallsShipping {
  static void onShippingFormSubInit(BuildContext context) {
    context
        .read<CartItemsBloc>()
        .add(OnShippingFormSubmit(prefs.nombreComprador, "nombreComprador"));
    context.read<CartItemsBloc>().add(
        OnShippingFormSubmit(prefs.apellidoComprador, "apellidoComprador"));
    context
        .read<CartItemsBloc>()
        .add(OnShippingFormSubmit(prefs.celComprador, "celComprador"));
    context
        .read<CartItemsBloc>()
        .add(OnShippingFormSubmit(prefs.mailComprador, "mailComprador"));
    context.read<CartItemsBloc>().add(
        OnShippingFormSubmit(prefs.direccionComprador, "direccionComprador"));
    context
        .read<CartItemsBloc>()
        .add(OnShippingFormSubmit(prefs.streetNumberString, "streetNumber"));
  }

  static void onShippingFormSubReplaceValue(
      BuildContext context, String value, String ctxKey) {
    context.read<CartItemsBloc>().add(OnShippingFormSubmit(value, ctxKey));
  }

  static void changeShippingMethod(BuildContext context, String? value) {
    context.read<CartItemsBloc>().add(OnShippingMethodChange(value));
  }
  static void changeMethodPayment(BuildContext context, String? value) {
    context.read<CartItemsBloc>().add(OnMethodPaymentChange(value));
  }

  static void getSummaryPage(BuildContext context) {
    //var stateCart = context.select((CartItemsBloc element) => element.state);
    CartInfoPrefId stateCart = context.read<CartItemsBloc>().state;
    if (stateCart.paymentMethodOptions == "Tarjeta") {
      context.read<GeneratePaymentBloc>().add(OnCallPrefGen(stateCart));
      Navigator.pushReplacementNamed(context, 'resumen');
    } else if(stateCart.paymentMethodOptions == "Efectivo"){
      Navigator.pushReplacementNamed(context, 'resumen');
      //context.read<GeneratePaymentBloc>().add(OnCartPurchase(stateCart)); //No usar aca
    }
  }
}

