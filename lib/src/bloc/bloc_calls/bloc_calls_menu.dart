

import 'package:codigo_hamburgueseria_flutter/src/bloc/get_prod_db/get_prod_db_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocCallsMenu {
  static void onCatSelectGetProd(
    BuildContext context, int productsIdentifier) {
      context.read<GetProdDbBloc>().add(OnCategorySelect(productsIdentifier + 1));
  }
}
