

import 'dart:async';

import 'package:codigo_hamburgueseria_flutter/src/bloc/generate_payment/generate_payment_bloc.dart';
import 'package:flutter/material.dart';
import 'package:mercado_pago_mobile_checkout/mercado_pago_mobile_checkout.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/constants.dart';


class GenPayment {
    static Future<void> generatePaymentCall(CartInfoPrefId stateCart, BuildContext context, String prefId) async {
    //var algoe = context.read<CartItemsBloc>().state;
    //var paymentType = context.select((CartItemsBloc element) => element.state.paymentMethodOptions);
    //var stateCart = context.select((CartItemsBloc element) => element.state);
    //var stateCart = context.read<CartItemsBloc>().state;
    //print(algoe);
    //context.select((va) => GeneratePaymentBloc().add(OnCartPurchase(algoe)));
    if (stateCart.paymentMethodOptions == "Efectivo") {
      Navigator.pushReplacementNamed(context, 'result_payment');
      context.read<GeneratePaymentBloc>().add(OnCartPurchase(stateCart, 0));
    } else if (stateCart.paymentMethodOptions == "Tarjeta") {
      // context.read<GeneratePaymentBloc>().add(OnCallPrefGen(stateCart));
      // Navigator.pushReplacementNamed(context, 'pago');
      //context.read<GeneratePaymentBloc>().add(OnCallPrefGen(stateCart));
      //sleep(Duration(seconds: 5));
      PaymentResult? result;
      try {
        result =
            await (MercadoPagoMobileCheckout.startCheckout(publicKey, prefId));
      } on Exception catch (e) {
              print(e);
              Navigator.pushReplacementNamed(context, 'menu');
              return;
      }
      // _CastError (type 'Future<Map<dynamic, dynamic>?>' is not a subtype of type 'FutureOr<Map<String, dynamic>>' in type cast)
      // print(result.toString());
      // 
      if (result != null) {
        if (result.statusDetail == "accredited" && result.status == "approved") {
          Navigator.pushReplacementNamed(context, 'result_payment');
          //prefs.codeMPGenerated = result.id;
          //context.read<GeneratePaymentBloc>().add(OnCartPurchase(stateCart));
          context.read<GeneratePaymentBloc>().add(OnCartPurchase(stateCart, result.id));
        } else if (result.statusDetail =="cc_rejected_insufficient_amount" &&result.status == "rejected") {
          //FUND
          //Navigator.pushReplacementNamed(context, 'menu');
          print("Rechazado por monto insuficiente.");
          context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
        } else if (result.statusDetail =="cc_rejected_other_reason" &&result.status == "rejected") {
          //OTHE
          Navigator.pushReplacementNamed(context, 'menu');
          print("Rechazado por error general.");
          context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
        } else if (result.statusDetail =="cc_rejected_bad_filled_other" &&result.status == "rejected") {
          //FORM
          print("Rechazado por error en formulario.");
          context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
        } else if (result.statusDetail =="cc_rejected_bad_filled_date" &&result.status == "rejected") {
          //EXPI
          print("Rechazado por error en formulario.");
          context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
        } else if (result.statusDetail =="cc_rejected_bad_filled_security_code" &&result.status == "rejected") {
          //SECU
          print("Rechazado por error en formulario.");
          context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
        } else if(result.result == "canceled") {
          Navigator.pushReplacementNamed(context, 'menu');
          print("Cancelado.");
          context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
        } else {
          Navigator.pushReplacementNamed(context, 'menu');
          print("Error no configurado.");
          context.read<GeneratePaymentBloc>().add(OnGetBackToMenu());
        }
      }
    }
  }
}