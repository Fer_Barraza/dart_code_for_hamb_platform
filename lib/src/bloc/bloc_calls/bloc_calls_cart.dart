

import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocCallsCart {
  static void onItemDelete(
      BuildContext context, int index, CartInfoPrefId state) {
    context
        .read<CartItemsBloc>()
        .add(OnItemDeleteChangeTotal(state.items[index].unitPrice));
    context
        .read<CartItemsBloc>()
        .add(OnItemDeleteDecrementQuantity(index, state.items));

    if (state.items[index].quantity == 0) {
      state.items.removeAt(index);
    }
  }
}
