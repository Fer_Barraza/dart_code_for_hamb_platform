

part of 'generate_payment_bloc.dart';

@immutable
class GeneratePaymentState {
  final int? orderCode;
  final int? orderMpGen;
  final String connectionState;
  final String preferenceId;

  GeneratePaymentState({
    this.orderCode,
    this.orderMpGen,
    this.connectionState = "isLoading",
    this.preferenceId = "",
  });
  GeneratePaymentState copyWith({
    int? orderCode,
    int? orderMpGen,
        String? connectionState,
        String? preferenceId,
  }) => GeneratePaymentState(
    orderCode: orderCode ?? this.orderCode,
    orderMpGen: orderMpGen ?? this.orderMpGen,
    connectionState: connectionState ?? this.connectionState,
    preferenceId: preferenceId ?? this.preferenceId,
  );


}

