

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/bloc/cart_items/cart_items_bloc.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/payment_products/payment_products_grpc.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/pref_id/prefid_generator_grpc.dart';
import 'package:meta/meta.dart';
import 'package:codigo_hamburgueseria_flutter/service/payment_products.pb.dart' as protopayment;
import 'package:codigo_hamburgueseria_flutter/service/pref_id_gen.pb.dart' as protopref;
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/preferencias_usuario.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_singleton/sqlite_provider.dart';
//import 'package:intl/intl.dart';

part 'generate_payment_event.dart';
part 'generate_payment_state.dart';


class GeneratePaymentBloc extends Bloc<GeneratePaymentEvent, GeneratePaymentState> {
  GeneratePaymentBloc() : super(GeneratePaymentState());
  final prefs = new PreferenciasUsuario();

  @override
  Stream<GeneratePaymentState> mapEventToState(
    GeneratePaymentEvent event,
  ) async* {    
    if (event is OnCallPrefGen) {
        protopref.ItemStatusResponse? responsePrefQuery;
        protopref.PaymentToProcess paymentToGeneratePrefId;
        try {
          paymentToGeneratePrefId = _castToPreferenceType(event);
          responsePrefQuery = await implementPrefIdGenerator(paymentToGeneratePrefId);
          print(responsePrefQuery.prefIdGen);
        } 
        catch (e) {
          print("error");
          yield state.copyWith(
            orderCode: 0,
            connectionState: "error",
            preferenceId: "",
          );
        }
        if (responsePrefQuery != null) {
          yield state.copyWith(
            //orderCode: responsePrefQuery.orderCode.toInt(),
            orderCode: 0,
            connectionState: "successful",
            preferenceId: responsePrefQuery.prefIdGen,
          );
        }
    } 
    else if (event is OnCartPurchase) {
        protopayment.ItemStatusResponse? responseQueryGrpc;
        try {
          responseQueryGrpc = await implementPayment(_castToPaymentType(event));
        } 
        catch (e) {
          yield state.copyWith(
            orderCode: 0,
            connectionState: "error",
            preferenceId: "",
          );
        }finally{
          if (responseQueryGrpc != null) {
            final DateTime today = DateTime.now();
            String dateNow = "${today.year.toString()}-${today.month.toString().padLeft(2,'0')}-${today.day.toString().padLeft(2,'0')}";
            print(dateNow);
            DBProvider.db.newOrderInsert(responseQueryGrpc.orderCode.toString(),event.orderGenerated, dateNow.toString());
            yield state.copyWith(
              orderCode: responseQueryGrpc.orderCode.toInt(),
              connectionState: "successful",
              preferenceId: "",
              orderMpGen: event.orderGenerated,
              //orderMpGen: prefs.codeMPGenerated,
            );
          }
        }
    } 
    else if (event is OnGetBackToMenu) {
      yield state.copyWith(orderCode: 0,connectionState: "isLoading",preferenceId: "");
    }
  }

  protopref.PaymentToProcess _castToPreferenceType(OnCallPrefGen event) {
    protopref.PaymentToProcess prefRequest = protopref.PaymentToProcess.create();
    prefRequest.branchId = 1;
    prefRequest.headquarterId = 1;
    prefRequest.additionalInfo = event.currentCart.additionalInfo;
    prefRequest.methodOfDelivery = event.currentCart.methodOfDelivery;
    prefRequest.statementDescriptor = event.currentCart.statementDescriptor;
    prefRequest.paymentMethodOptions = event.currentCart.paymentMethodOptions;
    prefRequest.totalPrice = event.currentCart.totalPrice;
    //prefRequest.preferenceId = "";
    
    var tempPayer = new protopref.Payer();
    var tempAddress = new protopref.Address();
    var tempPhone = new protopref.Phone();
    
    tempPhone.areaCode = event.currentCart.payer!.phone!.areaCode;
    tempPhone.number = event.currentCart.payer!.phone!.number;
    tempPayer.phone = tempPhone;
    
    
    if (event.currentCart.methodOfDelivery == "Delivery") {
      tempAddress.streetName = event.currentCart.payer!.address!.streetName!;
      tempAddress.streetNumber = event.currentCart.payer!.address!.streetNumber!;
      tempAddress.zipCode = event.currentCart.payer!.address!.zipCode;
    }else{
      tempAddress.streetName = "";
      tempAddress.streetNumber = 0;
      tempAddress.zipCode = "";
    }
    tempPayer.address = tempAddress;
    
    tempPayer.email = event.currentCart.payer!.email;
    tempPayer.name = event.currentCart.payer!.name;
    tempPayer.surname = event.currentCart.payer!.surname;
    prefRequest.payer = tempPayer;
    
    event.currentCart.items.forEach((element) {
      var tempItem = new protopref.Item();
    
      tempItem.categoryId = element.categoryId!;
      tempItem.itemId = element.itemId!;
      tempItem.currencyId = element.currencyId!;
      tempItem.description = element.description!;
      tempItem.quantity = element.quantity!;
      tempItem.title = element.title!;
      tempItem.unitPrice = element.unitPrice!;
    
      prefRequest.listaItems.add(tempItem);
    });
    return prefRequest;
  }

  protopayment.PaymentToProcess _castToPaymentType(OnCartPurchase event) {
    protopayment.PaymentToProcess request = protopayment.PaymentToProcess.create();
    request.branchId = 1;
    request.headquarterId = 1;
    request.additionalInfo = event.currentCart.additionalInfo;
    request.methodOfDelivery = event.currentCart.methodOfDelivery;
    request.statementDescriptor = event.currentCart.statementDescriptor;
    request.paymentMethodOptions = event.currentCart.paymentMethodOptions;
    request.totalPrice = event.currentCart.totalPrice;
    //request.preferenceId = "";
    request.orderMpGenerated = event.orderGenerated!;  //Descomentar cuando se implemente el nuevo campo de la BD
    
    var tempPayer = new protopayment.Payer();
    var tempAddress = new protopayment.Address();
    var tempPhone = new protopayment.Phone();
    
    tempPhone.areaCode = event.currentCart.payer!.phone!.areaCode;
    tempPhone.number = event.currentCart.payer!.phone!.number;
    tempPayer.phone = tempPhone;
    
    
    if (event.currentCart.methodOfDelivery == "Delivery") {
      tempAddress.streetName = event.currentCart.payer!.address!.streetName!;
      tempAddress.streetNumber = event.currentCart.payer!.address!.streetNumber!;
      tempAddress.zipCode = event.currentCart.payer!.address!.zipCode;
    }else{
      tempAddress.streetName = "";
      tempAddress.streetNumber = 0;
      tempAddress.zipCode = "";
    }
    tempPayer.address = tempAddress;
    
    tempPayer.email = event.currentCart.payer!.email;
    tempPayer.name = event.currentCart.payer!.name;
    tempPayer.surname = event.currentCart.payer!.surname;
    request.payer = tempPayer;
    
    event.currentCart.items.forEach((element) {
      var tempItem = new protopayment.Item();
    
      tempItem.categoryId = element.categoryId!;
      tempItem.itemId = element.itemId!;
      tempItem.currencyId = element.currencyId!;
      tempItem.description = element.description!;
      tempItem.quantity = element.quantity!;
      tempItem.title = element.title!;
      tempItem.unitPrice = element.unitPrice!;
    
      request.listaItems.add(tempItem);
    });
    return request;
  }
}
