

part of 'generate_payment_bloc.dart';

@immutable
abstract class GeneratePaymentEvent {}

class OnCartPurchase extends GeneratePaymentEvent {
  //final CartInfoPrefId cartinfoprefid;
  final CartInfoPrefId currentCart;
  //final String asdfa;
  final int? orderGenerated;

  OnCartPurchase(this.currentCart,this.orderGenerated);
}
class OnCallPrefGen extends GeneratePaymentEvent {
  //final CartInfoPrefId cartinfoprefid;
  final CartInfoPrefId currentCart;

  OnCallPrefGen(this.currentCart,);
}

class OnGetBackToMenu extends GeneratePaymentEvent {}
