part of 'addcartchangelike_cubit.dart';

@immutable
abstract class AddcartchangelikeState {}

class AddcartchangelikeInitial extends AddcartchangelikeState {
  final isPaused = true;
}
class NotMorePaused extends AddcartchangelikeState {
  final isPaused = false;
}
class RestartPaused extends AddcartchangelikeState {
  final isPaused = true;
}
