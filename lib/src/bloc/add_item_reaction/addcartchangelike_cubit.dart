import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'addcartchangelike_state.dart';

class AddcartchangelikeCubit extends Cubit<AddcartchangelikeState> {
  AddcartchangelikeCubit() : super(AddcartchangelikeInitial());

  void addItemNotMorePaused(){
    emit(NotMorePaused());
  }
  void restartPaused(){
    emit(RestartPaused());
  }
}
