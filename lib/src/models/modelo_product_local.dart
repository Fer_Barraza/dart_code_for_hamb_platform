// To parse this JSON data, do
//
//     final productLocalDbModel = productLocalDbModelFromJson(jsonString);



import 'dart:convert';

//import 'dart:ffi';

List<ProductLocalDbModel> productLocalDbModelFromJson(String str) => List<ProductLocalDbModel>.from(json.decode(str).map((x) => ProductLocalDbModel.fromJson(x)));

String productLocalDbModelToJson(List<ProductLocalDbModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductLocalDbModel {
    ProductLocalDbModel({
        this.id,
        this.name,
        this.tipos,
    });

    int? id;
    String? name;
    List<Tipo>? tipos;

    factory ProductLocalDbModel.fromJson(Map<String, dynamic> json) => ProductLocalDbModel(
        id: json["id"],
        name: json["name"],
        tipos: List<Tipo>.from(json["tipos"].map((x) => Tipo.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "tipos": List<dynamic>.from(tipos!.map((x) => x.toJson())),
    };
}

class Tipo {
    Tipo({
        this.id,
        this.imageName,
        this.descripcion,
        this.price,
    });

    int? id;
    String? imageName;
    String? descripcion;
    double? price;

    factory Tipo.fromJson(Map<String, dynamic> json) => Tipo(
        id: json["id"],
        imageName: json["imageName"],
        descripcion: json["descripcion"],
        price: json["price"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "imageName": imageName,
        "descripcion": descripcion,
        "price": price,
    };
}
