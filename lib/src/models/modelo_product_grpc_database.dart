// To parse this JSON data, do
//
//     final productLocalDbModel = productLocalDbModelFromJson(jsonString);



//import 'dart:convert';

//import 'dart:ffi';

//List<ProductLocalDbModel> productLocalDbModelFromJson(String str) => List<ProductLocalDbModel>.from(json.decode(str).map((x) => ProductLocalDbModel.fromJson(x)));

//String productLocalDbModelToJson(List<ProductLocalDbModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductDbModel {
    ProductDbModel({
        // this.id,
        // this.name,
        this.tipos,
    });
    // int id;
    // String name;
    List<Tipo>? tipos;
}
class Tipo {
    Tipo({
        this.category,
        this.id,
        this.categoryId,
        this.nombreItem,
        this.imageName,
        this.descripcion,
        this.price,
    });

    String? category;
    int? id;
    int? categoryId;
    String? nombreItem;
    String? imageName;
    String? descripcion;
    double? price;

  String getPosterImg() {

    if ( imageName == null || imageName == "" ) {
      return 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg';
      //return 'https://cdn11.bigcommerce.com/s-auu4kfi2d9/stencil/59512910-bb6d-0136-46ec-71c445b85d45/e/933395a0-cb1b-0135-a812-525400970412/icons/icon-no-image.svg';
    } else {
      return 'https://grpc-fabrikapps9.servehttp.com:30443/images/$categoryId/$imageName';
      // return 'https://grpc-fabrikapps9.servehttp.com:29543/images/$categoryId/$imageName';
      //return 'https://image.tmdb.org/t/p/w500/$imageName';
    }
  }
}
