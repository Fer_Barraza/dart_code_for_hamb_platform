// To parse this JSON data, do
//
//     final orderSqliteModel = orderSqliteModelFromJson(jsonString);



import 'dart:convert';


OrderSqliteModel orderSqliteModelFromJson(String str) => OrderSqliteModel.fromJson(json.decode(str));

String orderSqliteModelToJson(OrderSqliteModel data) => json.encode(data.toJson());

class OrderSqliteModel {
    OrderSqliteModel({
        this.orderId,
        this.orderCode,
        this.orderMp,
        this.orderDate,
    });

    int? orderId;
    String? orderCode;
    int? orderMp;
    String? orderDate;

    factory OrderSqliteModel.fromJson(Map<String, dynamic> json) => OrderSqliteModel(
        orderId: json["OrderId"],
        orderCode: json["OrderCode"],
        orderMp: json["OrderMp"],
        orderDate: json["OrderDate"],
    );

    Map<String, dynamic> toJson() => {
        "OrderId": orderId,
        "OrderCode": orderCode,
        "OrderMp": orderMp,
        "OrderDate": orderDate,
    };
}
