// To parse this JSON data, do
//
//     final cartInfoPrefId = cartInfoPrefIdFromJson(jsonString);



import 'dart:convert';

CartInfoPrefId cartInfoPrefIdFromJson(String str) => CartInfoPrefId.fromJson(json.decode(str));

String cartInfoPrefIdToJson(CartInfoPrefId data) => json.encode(data.toJson());

class CartInfoPrefId {
    CartInfoPrefId({
        this.items,
        this.payer,
        this.paymentMethods,
        this.additionalInfo,
    });

    List<Item>? items;
    Payer? payer;
    PaymentMethods? paymentMethods;
    String? additionalInfo;

    CartInfoPrefId copyWith({
        List<Item>? items,
        Payer? payer,
        PaymentMethods? paymentMethods,
        String? additionalInfo,
    }) => 
        CartInfoPrefId(
            items: items ?? this.items,
            payer: payer ?? this.payer,
            paymentMethods: paymentMethods ?? this.paymentMethods,
            additionalInfo: additionalInfo ?? this.additionalInfo,
        );

    factory CartInfoPrefId.fromJson(Map<String, dynamic> json) => CartInfoPrefId(
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        payer: Payer.fromJson(json["payer"]),
        paymentMethods: PaymentMethods.fromJson(json["payment_methods"]),
        additionalInfo: json["additional_info"],
    );

    Map<String, dynamic> toJson() => {
        "items": List<dynamic>.from(items!.map((x) => x.toJson())),
        "payer": payer!.toJson(),
        "payment_methods": paymentMethods!.toJson(),
        "additional_info": additionalInfo,
    };
}

class Item {
    Item({
        this.title,
        this.description,
        this.quantity,
        this.currencyId,
        this.unitPrice,
    });

    String? title;
    String? description;
    int? quantity;
    String? currencyId;
    double? unitPrice;

    Item copyWith({
        String? title,
        String? description,
        int? quantity,
        String? currencyId,
        double? unitPrice,
    }) => 
        Item(
            title: title ?? this.title,
            description: description ?? this.description,
            quantity: quantity ?? this.quantity,
            currencyId: currencyId ?? this.currencyId,
            unitPrice: unitPrice ?? this.unitPrice,
        );

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        title: json["title"],
        description: json["description"],
        quantity: json["quantity"],
        currencyId: json["currency_id"],
        unitPrice: json["unit_price"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "description": description,
        "quantity": quantity,
        "currency_id": currencyId,
        "unit_price": unitPrice,
    };
}

class Payer {
    Payer({
        this.email,
        this.name,
        this.surname,
        this.phone,
        this.address,
    });

    String? email;
    String? name;
    String? surname;
    Phone? phone;
    Address? address;

    Payer copyWith({
        String? email,
        String? name,
        String? surname,
        Phone? phone,
        Address? address,
    }) => 
        Payer(
            email: email ?? this.email,
            name: name ?? this.name,
            surname: surname ?? this.surname,
            phone: phone ?? this.phone,
            address: address ?? this.address,
        );

    factory Payer.fromJson(Map<String, dynamic> json) => Payer(
        email: json["email"],
        name: json["name"],
        surname: json["surname"],
        phone: Phone.fromJson(json["phone"]),
        address: Address.fromJson(json["address"]),
    );

    Map<String, dynamic> toJson() => {
        "email": email,
        "name": name,
        "surname": surname,
        "phone": phone!.toJson(),
        "address": address!.toJson(),
    };
}

class Address {
    Address({
        this.zipCode,
        this.streetName,
        this.streetNumber,
    });

    String? zipCode;
    String? streetName;
    int? streetNumber;

    Address copyWith({
        String? zipCode,
        String? streetName,
        int? streetNumber,
    }) => 
        Address(
            zipCode: zipCode ?? this.zipCode,
            streetName: streetName ?? this.streetName,
            streetNumber: streetNumber ?? this.streetNumber,
        );

    factory Address.fromJson(Map<String, dynamic> json) => Address(
        zipCode: json["zip_code"],
        streetName: json["street_name"],
        streetNumber: json["street_number"],
    );

    Map<String, dynamic> toJson() => {
        "zip_code": zipCode,
        "street_name": streetName,
        "street_number": streetNumber,
    };
}

class Phone {
    Phone({
        this.areaCode,
        this.number,
    });

    String? areaCode;
    String? number;

    Phone copyWith({
        String? areaCode,
        String? number,
    }) => 
        Phone(
            areaCode: areaCode ?? this.areaCode,
            number: number ?? this.number,
        );

    factory Phone.fromJson(Map<String, dynamic> json) => Phone(
        areaCode: json["area_code"],
        number: json["number"],
    );

    Map<String, dynamic> toJson() => {
        "area_code": areaCode,
        "number": number,
    };
}

class PaymentMethods {
    PaymentMethods({
        this.excludedPaymentTypes,
        this.installments,
    });

    List<ExcludedPaymentType>? excludedPaymentTypes;
    int? installments;

    PaymentMethods copyWith({
        List<ExcludedPaymentType>? excludedPaymentTypes,
        int? installments,
    }) => 
        PaymentMethods(
            excludedPaymentTypes: excludedPaymentTypes ?? this.excludedPaymentTypes,
            installments: installments ?? this.installments,
        );

    factory PaymentMethods.fromJson(Map<String, dynamic> json) => PaymentMethods(
        excludedPaymentTypes: List<ExcludedPaymentType>.from(json["excluded_payment_types"].map((x) => ExcludedPaymentType.fromJson(x))),
        installments: json["installments"],
    );

    Map<String, dynamic> toJson() => {
        "excluded_payment_types": List<dynamic>.from(excludedPaymentTypes!.map((x) => x.toJson())),
        "installments": installments,
    };
}

class ExcludedPaymentType {
    ExcludedPaymentType({
        this.id,
    });

    String? id;

    ExcludedPaymentType copyWith({
        String? id,
    }) => 
        ExcludedPaymentType(
            id: id ?? this.id,
        );

    factory ExcludedPaymentType.fromJson(Map<String, dynamic> json) => ExcludedPaymentType(
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
    };
}
