import 'dart:io';

import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdState {
  Future<InitializationStatus> initialization;

  AdState(this.initialization);

  String get bannerUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/2934735716';
    } else if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/6300978111';
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
  String get intersticialUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/1033173712';
    } else if (Platform.isIOS) {
      return '';
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
  String get intersticialVideoUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/8691691433';
    } else if (Platform.isIOS) {
      return '';
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  AdListener get adListener => _adListener;

  AdListener _adListener = AdListener(
    onAdLoaded: (ad) => print('Ad Loaded: ${ad.adUnitId}.'),
    onAdClosed: (ad) => print('Ad Closed: ${ad.adUnitId}.'),
    onAdFailedToLoad: (ad,err) {
      print('Ad failed to load: ${ad.adUnitId}, $err.');
      ad.dispose();
    },
    onAdOpened: (ad) => print('Ad opened: ${ad.adUnitId}.'),
    onAppEvent: (ad,name,data) => print('Ad Closed: ${ad.adUnitId}.$name, $data.'),
    onApplicationExit: (ad) => print('Ad exit: ${ad.adUnitId}.'),
    onNativeAdClicked: (nativeAd) => print('Native ad clicked: ${nativeAd.adUnitId}.'),
    onNativeAdImpression: (nativeAd) => print('Native ad impression: ${nativeAd.adUnitId}.'),
    onRewardedAdUserEarnedReward: (ad,reward) => print('User rewarded: ${ad.adUnitId}, ${reward.amount}, ${reward.type}, '),
  );
}
