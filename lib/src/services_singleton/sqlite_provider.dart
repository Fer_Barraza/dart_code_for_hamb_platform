

import 'dart:async';
import 'dart:io';

import 'package:codigo_hamburgueseria_flutter/src/models/sql_db_model.dart';
import 'package:sqflite/sqflite.dart';
//import 'package:sqflite/sqlite_api.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._internal();

  // factory PreferenciasUsuario() {
  //   return _instancia;
  // }

  DBProvider._internal();

  Future<Database?> get database async {
    if(_database != null) return _database;
    _database = await initLocalDb();
    return _database;
  }


  initLocalDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    final path = join(documentDirectory.path, 'localdb.db');

    return await openDatabase(
      path,
      version: 1,
      onOpen: (db){},
      onCreate: (Database db, int version) async {
        await db.execute('CREATE TABLE OrderPurchaseHistory ( OrderId INTEGER PRIMARY KEY, OrderCode TEXT, OrderMp INTEGER, OrderDate TEXT )');
      }
    );
  }

  newOrderInsert(String orderCode, int? orderMp, String orderDate)async{
    final db = await database;
    if (orderMp == null) orderMp = 0;
    if (db == null) return null;
    final res = await db.rawInsert('INSERT INTO OrderPurchaseHistory (OrderCode,OrderMp,OrderDate) VALUES ("$orderCode",$orderMp,"$orderDate")');
    return res;
  }
  Future<List<OrderSqliteModel>> newOrderSelect()async{
    final db = await database;
    if (db == null) return [];
    //var res = await db.rawQuery('SELECT OrderId, OrderMp FROM OrderPurchaseHistory');
    var res = await db.rawQuery('SELECT OrderId, OrderCode, OrderMp, OrderDate FROM OrderPurchaseHistory WHERE OrderId != (SELECT OrderId FROM OrderPurchaseHistory ORDER BY OrderId DESC LIMIT 1) ORDER BY OrderId DESC');
    List<OrderSqliteModel> list = res.isNotEmpty ? res.map((c) => OrderSqliteModel.fromJson(c)).toList():[];
    return list;
  }
  Future<OrderSqliteModel?> orderSelectLastOnly()async{
    final db = await database;
    if (db == null) return null;
    var res = await db.rawQuery('SELECT * FROM OrderPurchaseHistory ORDER BY OrderId DESC LIMIT 1');
    List<OrderSqliteModel> last = res.isNotEmpty ? res.map((c) => OrderSqliteModel.fromJson(c)).toList():[];
    if (last.length == 0){
      OrderSqliteModel? algo;
      return algo;
    }{
      return last.first;
    }  }



}