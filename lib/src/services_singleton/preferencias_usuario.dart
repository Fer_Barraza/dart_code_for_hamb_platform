

import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  late SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

 // GET y SET del nombreUsuario
  String get nombreComprador {
    return _prefs.getString('nombreComprador') ?? '';
  }

  set nombreComprador( String value ) {
    _prefs.setString('nombreComprador', value);
  }

 // GET y SET del nombreUsuario
  String get apellidoComprador {
    return _prefs.getString('apellidoComprador') ?? '';
  }

  set apellidoComprador( String value ) {
    _prefs.setString('apellidoComprador', value);
  }

 // GET y SET del nombreUsuario
  String get celComprador {
    return _prefs.getString('celComprador') ?? '';
  }

  set celComprador( String value ) {
    _prefs.setString('celComprador', value);
  }

 // GET y SET del nombreUsuario
  String get mailComprador {
    return _prefs.getString('mailComprador') ?? '';
  }

  set mailComprador( String value ) {
    _prefs.setString('mailComprador', value);
  }

 // GET y SET del nombreUsuario
  String get direccionComprador {
    return _prefs.getString('direccionComprador') ?? '';
  }

  set direccionComprador( String value ) {
    _prefs.setString('direccionComprador', value);
  }







  // GET y SET de streetNumber
  String get streetNumberString {
    return _prefs.getString('streetNumber') ?? "";
  }

  set streetNumberString( String value ) {
    //int temp = int.parse(value);
    _prefs.setString('streetNumber', value);
  }



  // GET y SET de MP codigo de compra
  // get codeMPGenerated {
  //   return _prefs.getInt('codeMPGenerated') ?? null;
  // }
  // set codeMPGenerated( int value ) {
  //   //int temp = int.parse(value);
  //   _prefs.setInt('codeMPGenerated', value);
  // }
  // // GET y SET de codigo de orden de la compra
  // get orderCode {
  //   return _prefs.getInt('orderCode') ?? null;
  // }
  // set orderCode( int value ) {
  //   //int temp = int.parse(value);
  //   _prefs.setInt('orderCode', value);
  // }
}
