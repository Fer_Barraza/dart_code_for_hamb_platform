import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:codigo_hamburgueseria_flutter/service/item_products.pbgrpc.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/client/grpc_client.dart';
//import 'package:grpc/src/client/call.dart';
import 'package:grpc/grpc.dart';

class ItemProdCaller {
  static Future<ItemListResponse> cargaLocalG(int req) async {
    CategoryIdRequest request = CategoryIdRequest.create();
    request.setField(1, req);
    //request.setField(2, 1);
    //request.setField(3,1);
    //categoryIdRequest.reqCategoryId = req; tambien funciona
    request.reqBranchId = branchId;
    request.reqHeadquarterId = headquarterId;
    ItemListResponse response = await _getProductsList(request);
    return response;
  }

  static Future<ItemListResponse> _getProductsList(CategoryIdRequest req) async {
    //var client = ProductoStockClient(GrpcClient.getClient(), options: CallOptions(metadata:{'my-header':'item-products'}));
    var client = ProductoStockClient(GrpcClient.getItemProdClient()!,
        options: CallOptions(metadata: {'my-header': 'item-products'}));

    return await client.getProductsList(req);
    //return await client.getProductsList(req, options: CallOptions(metadata:{'my-header':'item-products'}));
  }

  static Future<GetCategoriesListResponse> getCategorieInfoForMenu(
      int branchId, int headquarterId) async {
    GetCategoriesListRequest request = GetCategoriesListRequest.create();
    request.reqBranchId = branchId;
    request.reqHeadquarterId = headquarterId;
    GetCategoriesListResponse response = await _getCategoryList(request);
    return response;
  }

  static Future<GetCategoriesListResponse> _getCategoryList(
      GetCategoriesListRequest req) async {
    //var client = ProductoStockClient(GrpcClient.getClient(), options: CallOptions(metadata:{'my-header':'item-products'}));
    var client = ProductoStockClient(GrpcClient.getItemProdClient()!,
        options: CallOptions(metadata: {'my-header': 'item-products'}));

    return await client.getCategoriesList(req);
    //return await client.getProductsList(req, options: CallOptions(metadata:{'my-header':'item-products'}));
  }
}
