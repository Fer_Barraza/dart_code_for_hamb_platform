//import 'package:codigo_hamburgueseria_flutter/service/item_products.pbgrpc.dart';



import 'package:grpc/grpc.dart';
//import 'package:codigo_hamburgueseria_flutter/src/services_grpc/get_products_desc_grpc.dart';
//final stub = Client(channel, options: CallOptions(metadata:{'authorization':'Bearer accesstokendetails'}))

class GrpcClient {
  static ClientChannel? client;
  static ClientChannel? clientItemProduct;
  static ClientChannel? clientPayment;
  static ClientChannel? clientPrefId;
  static ClientChannel? clientEnv;
  
  static ClientChannel? getClient(){
    if (client == null){
      client = 
        ClientChannel(
          "grpc-fabrikapps.servehttp.com",
          //"192.168.1.36",
          port: 31380,
          options: ChannelOptions(
            credentials: ChannelCredentials.insecure(),
            idleTimeout: Duration(seconds: 12)
          )

        );
    }
    //var stub = ProductoStockClient(client, options: CallOptions(metadata:{'my-header':'item-products'}));

    return client;
  }
  static ClientChannel? getPaymentClient(){
    if (clientPayment == null){
      clientPayment = 
        ClientChannel(
          "grpc-fabrikapps9.servehttp.com",
          // port: 50052,
          port: 30052,
          options: ChannelOptions(
            credentials: ChannelCredentials.insecure(),
            idleTimeout: Duration(seconds: 12)
          )
        );
    }
    return clientPayment;
  }
  static ClientChannel? getPrefIdClient(){
    if (clientPrefId == null){
      clientPrefId = 
        ClientChannel(
          "grpc-fabrikapps9.servehttp.com",
          port: 30054,
          // port: 50054,
          options: ChannelOptions(
            credentials: ChannelCredentials.insecure(),
            idleTimeout: Duration(seconds: 12)
          )
        );
    }
    return clientPrefId;
  }
  static ClientChannel? getItemProdClient(){
    if (clientItemProduct == null){
      clientItemProduct = 
        ClientChannel(
          "grpc-fabrikapps9.servehttp.com",
          port: 30057,
          // port: 50057,
          options: ChannelOptions(
            credentials: ChannelCredentials.insecure(),
            idleTimeout: Duration(seconds: 12)
          )
        );
    }
    return clientItemProduct;
  }
  static ClientChannel? getEnvOnInitClient(){
    if (clientEnv == null){
      clientEnv = 
        ClientChannel(
          "grpc-fabrikapps9.servehttp.com",
          port: 4060,
          options: ChannelOptions(
            credentials: ChannelCredentials.insecure(),
            idleTimeout: Duration(seconds: 12)
          )
        );
    }
    return clientEnv;
  }

  //clientPayment.shutdown();


}