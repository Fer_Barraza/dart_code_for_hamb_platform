

import 'package:codigo_hamburgueseria_flutter/service/pref_id_gen.pbgrpc.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/client/grpc_client.dart';
import 'package:grpc/grpc.dart';



Future<ItemStatusResponse> implementPrefIdGenerator(PaymentToProcess req) async {

    ItemStatusResponse response = await _getProductsList(req);
    return response;


}

Future<ItemStatusResponse> _getProductsList(PaymentToProcess req) async {
  //var client = PaymentProductsClient(GrpcClient.getClient(), options: CallOptions(metadata:{'my-header':'preference-id'}));
  var client = PaymentProductsClient(GrpcClient.getPrefIdClient()!, options: CallOptions(metadata:{'my-header':'preference-id'}));

   return await client.prefIdGenAndInsertProductsPurchased(req);   

}
