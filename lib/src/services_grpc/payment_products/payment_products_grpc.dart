

import 'dart:ffi';

import 'package:codigo_hamburgueseria_flutter/src/constants.dart';
import 'package:codigo_hamburgueseria_flutter/service/payment_products.pbgrpc.dart';
import 'package:codigo_hamburgueseria_flutter/src/services_grpc/client/grpc_client.dart';
import 'package:fixnum/fixnum.dart';
import 'package:grpc/grpc.dart';



Future<ItemStatusResponse> implementPayment(PaymentToProcess req) async {
  //PaymentToProcess request = PaymentToProcess.create();
  // request.setField(1, req);
  // request.setField(2, 1);
  //categoryIdRequest.reqCategoryId = req; tambien funciona
 
    ItemStatusResponse response = await _getProductsList(req);
    return response;


}
Future<ItemStatusResponse> _getProductsList(PaymentToProcess req) async {
  //var client = PaymentProductsClient(GrpcClient.getClient(), options: CallOptions(metadata:{'my-header':'payment-products'}));
  var client = PaymentProductsClient(GrpcClient.getPaymentClient()!, options: CallOptions(metadata:{'my-header':'payment-products'}));

  return await client.insertProductsPurchased(req);   
}
// GetOrderById
// Stream<ResponseStream<GetLastOrderByIdResponse>> getOrderByOrderId(GetLastOrderByIdRequest req) async* {
//     var response =  _getOrderByOrderId(req);
//     yield response;
// }

// Stream<ResponseStream<GetLastOrderByIdResponse>> _getOrderByOrderId(GetLastOrderByIdRequest req) async* {
//   //var client = PaymentProductsClient(GrpcClient.getClient(), options: CallOptions(metadata:{'my-header':'payment-products'}));
//   var client = PaymentProductsClient(GrpcClient.getPaymentClient(), options: CallOptions(metadata:{'my-header':'payment-products'}));
  
//   // await for (var feature in client.getLastOrderById(req)) {
//   //   print(feature);
//   // }
  
  
//   client.getLastOrderById(req).listen((value) {
//     print(value);
//   });  
  
//   yield client.getLastOrderById(req);   
// }


// Future<GetLastOrderByIdResponse> getOrderByOrderIdF(GetLastOrderByIdRequest req) async {
//   //var client = PaymentProductsClient(GrpcClient.getClient(), options: CallOptions(metadata:{'my-header':'payment-products'}));
//   var client = PaymentProductsClient(GrpcClient.getPaymentClient(), options: CallOptions(metadata:{'my-header':'payment-products'}));
  
//   await for (var feature in client.getLastOrderById(req)) {
//     print(feature);
//     return feature;

//   }
// }
Stream<GetLastOrderByIdResponse> getOrderByOrderId(String lastOrder) async* {
   //var client = PaymentProductsClient(GrpcClient.getClient(), options: CallOptions(metadata:{'my-header':'payment-products'}));
  final client = PaymentProductsClient(GrpcClient.getPaymentClient()!, options: CallOptions(metadata:{'my-header':'payment-products'}));
  
  // await for (var feature in client.getLastOrderById(req)) {
  //   print(feature);
  // }
  GetLastOrderByIdRequest req = GetLastOrderByIdRequest.create();
  req.branchId = branchId;
  Int64 algo = Int64.parseInt(lastOrder);
  // var algo = lastOrder as Int64;
  print(algo.toInt());
  print(algo.toString());
  req.orderCode = algo;
  req.headquarterId = headquarterId;
  
  // client.getLastOrderById(req).listen((value) {
     print("aca");
  // });  
  //client.getLastOrderById(req).cancel();

  //yield client.getLastOrderById(req);  
  //client.getLastOrderById(req).cancel();
    await for (var val in client.getLastOrderById(req)) { //the error in this line from the server
      //print("in the loop");
      //print(val.orderById.methodOfDelivery);
      yield val;
    }
}

    //final streamOrder = getOrderByOrderId(req);