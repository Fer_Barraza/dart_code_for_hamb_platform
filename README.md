# codigo_hamburgueseria_flutter

General ecommerce application with flutter for customers

## About the app

The application was made with flutter and uses external services such as grpc in the backend, mercadopago mobile checkout and facebook api

## UI example

This is an application test, but in production the appearance may change. 

Principal functionalities:

- See categories
- See products
- See, make and track orders
- Chat
- Credit/debit payments
- Etc...

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://drive.google.com/file/d/1_IgepBJyZNTrV5GbGSuEshkh8qk0D1L6/preview" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
<img src="doc/assets/ezgif-4-377e432deb9f.gif" width="250">
<img src="doc/assets/ezgif-4-83c5a6fd6a7e.gif" width="250">
<img src="doc/assets/ezgif-4-05a9f8d55ae9.gif" width="250">
<img src="doc/assets/ezgif-4-9e9b7c96cf76.gif" width="250">
<img src="doc/assets/ezgif-4-011510178803.gif" width="250">
<img src="doc/assets/ezgif-4-b1ca1e578020.gif" width="250">
<img src="doc/assets/ezgif-4-9ac231300231.gif" width="250">

<!-- ![Alt Text](doc/assets/ezgif-4-377e432deb9f.gif "intro")
![Alt Text](doc/assets/ezgif-4-83c5a6fd6a7e.gif "products")
![Alt Text](doc/assets/ezgif-4-05a9f8d55ae9.gif "borro")
![Alt Text](doc/assets/ezgif-4-9e9b7c96cf76.gif "form")
![Alt Text](doc/assets/ezgif-4-011510178803.gif "crear orden")
![Alt Text](doc/assets/ezgif-4-b1ca1e578020.gif "ver orden")
![Alt Text](doc/assets/ezgif-4-9ac231300231.gif "mp") -->

